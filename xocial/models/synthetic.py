from . import *
from django.db.models import *
from .meta import *
from enum import Enum
from django.db import IntegrityError

class SyntheticSource(Source):
    pass

class SyntheticMessage(Message):
    visible_fields = ['sent_timestamp', 'remote_id']


    # Validates that remote_id and conversation are unique together
    def save(self, *args, **kwargs):
        if self.remote_id is not None:
            try:
                obj = self.__class__._default_manager.get(conversation=self.conversation, remote_id=self.remote_id)
                if obj.id != self.id:
                    raise IntegrityError(f'Duplicate entry for conversation: {self.conversation.id} and remote_id: {self.remote_id}')
            except self.__class__.DoesNotExist:
                pass

        super().save(*args, **kwargs)

    sent_timestamp = DateTimeField(default=None, null=True, blank=True)
    remote_id = IntegerField(default=None, null=True, blank=True)
