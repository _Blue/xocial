import django.db
from datetime import timedelta
from pydoc import locate
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from xocial.websocket.app import *

import logging

now_override = None # Used for testing only


def override_now(ts):
    global now_override
    now_override = ts

def get_all_visible_fields(model_type: type):
    visible_fields = set()
    visible_by_id_fields = set()

    while model_type != models.Model:
        if type(model_type) is str:
            model_type = locate(f'xocial.models.{model_type}')

        visible_fields.update(model_type.visible_fields)
        visible_by_id_fields.update(model_type.visible_by_id_fields)

        model_type = model_type.__bases__[0]

    return (visible_fields, visible_by_id_fields)

def get_all_extra_queries(model_type: type):
    extra_queries = {}

    while model_type != models.Model:
        extra_queries.update(model_type.extra_queries)

        model_type = model_type.__bases__[0]

    return extra_queries

class ManyToManyField(models.ManyToManyField):
    def __init__(self, to=None, *args, **kwargs):
        super().__init__(to, *args, **kwargs)

        if type:
            self._type = to


class ForeignKey(models.ForeignKey):
    def __init__(self, to=None, *args, **kwargs):
        super().__init__(to, *args, **kwargs)

        if type:
            self._type = to

class BlobField(models.Field):
    description = "Blob"

    def db_type(self, connection):
        if connection.display_name == 'SQLite':
            return "blob"
        elif connection.display_name == 'PostgreSQL':
            return "bytea"
        else:
            raise RuntimeError(f'Database engine not supported: {connection.display_name}')


class DataModel(models.Model):
    visible_fields = ['id', 'last_updated']
    visible_by_id_fields = []
    extra_queries = []
    parent_updates = []
    fields_types = {}
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    last_updated = models.DateTimeField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.leaf_instance = None

    def update_parent(self, parent: str, now):

        field = self
        for e in parent.split('.'):
            field = getattr(field, e)

        if not field:
            return

        field.mark_updated(now)
        field.save(modified=True)

    # This logic is here to simplify the caching contract for clients:
    # It is guaranteed that every modification to an object will update its last_updated timestamp
    # But it it theoretically possible for two modifications to happens in the same millisecond.
    # The below block ensure that if that happens, the timestamp will be different between the two updates
    def get_new_updated_time(self):
        now = timezone.now()

        if not self.last_updated:
            return now

        return max(now, self.last_updated + timedelta(microseconds=1000))

    def mark_updated(self, now=None):
        now = now or now_override or self.get_new_updated_time()
        self.last_updated = now

        for e in self.parent_updates:
            self.update_parent(e, now)

    def save(self, modified=False, *args, **kwargs):
        if self.pk is None: # If new object is being saved
            self.content_type = ContentType.objects.get_for_model(self.__class__)


        modified = modified or self.is_modified()
        if modified:
            self.mark_updated()

        super().save(*args, **kwargs)

        if modified: # Can't be done before saving because object might not have an ID
            push(self.id)

    def is_modified(self):
        if not self.last_updated:
            return True

        try:
            stored_instance = DataModel.objects.get(id=self.id).as_leaf_class()
        except DataModel.DoesNotExist as e:
            return False

        visible_fields, visible_by_id_fields= get_all_visible_fields(type(self))
        for e in [f for f in visible_fields | visible_by_id_fields if f != 'last_updated']:
            field = type(self)._meta.get_field(e)

            if isinstance(field, ManyToManyField):
                if set([e.id for e in getattr(self, e).all()]) != set([e.id for e in getattr(stored_instance, e).all()]):
                    return True

            elif isinstance(field, ForeignKey):
                current = getattr(self, e)
                stored = getattr(stored_instance, e)

                if (current is None and stored is not None) or (current is not None and stored is None):
                    return True
                elif current is not None and current.id != stored.id:
                    return True


            elif getattr(self, e) != getattr(stored_instance, e):
                return True

        return False

    def as_leaf_class(self):
        if self.leaf_instance is None:
            model = self.content_type.model_class()

            if model == type(self):
                self.leaf_instance = self
            else:
                self.leaf_instance = model.objects.get(id=self.id)

        return self.leaf_instance

def on_many_to_many_changed(sender, action, pk_set, instance, **kwargs):
    if action not in ["pre_add", "pre_remove", "pre_clear"]:
        return

    instance.mark_updated(now_override or timezone.now())
    instance.save(modified=True)
