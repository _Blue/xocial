from . import *
from django.db.models import *
from .meta import *
from enum import Enum


class FacebookSource(Source):
    visible_fields = ['user_email', 'facebook_user_id']

    user_email = CharField(max_length=256)

    credential = TextField()

    facebook_user_id = TextField(null=True, default=None, blank=True)

    last_sync = DateTimeField(null=True, blank=True, default=None)

    disable_listen = BooleanField(default=False)

class FacebookSourceContact(SourceContact):
    visible_fields = ['facebook_user_id']

    facebook_user_id = TextField(unique=True)

    has_fetch_error = BooleanField(default=False)


class FacebookConversation(Conversation):
    visible_fields = ['facebook_conversation_id', 'facebook_conversation_type', 'facebook_last_message_timestamp']

    facebook_conversation_id = TextField(unique=True)

    facebook_conversation_type_choices = [(e, e) for e in ('user', 'group', 'room', 'page')]

    facebook_conversation_type = CharField(choices=facebook_conversation_type_choices, max_length=10)

    facebook_last_message_timestamp = DateTimeField(null=True, blank=True, default=None)

    # This field is used to generic 'fake' conversation for friend requests
    synthetic = BooleanField(default=False)


class FacebookMessage(Message):
    visible_fields = ['facebook_message_id']

    facebook_message_id = TextField(unique=True)

    unsent = BooleanField(default=False)

class FacebookMessageReaction(MessageReaction):
    visible_fields = ['emoji']

    emoji = TextField()


class FacebookUrlAttachment(UrlAttachment):
    visible_fields = ['facebook_attachment_id']

    facebook_attachment_id = TextField(unique=True)


class FacebookImageAttachment(ImageAttachment):
    visible_fields = ['facebook_attachment_id']

    facebook_attachment_id = TextField(unique=True)


class FacebookStickerAttachment(FacebookImageAttachment):
    visible_fields = ['facebook_sticker_pack_id', 'facebook_label']

    facebook_sticker_pack_id = TextField(null=True)

    facebook_label = TextField()


class FacebookLocationAttachment(LocationAttachment):
    visible_fields = ['facebook_attachment_id']

    facebook_attachment_id = TextField(unique=True)


class FacebookFileAttachment(FileAttachment):
    visible_fields = ['facebook_attachment_id']

    facebook_attachment_id = TextField(unique=True)


class FacebookAudioAttachment(AudioAttachment):
    visible_fields = ['facebook_attachment_id']

    facebook_attachment_id = TextField(unique=True)


class FacebookVideoAttachment(VideoAttachment):
    visible_fields = ['facebook_attachment_id']

    facebook_attachment_id = TextField(unique=True)

