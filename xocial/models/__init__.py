from xocial import settings
from django.db.models import *
from .meta import *
from enum import Enum
from django.db.models.signals import m2m_changed

def load(id: int) -> DataModel:
    return DataModel.objects.get(id=id).as_leaf_class()

def lookup_hosted_url(attachment):
    download = AttachmentDownload.objects.filter(attachment=attachment).first()
    if not download:
        return None

    return f'{settings.EXTERNAL_URL}/attachment/{download.id}'


class Error(DataModel):
    visible_fields = ['id', 'code', 'details']

    code = TextField()

    details = TextField()

class SourceStatus(Enum):
    NotStarted = "NotStarted"
    Initializing = "Initializing"
    Running = "Running"
    SoftFailed = "SoftFailed"
    HardFailed = "HardFailed"

class AttachmentState(Enum):
    Added = 'Added'
    Downloading = 'Downloading'
    Available = 'Available'
    BackedOff = 'BackedOff'
    Failed = 'Failed'

class Source(DataModel):
    visible_fields = ['display_name', 'errors', 'status', 'refresh_time']

    display_name = CharField(max_length=64, unique=True)

    status = CharField(max_length=25, choices=[(e.value, e.value) for e in SourceStatus])

    errors = ManyToManyField(Error, blank=True)

    refresh_time = FloatField(default=180)

    def save(self, *args, **kwargs):
        status_attr = getattr(self.status, 'value', None)

        if status_attr: # Work-around enum to str conversions
            self.status = status_attr

        super().save(*args, **kwargs)

class SourceContact(DataModel):
    visible_fields = ['display_name', 'display_image', 'source', 'is_me']

    source = ForeignKey(Source, on_delete=CASCADE)

    display_name = TextField()

    display_image = URLField(null=True, default=None, max_length=1000)

    is_me = BooleanField(default=False)

class Contact(DataModel):
    visible_fields = ['display_name', 'display_image', 'sources', 'is_me']

    display_name = TextField()

    display_image = URLField(null=True, blank=True, default=None, max_length=1000)

    sources = ManyToManyField(SourceContact)

    is_me = BooleanField(default=False)

class ReadMarker(DataModel):
    visible_fields = ['timestamp']
    visible_by_id_fields = ['contact', 'message']
    parent_updates = ['message.conversation']

    contact = ForeignKey(Contact, on_delete=CASCADE)
    timestamp = DateTimeField(null=True, blank=True)
    message = ForeignKey('Message', null=True, blank=True, on_delete=CASCADE)

class Conversation(DataModel):
    visible_fields = ['display_name', 'display_image', 'members', 'source', 'errors', 'gone_members', 'read_markers', 'last_read_message']

    def _unread_count(self):
        if self.last_read_message:
            return Message.objects.filter(conversation=self, timestamp__gt=self.last_read_message.timestamp).count()
        else:
            return Message.objects.filter(conversation=self).count()

    extra_queries = {
                        'last_message': lambda e: Message.objects.filter(conversation=e).order_by('-timestamp').first(),
                        'unread_count': lambda e: e._unread_count()
                    }

    display_name = TextField(max_length=100)

    display_image = URLField(null=True, blank=True, default=None, max_length=1000)

    members = ManyToManyField(Contact)

    gone_members = ManyToManyField(Contact, related_name='gone_members_conversation', blank=True)

    source = ForeignKey(Source, on_delete=CASCADE)

    last_read_message = ForeignKey('Message', related_name='last_read_conversation',  null=True, default=None, blank=True, on_delete=SET_NULL)

    errors = ManyToManyField(Error, blank=True)

    read_markers = ManyToManyField(ReadMarker, blank=True)

class Attachment(DataModel):
    visible_fields = []

    extra_queries = { 'hosted_url': lambda e: lookup_hosted_url(e) }


class UrlAttachment(Attachment):
    visible_fields = ['url', 'title', 'description', 'original_url']

    url = URLField(max_length=1000)

    title = TextField(null=True, blank=True)

    description = TextField(null=True, blank=True)

    original_url = URLField(null=True, max_length=1000, blank=True)


class ImageAttachment(Attachment):
    visible_fields = ['url', 'preview_url']

    url = URLField(max_length=1000)

    preview_url = URLField(null=True, blank=True, max_length=1000)


class LocationAttachment(Attachment):
    visible_fields = ['latitude', 'longitude', 'image_url', 'map_url']

    latitude = FloatField(null=True, blank=True)

    longitude = FloatField(null=True, blank=True)

    image_url = URLField(null=True, blank=True, max_length=1000)

    map_url = URLField(null=True, blank=True, max_length=1000)


class FileAttachment(Attachment):
    visible_fields = ['url', 'name', 'size']

    url = URLField(max_length=1000)

    name = TextField(null=True, blank=True)

    size = IntegerField(null=True, blank=True)


class AudioAttachment(Attachment):
    visible_fields = ['url', 'name', 'length_ms']

    url = URLField(max_length=1000)

    name = TextField(null=True, blank=True)

    length_ms = IntegerField(null=True, blank=True)


class VideoAttachment(Attachment):
    visible_fields = ['url', 'length_ms']

    url = URLField(max_length=1000)

    length_ms = IntegerField(null=True, blank=True)


class MessageReaction(DataModel):
    visible_fields = []
    visible_by_id_fields = ['author']

    author = ForeignKey(Contact, on_delete=CASCADE)


class Message(DataModel):
    visible_fields = ['text', 'timestamp', 'author', 'conversation', 'reactions', 'edited_timestamp', 'attachments', 'reply_to']

    parent_updates = ['conversation']

    text = TextField()

    timestamp = DateTimeField()

    author = ForeignKey(Contact, on_delete=CASCADE)

    conversation = ForeignKey(Conversation, on_delete=CASCADE)

    edited_timestamp = DateTimeField(null=True, blank=True)

    attachments = ManyToManyField(Attachment, blank=True)

    reactions = ManyToManyField(MessageReaction, blank=True)

    reply_to = ForeignKey('self', null=True, default=None, blank=True, on_delete=SET_NULL)


class UploadedAttachment(DataModel):
    visible_fields = ['file_name', 'size']

    file_name = TextField()

    size = IntegerField(null=True, blank=True)

for e in [Contact.sources, Conversation.members, Conversation.gone_members, Message.attachments, Message.reactions]:
    m2m_changed.connect(on_many_to_many_changed, sender=e.through)

class AttachmentDownload(DataModel):
    attachment = ForeignKey(Attachment, on_delete=CASCADE)

    state = CharField(max_length=25, choices=[(e.value, e.value) for e in AttachmentState])

    errors = ManyToManyField(Error, blank=True)

    backoff = IntegerField(default=1)

    last_attempt = DateTimeField(null=True, blank=True)

    last_download_url = URLField(max_length=1000)

    content = BlobField(null=True, blank=True)

    web_content_type = TextField(null=True, blank=True)
