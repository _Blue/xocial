import django.db
from threading import local
from xocial.websocket.app import *

# This class is a wrapper around a django transaction
# It's used to create 'pending' queues around the websocket pusher
# To make sure that objects that haven't been flushed yet aren't push on
# the websocket thread yet (where they wouldn't be visible)

tls = local()

class Transaction:
    def __init__(self, transaction):
        tls.transaction = getattr(tls, 'transaction', self)
        self.tasks = []
        self.transaction = transaction

    def add_commit_task(self, task):
        self.tasks.append(task)

    def __enter__(self):
        start_transaction()
        self.transaction.__enter__()

    def __exit__(self, exc_type, exc_value, traceback):
        if tls.transaction == self:
            del tls.transaction

        self.transaction.__exit__(exc_type, exc_value, traceback)

        if exc_type is None:
            commit_transaction()

            for e in self.tasks:
                e()
        else:
            rollback_transaction()




def transaction():
    return Transaction(django.db.transaction.atomic())

def push_transaction_task(task):
    assert getattr(tls, 'transaction', None) is not None

    tls.transaction.add_commit_task(task)
