from . import backends, models

def get_backend(id = None):
    return backends.acquire_backend(models.Source.get(id=id) if id else models.Source.objects.first())
