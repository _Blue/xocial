import traceback
import logging
from xocial.models import DataModel, Error
from io import StringIO


MAX_OBJECT_ERRORS = 10

def save_error(object: DataModel, exception):
    if object.errors.count() > MAX_OBJECT_ERRORS:
        logging.error(f'Object {object} has exceeded maximum error count, dropping {type(exception)}')
        return

    details = StringIO()
    traceback.print_exception(type(exception), exception, exception.__traceback__, file=details)

    error = Error()
    error.code = str(type(exception).__name__)
    error.details = details.getvalue()
    error.save()

    object.errors.add(error)
    object.save()

