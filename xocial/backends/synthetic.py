import logging

import django.utils.timezone
from django.db import IntegrityError
from datetime import datetime
from xocial.models.synthetic import *
from xocial.models import *
from xocial.models.transaction import transaction
from xocial import errors
from xocial.api import utils


class SyntheticBackend:
    def __init__(self, source):
        self.source = source.as_leaf_class()
        self.create_me_contact()

    def create_me_contact(self):
        self.me_contact = Contact.objects.filter(is_me=True).first()
        if self.me_contact and any(e.source.id == self.source.id for e in self.me_contact.sources.all()):
            return

        logging.info("'Me' contact not found, creating it now")

        source_contact = self.create_source_contact('Me', None, True)
        self.me_contact = self.create_contact(source_contact)

        assert self.me_contact.is_me

    def create_source_contact(self, display_name: str, display_image: str, is_me: bool) -> SourceContact:
        if is_me:
            existing_contact = SourceContact.objects.filter(source=self.source, is_me=True).first()
        else:
            existing_contact = SourceContact.objects.filter(source=self.source, display_name=display_name, is_me=False).first()

        if existing_contact:
            logging.info(f'Existing contact found for {display_name}, returning source contact : {existing_contact.id}')
            existing_contact.display_image = display_image
            existing_contact.display_name = display_name
        else:
            logging.info(f'Creating source contact for {display_name}')
            existing_contact = SourceContact(source=self.source, display_name=display_name, display_image=display_image, is_me=is_me)

        existing_contact.save()
        return existing_contact

    def create_contact(self, source_contact: SourceContact) -> Contact:
        if source_contact.source.id != self.source.id:
            raise errors.InvalidArgument('source_contact_id', f'SourceContact {source_contact} does not match source {self.source}')


        existing_contact = Contact.objects.filter(sources=source_contact).first()
        if existing_contact:
            existing_contact.display_name = source_contact.display_name
            existing_contact.display_image = source_contact.display_image
            existing_contact.is_me = source_contact.is_me
            existing_contact.save()

            return existing_contact

        logging.info(f'Creating contact from source {source_contact}')

        contact = Contact(display_name=source_contact.display_name, display_image=source_contact.display_image, is_me=source_contact.is_me)
        contact.save()

        contact.sources.add(source_contact)
        return contact

    def create_conversation(self, display_name: str, display_image: str, members: list) -> Conversation:
        if len(members) <= 1:
            raise errors.InvalidArgument('members', 'Conversation needs at least two members')

        if not any(e.is_me for e in members):
            raise errors.InvalidArgument('members', 'Conversation needs to include "me" contact')

        if any(members.count(e) != 1 for e in members):
            raise errors.InvalidArgument('members', f'Duplicated member in member list {members}')

        existing_conversations = Conversation.objects.filter(source=self.source, display_name=display_name).all()
        for e in existing_conversations:
            if set([m.id for m in e.members.all()]) == set([m.id for m in members]):
                logging.info(f'Existing conversation found for {display_name}, returning conversation: {e.id}')
                return e

        conversation = Conversation(display_name=display_name,
                                    display_image=display_image,
                                    source=self.source)

        with transaction():
            conversation.save()

            for e in members:
                conversation.members.add(e)

        logging.info(f'Created conversation {conversation} with {len(members)} members')

        return conversation


    def send_message(self, **kwargs):
        message = self.create_message(author=self.me_contact, timestamp=django.utils.timezone.now(), **kwargs)
        message.conversation.last_read_message = message
        message.conversation.save()

        return message

    def create_message(
            self,
            author: Contact,
            text: str,
            conversation: Conversation,
            timestamp: datetime,
            sent_timestamp: datetime=None,
            attachments: list=None,
            reply_to: Message=None,
            remote_id: int=None) -> SyntheticMessage:

        if attachments:
            raise errors.NotSupportedBySource(self.source, 'attachments')

        message = SyntheticMessage(text=text,
                          author=author,
                          reply_to=reply_to,
                          conversation=conversation,
                          timestamp=timestamp,
                          sent_timestamp=sent_timestamp,
                          remote_id=remote_id)

        try:
            message.save()
        except IntegrityError as e:
            raise errors.AlreadyExists('A conflicting message already exists') from e

        logging.info(f'Created message: {message}')

        return message

    def update_message(
            self,
            text: str,
            conversation: Conversation,
            reply_to: SyntheticMessage,
            author: Contact,
            timestamp: datetime,
            sent_timestamp: datetime,
            attachments: list,
            local_id: int,
            remote_id: int,
            transmitted=False) -> SyntheticMessage:

        if attachments:
            raise errors.NotSupportedBySource(self.source, 'attachments')

        message = utils.get_message(local_id).as_leaf_class()

        if not isinstance(message, SyntheticMessage):
            raise erors.InvalidArgument('message', f'Unexpected message type for {message}')

        if message.conversation.source.id != self.source.id:
            raise errors.InvalidArgument('message', f'Invalid source id for {message}, expected {self.source}')

        if text is not None and text != message.text:
            logging.warning(f'Changing text for message {message.id} from {message.text} to {text}')
            message.text = text

        if author is not None and author.id != message.author.id:
            logging.warning(f'Changing author for message {message.id} from {author} to {author}')
            message.author = author

        if reply_to != message.reply_to:
            logging.warning(f'Changing reply_to for message {message.id} from {message.reply_to} to {reply_to}')
            message.reply_to = reply_to

        if remote_id != message.remote_id and message.remote_id is not None:
            logging.warning(f'Changing remote_id for message {message.id} from {message.remote_id} to {remote_id}')

        message.remote_id = remote_id

        if timestamp is not None and timestamp != message.timestamp:
            logging.warning(f'Changing timestamp for message {message.id} from {message.timestamp} to {timestamp}')
            message.timestamp = timestamp

        if message.sent_timestamp is not None and message.sent_timestamp != sent_timestamp:
            logging.warning(f'Changing sent_timestamp for message {message.id} from {message.sent_timestamp} to {sent_timestamp}')

        message.sent_timestamp = sent_timestamp

        if conversation.id != message.conversation.id:
            logging.warning(f'Changing conversation for message {message.id} from {message.conversation} to {conversation}')
            message.conversation = conversation


        try:
            message.save()
        except IntegrityError as e:
            raise errors.AlreadyExists('A conflicting message already exists') from e
        logging.info(f'Saved message: {message}')

        return message

    def __enter__(self, *args, **kwargs): # No-op
        return self

    def __exit__(self, *args, **kwargs): # No-op
        pass
