import logging
import backoff
import traceback
from threading import Thread
from xocial.backends import facebook
from xocial import models, errors, log
from xocial.backends.pool import BackendPool

pool = BackendPool()

def acquire_backend(source):
    return pool.acquire(source)

def release_backend(backend):
    return pool.release(backend)

def inject_backend(backend):
    return pool.inject(backend)

class BackendRunner:
    def __init__(self):
        self.threads = {}

    @backoff.on_exception(backoff.expo, Exception, max_tries=20)
    def run_backend_impl(self, source: models.Source):
        try:
            with BackendPool.instanciate(source) as backend:
                logging.info(f"Initialized backend for source {source.id}")

                source.status = models.SourceStatus.Running
                source.save()

                backend.listen()
        except Exception as e:
            logging.error(f"Backend for source {source.id} failed")
            log.log_error(e)

            if source.errors.count() > 100:
                logging.error(f"Maximum error count exceeded for source {source.id}, dropping error")
            else:
                error = models.Error()
                error.code = type(e).__name__
                error.details = traceback.format_exc()
                error.save()

                source.errors.add(error)

            source.status = models.SourceStatus.SoftFailed
            source.save()

            raise

    def run_backend(self, source):
        try:
            logging.info(f"Initializing backend for source {source.id}")

            source.status = models.SourceStatus.Initializing
            source.save()

            self.run_backend_impl(source)
        except Exception as e:
            source.status = models.SourceStatus.HardFailed

            source.save()

            logging.error(f"Backend for source {source.id} is hard failed: {traceback.format_exc()}")

    def start_backends(self):
        logging.info(f"Starting backends")

        sources = [e for e in models.Source.objects.all() if not isinstance(e.as_leaf_class(), models.synthetic.SyntheticSource)]

        self.threads={e.id: Thread(target=BackendRunner.run_backend, args=(self, e,)) for e in sources}

        for e in self.threads:
            self.threads[e].daemon = True
            self.threads[e].start()
