import logging
from xocial import models
from xocial.models import facebook, synthetic
from xocial.backends.facebook import FacebookBackend
from xocial.backends.synthetic import SyntheticBackend
from threading import Lock

class BackendInstance:
    def __init__(self, instance):
        self.instance = instance
        self.lock = Lock()

class BackendPool:
    def __init__(self):

        self.backends = {}
        self.lock = Lock()

    def instanciate(source: models.Source):
        logging.info(f"Instanciate backend for source: {source.id}")

        source = source.as_leaf_class()

        if type(source) is facebook.FacebookSource:
            return FacebookBackend(source)
        elif type(source) is synthetic.SyntheticSource:
            return SyntheticBackend(source)
        else:
            raise RuntimeError(f"Unexpected backend type on source '{source.id}': '{type(source)}'")


    def acquire(self, source: models.Source):
        with self.lock:
            if not source.id in self.backends:
                self.backends[source.id] = BackendInstance(BackendPool.instanciate(source))

            self.backends[source.id].lock.acquire()
            logging.info(f"Acquired backend for source {source.id}")

            return self.backends[source.id].instance

    def release(self, backend):
        logging.info(f"Released backend for source {backend.source.id}")

        self.backends[backend.source.id].lock.release()

    def inject(self, backend):
        with self.lock:
            self.backends[backend.source.id] = BackendInstance(backend)
