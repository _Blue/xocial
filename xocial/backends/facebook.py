import logging
import json
import fbchat
import time
import random
import traceback
import os
import backoff
import django.utils.timezone
from io import StringIO
from itertools import chain
from xocial import log, errors, settings
from xocial.models import *
from xocial.models.transaction import transaction, push_transaction_task
from xocial.models.facebook import *
from datetime import datetime, timezone
from django import db
from datetime import datetime, timezone, timedelta
from django.db import utils
from .attachments import AttachmentDownloader
from . import common

RECENT_THREAD_LIMIT = 20
MESSAGE_FETCH_LIMIT = 20
OLD_CONTACTS_FETCH = 12
MQTT_RESET_MINIMUM = 60 * 60

def read_fb_timestamp(timestamp):
    return datetime.utcfromtimestamp(int(timestamp) / 1000).replace(tzinfo=timezone.utc)

def to_fb_timestamp(datetime):
    return time.mktime(datetime.timetuple()) * 1000

class FacebookBackend(fbchat.Client):

    def __init__(self, source):
        self.source = source.as_leaf_class()

        self.init_downloader()

        super().__init__(source.user_email, ' ', session_cookies=json.loads(source.credential), max_tries=1)

        self.update_user_id()
        self.create_me_if_missing()

    def init_downloader(self):
        self.downloader = AttachmentDownloader(self.source)
        self.downloader.start()


    def create_me_if_missing(self):
        self.me_contact = Contact.objects.filter(is_me=True).first()
        if self.me_contact and any(e.source.id == self.source.id for e in self.me_contact.sources.all()):
            return

        logging.info("'Me' contact not found, creating it now")

        self.me_contact = self.process_discovered_contact(self.fetchUserInfo(self.uid)[self.uid])
        self.me_contact.is_me = True
        self.me_contact.save()

        for e in self.me_contact.sources.all():
            e.is_me = True
            e.save()


    def update_user_id(self):
        if self.source.facebook_user_id == str(self.uid):
            return

        logging.info(f"Updating facebook_user_id from {self.source.facebook_user_id} to {self.uid}")
        self.source.facebook_user_id = self.uid
        self.source.save()

    def __enter__(self):
        return self


    def __exit__(self, exc_type, exc_value, traceback):
        if self.listening:
            self.stopListening()

        logging.info(f"Backend for source: {self.source.id} stopped")

    def save_error(self, object: DataModel, exception):
        common.save_error(object, exception)

    def fetch_contacts(self):
        logging.info(f"Fetching contacts for source {self.source.display_name}")

        def process(users, force_update):
            for e in users:
                try:
                    with transaction():
                        contact = self.process_discovered_contact(e)

                        if force_update:
                            contact.sources.filter(source=self.source).first().mark_updated()

                    logging.debug(f"Processed contact {e.uid}")
                except Exception as ex:
                    logging.error(f"Failed to process contact {e.uid}")
                    log.log_error(ex)
                    self.save_error(self.source, ex)

        fetched_users = self.fetchAllUsers()
        process(fetched_users, False)

        # Not all users are fetched by the above call. This blocks ensures that every contact is eventually refreshed
        other_users = [e for e in FacebookSourceContact.objects.filter(source=self.source)
                       if not any(c.uid == e.facebook_user_id for c in fetched_users) and not e.has_fetch_error]

        old_users = sorted(other_users, key= lambda e: e.last_updated)[:OLD_CONTACTS_FETCH]

        logging.info(f'Fetching {len(old_users)} old contact profiles')

        fetched_users = []
        for e in old_users:
            try:
                fetched_users.append(self.fetchThreadInfo(e.facebook_user_id)[e.facebook_user_id])
            except Exception as ex:
                logging.error(f"Failed to fetch old contact {e.facebook_user_id}, setting has_fetch_error to True")
                log.log_error(ex)
                self.save_error(self.source, ex)

                e.has_fetch_error = True # Record the fetch error so we don't attempt another fetch later
                e.save()

        process(fetched_users, True)

    def fetch_all_conversations(self, cooldown=30, fetch_limit=20):
        types = [fbchat.ThreadLocation(e.value) for e in fbchat.ThreadLocation]

        for type in types:
            threads = self.fetchThreads(thread_location=type, limit=None)

            for e in threads:
                if self.process_discovered_thread(e, fetch_limit):
                    time.sleep(cooldown * random.random())


    def fetch_threads(self, limit=RECENT_THREAD_LIMIT):
        types = [fbchat.ThreadLocation(e.value) for e in fbchat.ThreadLocation]

        threads = [self.fetchThreadList(thread_location=e, limit=limit) for e in types]

        # Using an array expression to make sure all entries are evaluated
        return any([self.process_discovered_thread(e) for e in chain.from_iterable(threads)])

    def process_discovered_thread(self, thread, fetch_limit=MESSAGE_FETCH_LIMIT) -> bool:
        try:
            return self.process_discovered_thread_impl(thread, fetch_limit)
        except Exception as e:
            logging.error(f"Failed to process thread {thread.uid}")
            log.log_error(e)
            self.save_error(self.source, e)

            return False

    def process_discovered_thread_impl(self, thread, fetch_limit) -> bool:
        conversation = self.get_or_create_conversation_from_id(thread.uid, thread.type)

        conversation.refresh_from_db() # Required as process_message can change conversation.last_read_message()
        conversation.display_name = thread.name or ''
        conversation.display_image = thread.photo
        conversation.save()

        if fetch_limit == 0 or not self.is_thread_updated(thread, conversation):
            return False

        conversation.facebook_last_message_timestamp = read_fb_timestamp(thread.last_message_timestamp)
        conversation.save()

        for e in sorted(self.fetchThreadMessages(thread.uid, limit=fetch_limit), key=lambda e: e.timestamp):
            self.process_message(e, thread.uid, thread.type)

        return True

    def create_conversation(self, members: list, original_message: str) -> FacebookConversation:
        thread_id = self.create_group(original_message, [e.facebook_user_id for e in members] + [self.uid])

        return self.get_or_create_conversation_from_id(thread_id, fbchat.ThreadType.GROUP)

    def add_contact_to_conversation(self, conversation: FacebookConversation, contact: FacebookSourceContact) -> FacebookConversation:
        conversation = conversation.as_leaf_class()

        # Add the user to the group
        self.add_users_to_group(conversation.facebook_conversation_id, contact.facebook_user_id)

        # Update group in DB
        return self.refresh_conversation(conversation)

    def refresh_conversation(self, conversation: FacebookConversation) -> FacebookConversation:
        logging.debug(f"Updating conversation: {conversation.id}")


        thread = self.fetchThreadInfo(conversation.facebook_conversation_id)[conversation.facebook_conversation_id]
        conversation.display_name = thread.name or ''
        conversation.display_image = thread.photo

        self.refresh_conversation_members(thread, conversation)

        return conversation


    def is_thread_updated(self, thread, conversation: FacebookConversation):
        timestamp = read_fb_timestamp(thread.last_message_timestamp)
        if not conversation.facebook_last_message_timestamp or timestamp > conversation.facebook_last_message_timestamp:
            logging.debug(f"Conversation {conversation.id} is out of date {timestamp} > { conversation.facebook_last_message_timestamp or 'None'}, fetching new messages")
            return True

        return False

    def contact_display_name(self, contact):
        if type(contact) is fbchat.Page:
            return contact.name

        name = (contact.first_name or '') + ' ' + (contact.last_name or '')
        name = contact.name or name or contact.nickname

        return name if name != "Facebook User" else f"Facebook User ({contact.uid})" # Give unique names to FB placeholders contacts

    def find_matching_contact(self, source_contact):
        matches = Contact.objects.filter(display_name=source_contact.display_name).all()

        if len(matches) > 1:
            raise RuntimeError(f"Failed to find an unique match for source contact {source_contact.id}, name: {source_contact.display_name}")

        return matches.first()

    def create_new_contact(self, source_contact):
        logging.debug(f"Creating new contact from source contact {source_contact.id}")

        return Contact.objects.create(
                display_name=source_contact.display_name,
                display_image=source_contact.display_image,
                is_me=source_contact.is_me)

    def process_discovered_contact(self, contact):
        source_contact, created = FacebookSourceContact.objects.get_or_create(
                facebook_user_id=contact.uid,
                source=self.source)

        source_contact.display_name = self.contact_display_name(contact)
        source_contact.source = self.source
        source_contact.display_image = contact.photo
        source_contact.is_me = self.uid == contact.uid

        source_contact.save() # Update any stale field

        if created:
            logging.debug(f"Created facebook contact {contact.uid}")

        return self.process_contact_impl(source_contact)

    def process_contact_impl(self, source_contact):
        contact = (self.find_contact_from_source(source_contact)
                   or self.find_matching_contact(source_contact)
                   or self.create_new_contact(source_contact))

        if not any(e for e in contact.sources.all() if e.id == source_contact.id):
            logging.debug(f"Added source contact {source_contact.id} to contact {contact.id}")
            contact.sources.add(source_contact)

        self.update_contact_from_source(contact, source_contact)
        contact.save()

        return contact

    def create_error_contact(self, facebook_user_id):
        source_contact, created = FacebookSourceContact.objects.get_or_create(
                facebook_user_id=facebook_user_id,
                source=self.source)

        source_contact.display_name = f'Facebook error user {facebook_user_id}'
        source_contact.source = self.source
        source_contact.display_image = None
        source_contact.is_me = False

        source_contact.save() # Update any stale field

        if created:
            logging.debug(f"Created error facebook contact {facebook_user_id}")

        return self.process_contact_impl(source_contact)


    def update_contact_from_source(self, contact: Contact, source: SourceContact):
        if len(contact.sources.all()) == 1: # Simple case: only one source
            if source.display_name != contact.display_name:
                logging.debug(f"Updating contact {contact.id} display name ('{contact.display_name}' -> '{source.display_name}')")
                contact.display_name = source.display_name

            if source.display_image != contact.display_image:
                logging.debug(f"Updating contact {contact.id} display image('{contact.display_image}' -> '{source.display_image}')")

                contact.display_image= source.display_image

        else:
            if not contact.display_image and source.display_image:
                logging.debug(f"Updating contact {contact.id} display image ('{contact.display_image}' -> '{source.display_image}')")
                contact.display_image = source.display_image

    def find_contact_from_source(self, source_contact):
        matched = Contact.objects.filter(sources=source_contact).all()

        assert len(matched) < 2

        return matched[0] if matched else None

    def process_message_reactions(self, message: FacebookMessage, message_object: fbchat.Message):
        def has(emoji, contact):
            return any(e.as_leaf_class().emoji == emoji for e in message.reactions.filter(author=contact))

        for e in message_object.reactions:
            author = self.get_or_create_contact(e)
            emoji = message_object.reactions[e].value

            if not has(emoji, author):
                reaction = FacebookMessageReaction(author=author, emoji=emoji)
                reaction.save()
                message.reactions.add(reaction)

        for e in message.reactions.all(): # Delete any stale reaction
            source_contact = next(e.as_leaf_class() for e in e.author.sources.filter(source=self.source))
            uid = source_contact.facebook_user_id
            emoji = e.as_leaf_class().emoji

            if not uid in message_object.reactions or message_object.reactions[uid].value != emoji:
                message.reactions.remove(e)

                logging.debug(f'Removed stale reaction from message {message.id}, by {uid}')

    # Parrallel message processing can cause integrity errors
    @backoff.on_exception(backoff.constant, (fbchat.FBchatException, utils.IntegrityError), max_tries=5, interval=0.5)
    def process_message(self, message_object, thread_id, thread_type):
        with transaction():
            message = FacebookMessage.objects.select_for_update().filter(facebook_message_id=message_object.uid).first()

            return self.process_message_impl(message, message_object, thread_id, thread_type)

    def process_message_impl(self, message: FacebookMessage, message_object, thread_id, thread_type):
        author = self.get_or_create_contact(message_object.author)

        if not message:
            logging.debug("Processing new message: {}".format(message_object.uid))
            message = FacebookMessage(facebook_message_id=message_object.uid)
        else:
            logging.warn("Message {} already in DB, updating".format(message_object.uid))

        message.timestamp = read_fb_timestamp(message_object.timestamp)
        message.author = author
        message.conversation = self.get_or_create_conversation_from_id(thread_id, thread_type)
        message.text = message_object.text or ''
        message.unsent = message.unsent

        if message_object.reply_to_id or message_object.replied_to:
            try:
                if message_object.replied_to:
                    message.reply_to = self.process_message(
                            message_object.replied_to,
                            message.conversation.facebook_conversation_id,
                            thread_type)
                else:
                    message.reply_to = self.fetch_message(message_object.reply_to_id, message.conversation.facebook_conversation_id, thread_type)
            except Exception as e:
                logging.error(f"Failed to fetch original message (replied to) with id: {message_object.reply_to_id}, discarding")
                log.log_error(e)
                self.save_error(message.conversation, e)

        message.save()

        self.process_message_reactions(message, message_object)

        # Update conversation read marker if needed
        self.update_last_read_message(message, is_read=message_object.is_read)

        if not message.author.is_me:
            self.update_read_marker(message.conversation, message.author, message, message.timestamp)

        for i, e in enumerate(message_object.attachments + ([message_object.sticker] if message_object.sticker else [])):
            try:
                if not e.uid:
                    logging.warning(f'Attachment {i} for message {message_object.uid} has no ID, generating a synthetic one')
                    e.uid = f'{message_object.uid}-a-{i}'

                message.attachments.add(self.process_attachment(e))
            except Exception as ex:
                logging.error(f'Failed to process attachment {e.uid} for message {message_object.uid}')
                log.log_error(ex)
                self.save_error(message.conversation, ex)

        message.save()

        self.refresh_conversation_if_needed(message.conversation, message_object.author)

        return message

    def refresh_conversation_if_needed(self, conversation: FacebookConversation, author_id: str):
        def is_author(source_contact: SourceContact) -> bool:
            source_contact = source_contact.as_leaf_class()

            return isinstance(source_contact, FacebookSourceContact) and source_contact.facebook_user_id == str(author_id)

        def is_sender_in_conversation(contacts: list):
            return any(any(is_author(c) for c in e.sources.all()) for e in contacts)

        # Opportunistic conversation refresh trigger
        if not (author_id == self.uid or is_sender_in_conversation(conversation.members.all()) or is_sender_in_conversation(conversation.gone_members.all())):
            logging.debug(f"New author {author_id} in conversation {conversation.id}, refreshing members")
            conversation = self.refresh_conversation(conversation)

            if not is_sender_in_conversation(conversation.members.all()):
                logging.debug(f"Author {author_id} not in conversation {conversation.id} after refresh, marking as gone member")
                conversation.gone_members.add(self.get_or_create_contact(author_id))
                conversation.save()


    def update_last_read_message(self, message, is_read):
        if (message.author.is_me or is_read) and (not message.conversation.last_read_message or message.conversation.last_read_message.timestamp < message.timestamp):
            logging.debug(f"Updating conversation {message.conversation.id} with last_read_message = {message.id}")

            message.conversation.last_read_message = message
            message.conversation.save()


    # Parrallel message processing can cause integrity errors
    @backoff.on_exception(backoff.constant, (fbchat.FBchatException, utils.IntegrityError), max_tries=3, interval=1)
    def process_attachment(self, attachment: fbchat.Attachment) -> Attachment:
        kwargs = {}
        model_type = None

        if isinstance(attachment, fbchat.ShareAttachment):
            model_type = FacebookUrlAttachment
            kwargs['url'] = attachment.url
            kwargs['original_url'] = attachment.original_url
            kwargs['description'] = attachment.description or ""
            kwargs['title'] = attachment.title
        elif isinstance(attachment, fbchat.Sticker):
            model_type = FacebookStickerAttachment
            kwargs['url'] = attachment.url
            kwargs['preview_url'] = attachment.url
            kwargs['facebook_sticker_pack_id'] = getattr(attachment, "pack", None) # This attribute is sometimes missing
            kwargs['facebook_label'] = getattr(attachment, "label", "") # This attribute appears to be missing on old attachments
        elif isinstance(attachment, fbchat.LocationAttachment):
            model_type = FacebookLocationAttachment
            kwargs['latitude'] = attachment.latitude
            kwargs['longitude'] = attachment.longitude
            kwargs['image_url'] = attachment.image_url
            kwargs['map_url'] = attachment.url
        elif isinstance(attachment, fbchat.FileAttachment):
            model_type = FacebookFileAttachment
            kwargs['url'] = attachment.url
            kwargs['name'] = attachment.name
            kwargs['size'] = attachment.size
        elif isinstance(attachment, fbchat.AudioAttachment):
            model_type = FacebookAudioAttachment
            kwargs['url'] = attachment.url
            kwargs['name'] = attachment.filename
            kwargs['length_ms'] = attachment.duration
        elif isinstance(attachment, fbchat.ImageAttachment):
            model_type = FacebookImageAttachment
            kwargs['url'] = self.fetchImageUrl(attachment.uid )
            kwargs['preview_url'] = attachment.large_preview_url
        elif isinstance(attachment, fbchat.VideoAttachment):
            model_type = FacebookVideoAttachment
            kwargs['url'] = attachment.large_image_url # TODO: fetch proper video URL
            kwargs['length_ms'] = attachment.duration
        else:
            raise RuntimeError(f"Unexpected attachment type {type(attachment)}, for id: {attachment.uid}")

        object = model_type.objects.filter(facebook_attachment_id=attachment.uid).first()

        if not object:
            logging.debug(f'Processing new attachment: {attachment.uid} (type: {type(attachment).__name__})')
            object = model_type(facebook_attachment_id=attachment.uid)

        for e in kwargs:
            setattr(object, e, kwargs[e])

        object.save()

        if self.should_download_attachment(object):
            push_transaction_task(lambda: self.downloader.add(object))
        return object

    def fetch_message_object(self, facebook_message_id, facebook_conversation_id, thread_type, use_recents=True) -> fbchat.Message:
        logging.debug(f"Manually fetching message {facebook_message_id} for conversation {facebook_conversation_id}")

        message_object = None

        if use_recents: # Work-around weird bug where fetchMessageInfo returns attachements with null Uid's
            message_object = next((e for e in self.fetchThreadMessages(facebook_conversation_id, limit=MESSAGE_FETCH_LIMIT) if e.uid == facebook_message_id), None)

        return message_object or self.fetchMessageInfo(facebook_message_id, facebook_conversation_id)

    def fetch_message(self, facebook_message_id, facebook_conversation_id, thread_type, use_recents=True, force=False):
        if not force:
            message = FacebookMessage.objects.filter(facebook_message_id=facebook_message_id).first()

            if message:
                return message

        message_object = self.fetch_message_object(facebook_message_id, facebook_conversation_id, thread_type, use_recents)

        return self.process_message(
                message_object,
                facebook_conversation_id,
                thread_type)


    def send_message(self, text: str, conversation: Conversation, attachments: list=None, reply_to: Message=None) -> FacebookMessage:
        conversation = conversation.as_leaf_class()

        message = fbchat.Message(text=text)

        if reply_to:
            message.reply_to_id = reply_to.as_leaf_class().facebook_message_id

        logging.debug(f"Sending message '{text}' to conversation {conversation.id}")

        thread_type = fbchat.ThreadType[conversation.facebook_conversation_type.upper()]

        self.setActiveStatus(True) # Lame attempt to prevent FB suspicious activity monitor trigger

        try:
            if attachments:
                paths = [os.path.join(settings.MEDIA_ROOT, str(e.id), e.file_name) for e in attachments]
                message_id = self.sendLocalFiles(paths, text, conversation.facebook_conversation_id, thread_type)
            else:
                message_id = self.send(message, conversation.facebook_conversation_id, thread_type)
        except:
            self.setActiveStatus(False)
            raise


        return self.fetch_message(message_id, conversation.facebook_conversation_id, thread_type, use_recents=True)

    def react_to_message(self, message: FacebookMessage, reaction_object: dict) -> FacebookMessage:
        reaction = reaction_object.get('emoji', None)

        if not reaction:
            raise errors.InvalidArgument('emoji field in message from reaction object')

        fb_reaction = next((e for e in fbchat.MessageReaction if e.value == reaction), None)
        if not fb_reaction:
            raise errors.InvalidArgument('emoji field in message is not supported by this backend')

        self.reactToMessage(message.facebook_message_id, fb_reaction)

        thread_type = fbchat.ThreadType[message.conversation.as_leaf_class().facebook_conversation_type.upper()]

        return self.fetch_message(
                message.facebook_message_id,
                message.conversation.as_leaf_class().facebook_conversation_id,
                thread_type,
                force=True)


    def mark_message_as_read(self, message):
        conversation = message.conversation.as_leaf_class()

        timestamp = to_fb_timestamp(message.timestamp) + 1000
        logging.debug(f"Marking conversation {conversation.id} as read")

        self.markAsRead([conversation.facebook_conversation_id]) # TODO: Set proper timestamp once fbchat V2 is out

    def get_or_create_contact(self, facebook_contact_id, user: fbchat.Thread = None):
        source_contact = FacebookSourceContact.objects.filter(
                                facebook_user_id=facebook_contact_id,
                                source=self.source).first()

        if source_contact: # Contact already in DB, not need to make a API call to get user details
            contact = self.find_contact_from_source(source_contact)
            if contact: # We might have a source contact without a contact, if that's the case, simply create it by running process_discovered_contact
                return contact

        # fetchThreadInfo works for both user and pages
        try:
            user = user or self.fetchThreadInfo(facebook_contact_id)[facebook_contact_id]
            return self.process_discovered_contact(user)
        except Exception as e:
            logging.warn(f'Error while fetching facebook user {facebook_contact_id}')
            self.save_error(self.source, e)

            return self.create_error_contact(facebook_contact_id) # Avoid failing the whole flow if one contact can't be fetched


    def get_or_create_conversation_from_id(self, thread_id, thread_type) -> FacebookConversation:
        conversation = FacebookConversation.objects.filter(facebook_conversation_id=thread_id,
                                                           source=self.source).first()

        if conversation:
            return conversation

        if thread_type == fbchat.ThreadType.PAGE:
            thread = self.fetchPageInfo(thread_id)[thread_id]
        else:
            thread = self.fetchThreadInfo(thread_id)[thread_id]

        # first look for a matching synthetic thread
        conversation = None
        if type(thread) is fbchat.User:
            conversation = FacebookConversation.objects.filter(
                    synthetic=True,
                    facebook_conversation_id=self.generate_synthsyetic_conversation_id(thread.uid)).first()

            if conversation: # Matching conversation is found. Merge the two threads
                conversation.synthetic = False
                conversation.facebook_conversation_id = thread_id
                created = True

                logging.debug(f'Merged conversation {thread_id} with synthetic conversation {conversation.id}')


        if not conversation:
            conversation = FacebookConversation(
                                    facebook_conversation_id=thread_id,
                                    facebook_conversation_type=thread_type.name.lower(),
                                    source=self.source)
            conversation.save()

        logging.debug("Processing new conversation: {}".format(thread_id))

        conversation.display_name = thread.name or ''
        conversation.display_image = thread.photo
        conversation.facebook_conversation_type = thread_type.name.lower()

        return self.refresh_conversation_members(thread, conversation)

    def refresh_conversation_members(self, thread, conversation: FacebookConversation):
        if type(thread) is fbchat.User:
            conversation.members.add(self.get_or_create_contact(thread.uid, thread))
        elif type(thread) is fbchat.Group:
            for m in [self.get_or_create_contact(e) for e in thread.participants]:
                conversation.members.add(m)
        elif type(thread) is fbchat.Page:
            conversation.members.add(self.get_or_create_contact(thread.uid, thread))
        else:
            raise RuntimeError("Thread {} has unsuported type: {}".format(thread.uid, thread.type))

        # Make sure 'me' is always a member
        conversation.members.add(self.me_contact)
        conversation.save()

        return conversation

    def fetch_all_messages(self, fetch_limit=1000):
        for e in FacebookConversation.objects.filter(source=self.source).all():
            logging.debug(f"Fetching all messages for conversation: {e.id}")

            try:
                self.fetch_all_conversation_messages(e, fetch_limit)
            except Exception as ex:
                logging.error(f"Error fetching all messages for conversation: {e.id}")
                log.log_error(ex)
                self.save_error(e, ex)

        logging.debug(f"Successfully fetched all messages")

    def fetch_all_conversation_messages(self, conversation: FacebookConversation, fetch_limit=1000, cooldown=10):
        conversation = conversation.as_leaf_class()

        thread_type = next(e for e in fbchat.ThreadType if e.name == conversation.facebook_conversation_type.upper())
        oldest_message = next(iter(FacebookMessage.objects.filter(conversation=conversation).order_by('timestamp')), None)

        def fetch(before):
            return self.fetchThreadMessages(thread_id=conversation.facebook_conversation_id, before=before, limit=fetch_limit)

        before = to_fb_timestamp(oldest_message.timestamp) if oldest_message else None

        messages = fetch(before)
        if not messages:
            logging.warn(f"No messages fetched for conversation {conversation.id}")
            return

        while len(messages) >= fetch_limit:
            for e in messages:
                self.process_message(e, conversation.facebook_conversation_id, thread_type)

            time.sleep(cooldown * random.random())
            messages = fetch(min(e.timestamp for e in messages))

        for e in messages:
            self.process_message(e, conversation.facebook_conversation_id, thread_type)

    def onMessage(self, author_id, message, message_object, thread_id, thread_type, **kwargs):
        logging.debug("Received message '{}' from thread: '{}' from author '{}'".format(message_object.uid, thread_id, author_id))

        try:
            self.process_message(message_object, thread_id, thread_type)
        except Exception as e:
            logging.error("Failed to process message '{}'".format(message_object.uid))
            log.log_error(e)
            self.save_error(self.source, e)


    def on_conversation_seen_impl(self, thread_id, thread_type, seen_ts):
        conversation = self.get_or_create_conversation_from_id(thread_id, thread_type)

        message = FacebookMessage.objects.filter(
                conversation=conversation,
                timestamp__lte=read_fb_timestamp(seen_ts)).order_by('-timestamp').first()

        if not message:
            logging.debug(f"Conversation {conversation.id}: no message found before timestamp {seen_ts}, not updating read marker")
            return

        self.update_last_read_message(message, is_read=True)


    def onMarkedSeen(self,
            threads=None,
            seen_ts=None,
            ts=None,
            metadata=None,
            msg=None):

        super().onMarkedSeen(threads, seen_ts, ts, metadata, msg)

        for (id, type) in threads:
            try:
                self.on_conversation_seen_impl(id, type, seen_ts)
            except Exception as e:
                logging.error(f"Failed to process update read marker for thread '{id}'")
                log.log_error(e)
                self.save_error(self.source, e)

    def update_read_marker(self, conversation: FacebookConversation, contact: Contact, message: FacebookMessage, timestamp) -> ReadMarker:
        marker = next((e for e in conversation.read_markers.filter(contact=contact)), ReadMarker(contact=contact, timestamp=timestamp, message=message))
        if message.timestamp < marker.message.timestamp:
            logging.warn(f'New message timestamp is older than current message timestamp. Not updating read marker ({message.timestamp} < {marker.message.timestamp})')
            return

        marker.message = message
        marker.timestamp = timestamp
        marker.save()

        conversation.read_markers.add(marker)

    def onMessageSeen(self,
            seen_by=None,
            thread_id=None,
            thread_type=fbchat.ThreadType.USER,
            seen_ts=None,
            ts=None,
            metadata=None,
            msg=None):

        super().onMessageSeen(seen_by, thread_id, thread_type, seen_ts, ts, metadata, msg)

        try:
            self.on_message_seen_impl(seen_by, thread_id, thread_type, seen_ts)
        except Exception as e:
            log.log_error(e)
            self.save_error(self.source, e)


    def on_message_seen_impl(self, seen_by, thread_id, thread_type, seen_ts):
        conversation = self.get_or_create_conversation_from_id(thread_id, thread_type)
        contact = self.get_or_create_contact(seen_by)
        timestamp = read_fb_timestamp(seen_ts)

        message = FacebookMessage.objects.filter(timestamp__lte=timestamp, conversation=conversation).order_by('-timestamp').first()
        if not message:
            logging.warn(f'Not updating read marker. No message found after timestamp {timestamp} in conversation {conversation.id}')
            return

        self.update_read_marker(conversation, contact, message, timestamp)
        logging.debug(f'Updated read marker for contact {seen_by} in conversation {conversation.id} to message {message.id}')

        self.refresh_conversation_if_needed(conversation, seen_by)
        return message



    def refresh_reactions(self, mid, author_id, thread_id, thread_type):
        message = self.fetch_message_object(mid, thread_id, thread_type)
        self.process_message(message, thread_id, thread_type)

    def onReactionAdded(self, mid=None, reaction=None, author_id=None, thread_id=None, thread_type=None, ts=None, msg=None):
        logging.debug(f'Reaction added to message: {mid}, thread: {thread_id}')
        self.refresh_reactions(mid, author_id, thread_id, thread_type)

    def onReactionRemoved(self, mid=None, author_id=None, thread_id=None, thread_type=None, ts=None, msg=None):
        logging.debug(f'Reaction removed from message: {mid}, thread: {thread_id}')
        self.refresh_reactions(mid, author_id, thread_id, thread_type)

    def onMessageError(self, exception=None, msg=None):
        logging.warn(f'Listening error while processing message: {exception}, {msg}')
        self.save_error(self.source, exception)

    def onUnknownMesssageType(self, msg=None):
        logging.warn(f'Unknown message type received: {msg}')

    def onListenError(self, exception=None):
        logging.warn(f'Listening error: {exception}')

        if exception:
            self.save_error(self.source, exception)

    def generate_synthsyetic_conversation_id(self, facebook_user_id: str):
        return f'xocial-{facebook_user_id}-friend-request'

    def generate_synthetic_chat(self, contact: Contact, facebook_user_id: str):
        source_contact = next(e for e in contact.sources.all() if e.as_leaf_class().facebook_user_id == facebook_user_id)
        assert source_contact

        generated_id = self.generate_synthsyetic_conversation_id(source_contact.as_leaf_class().facebook_user_id)


        conversation = FacebookConversation(source=self.source,
                                            display_name=contact.display_name,
                                            display_image=contact.display_image,
                                            facebook_conversation_id=generated_id,
                                            facebook_conversation_type=fbchat.ThreadType.USER.name.lower(),
                                            synthetic=True)
        conversation.save()

        conversation.members.add(self.me_contact)
        conversation.members.add(contact)
        return conversation

    def find_conversation_or_generate_synthetic(self, contact: Contact, facebook_user_id: str):
        matching_conversations = [e for e in FacebookConversation.objects.filter(source=self.source, members=contact)]
        matching_conversations = [e for e in matching_conversations if set([c.id for c in e.members.all()]) == set([self.me_contact.id, contact.id])]

        if len(matching_conversations) > 1:
            logging.error(f'Unexpected multiple matches for existing conversations with contact {contact.id}')
        elif len(matching_conversations) == 1:
            return matching_conversations[0]

        return self.generate_synthetic_chat(contact, facebook_user_id)


    def onFriendRequest(self, from_id=None, msg=None):
        logging.debug(f'Received friend request from: {from_id}')

        from_id = str(from_id)
        try:
            contact = self.process_discovered_contact(self.fetchUserInfo(from_id)[from_id])
            conversation = self.find_conversation_or_generate_synthetic(contact, from_id)
            count = FacebookMessage.objects.filter(conversation=conversation).count()
            message = FacebookMessage(facebook_message_id=f'{self.generate_synthsyetic_conversation_id(from_id)}-message-{count}',
                                      text=f'Received friend request. Message: {msg}',
                                      author=contact,
                                      timestamp=django.utils.timezone.now(),
                                      conversation=conversation)

            message.save()
        except Exception as e:
            logging.error(f'Failed to process friend request from: {from_id}')
            log.log_error(e)
            self.save_error(self.source, e)

    def onPendingMessage(self, thread_id=None, thread_type=None, metadata=None, msg=None):
        logging.debug(f'Received pending message request on thread: {thread_id}')

        try:
            conversation = self.get_or_create_conversation_from_id(thread_id, thread_type)
            count = FacebookMessage.objects.filter(conversation=conversation).count()
            message = FacebookMessage(facebook_message_id=f'{self.generate_synthsyetic_conversation_id(thread_id)}-message-request-{count}',
                                      text=f'Received conversation request. Metadata: {metadata}, Message: {msg}',
                                      author=self.me_contact,
                                      timestamp=django.utils.timezone.now(),
                                      conversation=conversation)
            message.save()
        except Exception as e:
            logging.error(f'Failed to process message in thread : {thread_id}')
            log.log_error(e)
            self.save_error(self.source, e)

    def refresh(self, refresh_contacts=False):
        logging.info(f"Refreshing facebook backend for source: {self.source.id}")

        try:
            if refresh_contacts:
                self.fetch_contacts()

            return self.fetch_threads()
        except Exception as e:
            log.log_error(e)
            self.save_error(self.source, e)

    def isLoggedIn(self):
        return True # hack to avoid sending request to check if user is logged in

    def poll(self):
        logging.info(f"Starting facebook backend in poll mode for source: {self.source.id}. Poll interval: {self.source.refresh_time} seconds")

        while True:
            self.refresh()
            db.close_old_connections()

            time.sleep(self.source.refresh_time)

    def reset_mqtt(self):
        logging.info('Resetting MQTT connection')
        self.stopListening()
        self.startListening()

    def listen(self):
        if self.source.disable_listen:
            self.poll()
            return

        logging.info(f"Starting facebook backend in listen mode for source: {self.source.id}")

        self.setActiveStatus(False)
        self.startListening()
        self.onListening()

        listen_reset_ts = None
        while True:
            self.source.refresh_from_db()

            # If polling returned new messages, the MQTT connection is probably stale
            if self.refresh():
                if listen_reset_ts and listen_reset_ts < datetime.now():
                    self.reset_mqtt()
                listen_reset_ts = datetime.now() + timedelta(seconds=int(MQTT_RESET_MINIMUM))

            if not self.source.last_sync or self.source.last_sync + timedelta(days=1) < django.utils.timezone.now():
                self.fetch_contacts()
                #self.fetch_all_conversations(fetch_limit=0)

                self.source.last_sync = django.utils.timezone.now()
                self.source.save()

            db.close_old_connections()

            last_refresh = datetime.now()
            while datetime.now() < last_refresh + timedelta(seconds=self.source.refresh_time):
                self.doOneListen()

    def should_download_attachment(self, attachment: Attachment) -> bool:
        assert isinstance(attachment, Attachment)

        return type(attachment.as_leaf_class()) in [FacebookImageAttachment, FacebookStickerAttachment, FacebookFileAttachment, FacebookAudioAttachment, FacebookVideoAttachment]


    def download_all_attachments(self, max_queue=100, cooldown=5):
        messages_with_attachments = []

        for e in FacebookConversation.objects.filter(source=self.source).all():
            messages_with_attachments += FacebookMessage.objects.filter(conversation=e).filter(attachments__isnull=False).distinct().all()

        logging.debug(f'Found {len(messages_with_attachments)} messages to process')


        def needs_download(attachment: Attachment) -> bool:
            download = AttachmentDownload.objects.filter(attachment=attachment).first()

            return not download or download.state not in [models.AttachmentState.Available.value, models.AttachmentState.Failed.value]

        for i, e in enumerate(messages_with_attachments):
            if not any(self.should_download_attachment(a) and needs_download(a) for a in e.attachments.all()):
                continue

            logging.debug(f'Processing message {e.id} ({i} / {len(messages_with_attachments)})')

            conversation = e.conversation.as_leaf_class()
            pulled_message = self.fetch_message(e.facebook_message_id,
                                                conversation.facebook_conversation_id,
                                                thread_type = fbchat.ThreadType[conversation.facebook_conversation_type.upper()],
                                                use_recents=False,
                                                force=True)

            for a in pulled_message.attachments.all():
                if not self.should_download_attachment(a) or not needs_download(a):
                    continue
                self.downloader.add(a.as_leaf_class())
                logging.debug(f'Added attachment {a} for download')

            if self.downloader.queue.qsize() > max_queue:
                logging.debug(f'Syncing with downloader')
                self.downloader.sync()

            time.sleep(cooldown * random.random())
