import requests
import logging
import shutil
import time
from io import StringIO
from threading import Thread, Event
from datetime import timedelta
from queue import Queue
from django.utils import timezone
from xocial.models import *
from xocial.models.transaction import transaction
from xocial.log import log_error
from . import common

import traceback

MAX_ERRORS = 20

class AttachmentDownloader:

    def __init__(self, source):
        self.source = source
        self.queue = Queue()
        self.thread = None

    def add(self, attachment: Attachment):
        self.queue.put(attachment)

        logging.debug(f'Added attachment {attachment} to download queue. Queue size = {self.queue.qsize()}')

    def get_or_create_download(self, attachment: Attachment):
        download = AttachmentDownload.objects.filter(attachment=attachment).first()

        if not download:
            logging.debug(f'Creating AttachmentDownload for Attachment {attachment.id}')
            download = AttachmentDownload(attachment=attachment,
                                          state=AttachmentState.Added.value)
            download.save()

        return download

    def backed_off(self, download: AttachmentDownload):

        return download.last_attempt and download.last_attempt + timedelta(seconds=download.backoff) > timezone.now()

    def start(self):
        assert self.thread is None

        self.thread = Thread(target=AttachmentDownloader.run, args=(self,))
        self.thread.start()

    def stop(self):
        logging.debug(f'Stopping downloader for source {self.source}')
        assert self.thread is not None

        self.queue.put(None)

        self.thread.join()

        logging.debug(f'Stopped downloader for source {self.source}')

        self.thread = None


    def sync(self):
        assert self.thread is not None

        event = Event()
        self.queue.put(event)

        event.wait()

    def run(self):
        logging.info(f'Started downloader for source {self.source}')

        while True:
            try:
                attachment = self.queue.get()

                if attachment is None:
                    return
                elif type(attachment) is Event:
                    attachment.set() # Hack to allow easy testing
                    continue

                download = self.get_or_create_download(attachment)

                if download.state == AttachmentState.Failed.value:
                    logging.debug(f'Download {download} is failed, dropping')
                    continue
                elif self.backed_off(download):
                    self.queue.put(attachment)

                    time.sleep(1) # Ugly, but works at our scale
                else:
                    if not self.download(download):
                        self.queue.put(attachment)
            except Exception as e:
                log_error(e)
                common.save_error(self.source, e)

    def download_impl(self, download: AttachmentDownload):
        download.attachment = download.attachment.as_leaf_class()

        if download.state == models.AttachmentState.Available.value and download.last_download_url == download.attachment.url:
            logging.debug(f'Download {download} for attachment {download.attachment} is available and up to date, discarding')
            return

        download.last_attempt = timezone.now()
        download.state = AttachmentState.Downloading.value
        download.last_download_url = download.attachment.url
        download.save()

        logging.debug(f'Starting download {download} for attachment {download.attachment}, url={download.last_download_url}')

        response = requests.get(download.last_download_url, stream=True)
        response.raise_for_status()

        download.content = response.content
        download.web_content_type = response.headers.get('content-type', None)
        download.state = AttachmentState.Available.value
        download.save()

        download.attachment.save()

    def download(self, attachment: AttachmentDownload):
        try:
            with transaction(): # In case the download is already available, don't invalidate it unless the download fails
                self.download_impl(attachment)
                return True
        except Exception as e:
            log_error(e)

            if attachment.errors.count() > MAX_ERRORS:
                logging.error(f'Object {attachment} has exceeded maximum error count, dropping {type(exception)}')
                return

            details = StringIO()
            traceback.print_exception(type(e), e, e.__traceback__, file=details)

            error = Error()
            error.code = str(type(e).__name__)
            error.details = details.getvalue()
            error.save()

            attachment.errors.add(error)

            attachment.backoff = max(attachment.backoff, 1) * 2
            attachment.state = AttachmentState.BackedOff.value
            attachment.save()

            logging.error(f'Backing off download for attachment {attachment.id}. Backoff={attachment.backoff}')


            return False
