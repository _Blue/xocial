import inspect
from django.db.models import *
from django.contrib import admin
from . import models
from .models import facebook, synthetic
from django.contrib.auth.models import User


for name, obj in {name: obj for (name, obj) in inspect.getmembers(models) + inspect.getmembers(facebook) + inspect.getmembers(synthetic)}.items():
    if inspect.isclass(obj) and not obj is Model and issubclass(obj, Model):

            id_fields = [e.name for e in obj._meta.get_fields() if type(e) in [models.ManyToManyField, models.ForeignKey]]

            searchable_types =[TextField, CharField, FloatField, models.BlobField, URLField, DateTimeField, IntegerField]
            search_fields = [e.name for e in obj._meta.get_fields() if type(e) in searchable_types]

            class AdminClass(admin.ModelAdmin):
                raw_id_fields = id_fields
                search_fields = search_fields

            admin.site.register(obj, AdminClass)


def get_admin(): # Trick to allow admin panel access without authentication
    user, created = User.objects.get_or_create(username='admin')

    if created:
        user.is_superuser = True
        user.save()

    return user


admin.site.has_permission = lambda r: setattr(r, 'user', get_admin()) or True

