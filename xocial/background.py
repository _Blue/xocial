import logging
import psutil
import sys
import traceback
from os import _exit, getpid, getpgid
from time import sleep
from threading import Thread
from django.core.exceptions import AppRegistryNotReady
from django.apps import AppConfig, apps
from xocial import log

def wait_for_db():
    while True:
        try:
            from xocial import models

            for e in models.Source.objects.all():
                e.status = models.SourceStatus.NotStarted
                e.failure_reason = None
                e.save()

            break
        except Exception:
            logging.warning(f"DB not ready, waiting 5 seconds. Exception: {traceback.format_exc()}")
            sleep(5)


class BackendConfig(AppConfig):
    name = 'xocial.background'

    backend_started = False
    thread = None

    def ready(self):
        logging.info(f'Background app is ready, pid: {getpid()}')

        if should_run() and not BackendConfig.backend_started:
            self.start()

    def start(self):
        BackendConfig.backend_started = True

        self.thread = Thread(target=BackendConfig.start_impl, args=(self,))

        self.thread.daemon = True
        self.thread.start()

        logging.info('Background application configured')

    def run_backends(self):
        logging.info("Initialize backends")

        self.backend_started = True

        from xocial.backends import BackendRunner

        BackendRunner().start_backends()


    def start_impl(self):
        from xocial.websocket import app
        app.start()

        wait_for_db()

        try:
            self.run_backends()
        except Exception as e:
            logging.error("Failed to start backends")
            log.log_error(e)
            _exit(1)

def should_run() -> bool:
    if  psutil.Process(getpid()).name() in ["uwsgi", "gunicorn: worker [xocial.wsgi]", 'gunicorn']:
        return True # Deployed with UWSGI, always run

    # Special cases for manage.py based deployments
    if getpid() != getpgid(getpid()): # Only the group leader cannot run backend jobs
        return False


    blacklisted_modes = ['test', 'makemigrations', 'migrate', 'shell']

    return not any(e in sys.argv for e in blacklisted_modes)
