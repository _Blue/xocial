class Error(Exception):
    def __init__(self, http_code, *args, **kwargs):
        self.http_code = http_code
        super().__init__(*args, **kwargs)

    @property
    def HttpCode(self):
        return self.http_code

class SourceNotFound(Error):
    def __init__(self, source_id: str):
        super().__init__(404, f"Source {source_id} not foud")

class NotSupportedBySource(Error):
    def __init__(self, source_id: str, feature: str):
        super().__init__(404, f"Source {source_id} does not support feature: {feature}")

class ConversationNotFound(Error):
    def __init__(self, id: str):
        super().__init__(404, f"Conversation {id} not foud")

class UploadedAttachmentNotFound(Error):
    def __init__(self, id: str):
        super().__init__(404, f"Attachment {id} not foud")

class ContactNotFound(Error):
    def __init__(self, id: str):
        super().__init__(404, f"Contact {id} not foud")

class SourceContactNotFound(Error):
    def __init__(self, id: str):
        super().__init__(404, f"SourceContact {id} not foud")

class ObjectNotFound(Error):
    def __init__(self, id: str):
        super().__init__(404, f"Object {id} not foud")

class MessageNotFound(Error):
    def __init__(self, id: str):
        super().__init__(404, f"Message {id} not foud")

class SourceNotFound(Error):
    def __init__(self, id: str):
        super().__init__(404, f"Source {id} not foud")

class ErrorNotFound(Error):
    def __init__(self, id: str):
        super().__init__(404, f"error {id} not foud")

class InvalidRequest(Error):
    def __init__(self, message: str):
        super().__init__(400, message)

class MissingArgument(InvalidRequest):
    def __init__(self, name: str):
        super().__init__(f"Argument {name} is missing")

class InvalidArgumentType(InvalidRequest):
    def __init__(self, name: str, expected: str, actual: str):
        super().__init__(f"Argument {name} has type {actual}, but {expected} was expected")

class InvalidArgument(InvalidRequest):
    def __init__(self, name: str, message: str):
        super().__init__(f"Argument {name} has invalid value: {message}")

class BackendError(Error):
    def __init__(self, backend):
        super().__init__(500, f"An error was returned by the backend. Backend type: {type(backend)}, source: {backend.source.id}")

class AttachmentUnavailable(Error):
    def __init__(self, attachment_id: str):
        super().__init__(417, f"Attachment {attachment_id} is not available")

class AlreadyExists(Error):
    def __init__(self, details: str):
        super().__init__(417, details)
