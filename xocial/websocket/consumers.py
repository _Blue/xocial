import logging
import json
import traceback
import queue
import asyncio

from xocial import log, settings, serializers
from .app import consumer_closed
from pydoc import locate
from asgiref.sync import sync_to_async

class UpdatesConsumer:
    def __init__(self, socket):
        self.socket = socket

        self.compress = False
        self.requested_types = []
        self.queue = asyncio.Queue(maxsize=settings.WEBSOCKET_MAX_QUEUE_SIZE)

    async def push(self, db_object):
        if not any(isinstance(db_object, e) for e in self.requested_types):
            logging.debug(f'Object {db_object} is not requested by client {self.requested_types}, dropping update')
            return # Not subscribed to this type

        try:
            self.queue.put_nowait(db_object)
        except queue.Full:
            logging.warn(f'Websocket queue is full. Closing')

            await self.close()

    async def sync(self):
        await self.queue.join()

    async def start(self):
        logging.info("Accepted new websocket client")

        try:
            await self.start_impl()
        except Exception as e:
            log.log_error(e)
            message = {'result': 'Error', 'code': type(e).__name__, 'details': traceback.format_exc()}
            try:
                await self.send(message)
            except Exception as e:
                log.log_error(e)

            await self.close()

    async def start_impl(self):
        message = await self.socket.recv()
        await self.process_message(json.loads(message))

        await self.send_updates()

    async def close(self):
        await consumer_closed(self)

        if self.socket:
            await self.socket.close()

        self.socket = None
        self.queue.put_nowait(None)

    def resolve_type(self, type_name: str):
        resolved_type = locate(f'xocial.models.{type_name}')

        if not resolved_type:
            raise RuntimeError(f'Unable to resolve type: {type_name}')

        return resolved_type

    async def send(self, payload):
        if not self.socket:
            return

        await self.socket.send(json.dumps(payload))

    async def process_message(self, data: dict):
        if data.get('action', None) == 'subscribe':
            self.requested_types = [self.resolve_type(e) for e in data['types']]

            self.compress = data.get('compress', False)

            await self.send({'result': 'OK'})
        else:
            raise RuntimeError("Command not supported")

    async def send_updates(self):
        while self.socket:
            db_object = await self.queue.get()

            if db_object is None: # Pushed when the consumer is closed
                return

            try:
                await self.send(await sync_to_async(serializers.serialize)(db_object, self.compress))
            except Exception as e:
                logging.error(f'Error while writing websocket: {traceback.format_exc()}')
                await self.close()
                return
            finally:
                self.queue.task_done()
