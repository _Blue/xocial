class MaxConnectionsCountReached(Exception):
    def __init__(self):
        super().__init__(self, "Maximum websocket connections count reached")
