import logging
import websockets
import websockets.exceptions
import websockets.headers
import websockets.http
import websockets.legacy
import websockets.legacy.handshake
import websockets.legacy.http
import websockets.legacy.protocol
import asyncio
import os
import socket
import traceback
import struct
from django.apps import AppConfig
from threading import Thread, local
from xocial import settings, background, log, models
from django.apps.registry import Apps
from django import db
from asgiref.sync import sync_to_async

class WebsocketRunner:
    def __init__(self):
        self.running = False
        self.consumers = []

        self.loop = None
        self.server = None

    def start(self):
        if self.running:
            return

        self.running = True
        logging.info("Starting websocket application")

        self.thread = Thread(target=WebsocketRunner.run, args=(self,))
        self.thread.daemon = True
        self.thread.start()
        logging.info('Websocket application started')

    async def stop(self):
        logging.info('Stopping websocket application')

        await self.push(None)

    async def push(self, id: int):
        try:
            self.queue.put_nowait(id)
        except Exception as e:
            logging.error(f'Failed to push entry in websocket queue, {e}')

    async def sync(self):
        await self.queue.join()

    async def propagate_update(self, id):
        from xocial import models

        logging.debug(f'Websocket propagating update for object {id} to {len(self.consumers)} clients')

        try:
            entry = (await sync_to_async(models.load)(id=id))
        except models.DataModel.DoesNotExist:
            logging.warn(f'Update pusher failed to load object {id} from DB, {traceback.format_exc()}')
            return

        for e in self.consumers:
            await e.push(entry)

    async def read_updates(self):
        while self.running:
            id = await self.queue.get()
            db.close_old_connections()

            if id is None:
                logging.info('Websocket queue received None, exiting')
                await self.cleanup()
                return

            try:
                await self.propagate_update(id)
            except Exception as e:
                log.log_error(e)

            self.queue.task_done()

    async def cleanup(self):
        for e in self.consumers:
            await e.sync()
            await e.close()

        if self.server:
            self.server.close()


    async def accept(self, socket, path):
        from xocial.websocket.consumers import UpdatesConsumer

        consumer = UpdatesConsumer(socket)

        self.consumers.append(consumer)
        await consumer.start()

    async def consumer_closed(self, consumer):
        self.consumers = [e for e in self.consumers if e is not consumer]

        logging.info(f'Websocket client disconnected, clients left: {len(self.consumers)}')

    def run(self):
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)

        self.queue = asyncio.Queue(settings.WEBSOCKET_MAX_QUEUE_SIZE)

        start_server = websockets.serve(self.accept, settings.WEBSOCKET_LISTEN_ADDRESS, settings.WEBSOCKET_LISTEN_PORT)

        self.server = self.loop.run_until_complete(start_server)
        logging.info(f'Websocket server running on {settings.WEBSOCKET_LISTEN_ADDRESS}:{settings.WEBSOCKET_LISTEN_PORT}')

        self.loop.run_until_complete(self.read_updates())
        self.loop.run_until_complete(self.server.wait_closed())

        # Cancel outstanding tasks:
        for e in asyncio.all_tasks(self.loop):
            e.cancel()

        self.loop.close()


runner = None

sync_push = False # Used in testing to remove a race condition on push returning before the push is done

def create():
    global runner
    assert runner is None
    runner = WebsocketRunner()


def start():
    global runner
    assert runner is None
    runner = WebsocketRunner()
    runner.start()

def stop():
    global runner

    if runner:
        asyncio.run_coroutine_threadsafe(runner.stop(), runner.loop).result()
        runner.thread.join()
        runner = None

def push(id: int):
    global runner, sync_push

    pending = get_transaction_queue()

    if pending is not None: # A transaction is in progress, don't push yet
        if not id in pending: # If object is already there, don't add it again
            pending.append(id)

        return


    if runner:
        future = asyncio.run_coroutine_threadsafe(runner.push(id), runner.loop)
        if sync_push:
            future.result()

def sync():
    if runner:
        asyncio.run_coroutine_threadsafe(runner.sync(), runner.loop).result()


async def consumer_closed(consumer):
    global runner

    if runner:
        await runner.consumer_closed(consumer)

tls = local()

def start_transaction():
    stack = getattr(tls, 'pending', None)

    if not stack:
        stack = [[]]
        tls.pending = stack

    else:
        stack.append([])

def get_transaction_queue():
    stack = getattr(tls, 'pending', None)

    if not stack:
        return None
    else:
        return stack[-1]

def commit_transaction():
    pending = tls.pending[-1]
    tls.pending.pop()

    for e in pending:
        push(e)

def rollback_transaction():
    tls.pending.pop()

