from . import models
from .models import synthetic
from django.urls import path, re_path, include
from django.contrib import admin
from django.contrib.staticfiles import views
from .api import refresh, message, conversation_message, conversation, object_view, error, uploaded_attachment, search, attachment, source
from . import serializers

urlpatterns = [
    path('api/refresh', refresh.RefreshHandler.as_view()),
    re_path(r'^api/conversation/unread', conversation.UnreadConversationHandler.as_view()),
    re_path(r'^api/conversation/(?P<id>\d+)/message/search', search.ConversationSearchHandler.as_view()),
    re_path(r'^api/conversation/(?P<id>\d+)/message', conversation_message.ConversationMessageHandler.as_view()),
    re_path(r'^api/conversation/(?P<id>\d+)/refresh', conversation.ConversationRefreshHandler.as_view()),
    re_path(r'^api/search', search.SearchHandler.as_view()),
    re_path(r'^api/message/(?P<id>\d+)/react', message.MessageReactHandler.as_view()),
    re_path(r'^api/message/(?P<id>\d+)/read', message.MessageReadHandler.as_view()),
    re_path(r'^api/message/(?P<id>\d+)', object_view.make_view(models.Message)),
    re_path(r'^api/message', message.MessageHandler.as_view()),
    re_path(r'^api/synthetic_message', object_view.make_view(synthetic.SyntheticMessage)),
    re_path(r'^api/conversation/(?P<id>\d+)', object_view.make_view(models.Conversation)),
    re_path(r'^api/conversation', object_view.make_view(models.Conversation)),
    re_path(r'^api/contact', object_view.make_view(models.Contact)),
    re_path(r'^api/contact/(?P<id>\d+)', object_view.make_view(models.Contact)),
    re_path(r'^api/sourcecontact/(?P<id>\d+)', object_view.make_view(models.SourceContact)),
    re_path(r'^api/sourcecontact', object_view.make_view(models.SourceContact)),
    re_path(r'^api/attachment/(?P<id>\d+)', object_view.make_view(models.Attachment)),
    re_path(r'^api/attachment', object_view.make_view(models.Attachment)),
    re_path(r'^api/error/(?P<id>\d+)/acknowledge', error.ErrorAcknowledgeHandler.as_view()),
    re_path(r'^api/error/(?P<id>\d+)', object_view.make_view(models.Error)),
    re_path(r'^api/error', object_view.make_view(models.Error)),
    re_path(r'^api/source/(?P<id>\d+)/sourcecontact', source.SourceSourceContactHandler.as_view()),
    re_path(r'^api/source/(?P<id>\d+)/contact', source.SourceContactHandler.as_view()),
    re_path(r'^api/source/(?P<id>\d+)/conversation', source.SourceConversationHandler.as_view()),
    re_path(r'^api/source/(?P<source_id>\d+)/(?P<conversation_id>\d+)/(?P<message_id>\d+)', source.SourceMessageConversationHandlerUpdate.as_view()),
    re_path(r'^api/source/(?P<source_id>\d+)/(?P<conversation_id>\d+)', source.SourceMessageConversationHandler.as_view()),
    re_path(r'^api/source/(?P<id>\d+)/conversation/message', source.SourceMessageConversationHandler.as_view()),
    re_path(r'^api/source/(?P<id>\d+)', object_view.make_view(models.Source)),
    re_path(r'^api/source', source.SourceHandler.as_view()),
    re_path(r'^api/object/(?P<id>\d+)', object_view.make_view(models.DataModel)),
    re_path(r'^api/object', object_view.make_view(models.DataModel)),
    re_path(r'^api/upload_attachment/(?P<filename>.+)', uploaded_attachment.UploadedAttachmentHandler.as_view()),
    re_path(r'^attachment/(?P<id>.+)', attachment.AttachmentDownloadHandler.as_view()),
    re_path(r'^static/(?P<path>.*)$', views.serve),
    path('', admin.site.urls),
]
