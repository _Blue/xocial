import pytz
import fbchat
import json
from datetime import datetime, timezone, timedelta
from django.test import Client, TestCase
from xocial import models, settings
from xocial.models import synthetic
from xocial.backends import inject_backend
from xocial.backends.synthetic import SyntheticBackend
from unittest.mock import Mock, patch
from xocial.models import meta
from .utils import TIMESTAMP

TIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"

def parse_ts(value: str) -> datetime:
    return datetime.strptime(value, TIME_FORMAT).replace(tzinfo=timezone.utc)

class BackendTest(TestCase):

    def setUp(self):
        self.maxDiff = 10000

        meta.override_now(TIMESTAMP)

        self.source = synthetic.SyntheticSource(
                display_name='display_name',
                status=models.SourceStatus.Running.value)
        self.source.save()

        self.backend = SyntheticBackend(self.source)
        inject_backend(self.backend)

    def test_create_source_contact(self):
        body = {'display_name': 'name', 'display_image':  None, 'is_me': False}
        response = self.client.post(f'/api/source/{self.source.id}/sourcecontact', json.dumps(body), content_type="application/json")

        self.assertEquals(response.status_code, 200)

        content = json.loads(response.content)

        self.assertEquals(content['type'], 'SourceContact')
        self.assertEquals(content['display_name'], 'name')
        self.assertEquals(content['display_image'], None)
        self.assertEquals(content['is_me'], False)

    def test_create_source_contact_twice(self):
        body = {'display_name': 'name', 'display_image':  None, 'is_me': False}
        response = self.client.post(f'/api/source/{self.source.id}/sourcecontact', json.dumps(body), content_type="application/json")

        self.assertEquals(response.status_code, 200)

        content = json.loads(response.content)

        self.assertEquals(content['type'], 'SourceContact')
        self.assertEquals(content['display_name'], 'name')
        self.assertEquals(content['display_image'], None)
        self.assertEquals(content['is_me'], False)

        second_response = self.client.post(f'/api/source/{self.source.id}/sourcecontact', json.dumps(body), content_type="application/json")
        self.assertEquals(second_response.status_code, 200)

        self.assertEquals(second_response.content, response.content)

    def test_update_source_contact(self):
        body = {'display_name': 'name', 'display_image':  None, 'is_me': False}
        response = self.client.post(f'/api/source/{self.source.id}/sourcecontact', json.dumps(body), content_type="application/json")

        self.assertEquals(response.status_code, 200)

        content = json.loads(response.content)

        self.assertEquals(content['type'], 'SourceContact')
        self.assertEquals(content['display_name'], 'name')
        self.assertEquals(content['display_image'], None)
        self.assertEquals(content['is_me'], False)

        body['display_image'] = 'http://image'
        second_response = self.client.post(f'/api/source/{self.source.id}/sourcecontact', json.dumps(body), content_type="application/json")
        self.assertEquals(second_response.status_code, 200)

        second_content = json.loads(second_response.content)
        self.assertEquals(second_content['display_image'], 'http://image')
        self.assertEquals(second_content['id'], content['id'])


    def test_create_two_source_contacts(self):
        body = {'display_name': 'name', 'display_image':  None, 'is_me': False}
        response = self.client.post(f'/api/source/{self.source.id}/sourcecontact', json.dumps(body), content_type="application/json")

        self.assertEquals(response.status_code, 200)

        content = json.loads(response.content)
        self.assertEquals(content['type'], 'SourceContact')
        self.assertEquals(content['display_name'], 'name')
        self.assertEquals(content['display_image'], None)
        self.assertEquals(content['is_me'], False)


        body = {'display_name': 'name2', 'display_image':  None, 'is_me': False}
        second_response = self.client.post(f'/api/source/{self.source.id}/sourcecontact', json.dumps(body), content_type="application/json")
        second_content = json.loads(second_response.content)
        self.assertEquals(second_response.status_code, 200)
        self.assertEquals(second_content['type'], 'SourceContact')
        self.assertEquals(second_content['display_name'], 'name2')
        self.assertEquals(second_content['display_image'], None)
        self.assertEquals(second_content['is_me'], False)

    def create_source_contact(self, display_name='dummyname', display_image=None, is_me=False):
        body = {'display_name': display_name, 'display_image':  display_image, 'is_me': is_me}
        response = self.client.post(f'/api/source/{self.source.id}/sourcecontact', json.dumps(body), content_type="application/json")

        self.assertEquals(response.status_code, 200)

        content = json.loads(response.content)
        self.assertEquals(content['type'], 'SourceContact')
        self.assertEquals(content['display_name'], display_name)
        self.assertEquals(content['display_image'], display_image)
        self.assertEquals(content['is_me'], is_me)

        return content['id']

    def create_contact(self, source_contact_id: int):
        body = {'source_contact_id': source_contact_id}
        response = self.client.post(f'/api/source/{self.source.id}/contact', json.dumps(body), content_type="application/json")
        self.assertEquals(response.status_code, 200)

        content = json.loads(response.content)

        self.assertEquals(content['sources'][0]['id'], source_contact_id)
        return content['id']


    def test_create_contact(self):
        source_contact = self.create_source_contact()

        contact_id = self.create_contact(source_contact)

        contact = models.Contact.objects.get(id=contact_id)

        self.assertEquals(len(contact.sources.all()), 1)

        source_contact = contact.sources.first()

        self.assertEquals(source_contact.display_name, 'dummyname')
        self.assertEquals(contact.display_name, 'dummyname')


    def test_create_contact_twice(self):
        source_contact_id = self.create_source_contact()

        contact_id = self.create_contact(source_contact_id)

        contact = models.Contact.objects.get(id=contact_id)

        self.assertEquals(len(contact.sources.all()), 1)

        source_contact = contact.sources.first()

        self.assertEquals(source_contact.display_name, 'dummyname')
        self.assertEquals(contact.display_name, 'dummyname')


        new_contact_id = self.create_contact(source_contact_id)
        self.assertEquals(contact_id, new_contact_id)

    def test_create_me_contact_twice(self):
        source_contact_id = self.create_source_contact(is_me=True)

        contact_id = self.create_contact(source_contact_id)
        contact = models.Contact.objects.get(id=contact_id)

        self.assertEquals(len(contact.sources.all()), 1)
        source_contact = contact.sources.first()

        self.assertEquals(source_contact.display_name, 'dummyname')
        self.assertEquals(contact.display_name, 'dummyname')
        self.assertTrue(contact.is_me)
        self.assertTrue(source_contact.is_me)

        new_contact_id = self.create_contact(source_contact_id)
        self.assertEquals(contact_id, new_contact_id)
        contact.refresh_from_db()
        self.assertTrue(contact.is_me)
        self.assertTrue(source_contact.is_me)

    def test_create_contact_empty_body(self):
        response = self.client.post(f'/api/source/{self.source.id}/contact', '{}', content_type="application/json")
        self.assertEquals(response.status_code, 400)

    def test_create_contact_bad_id(self):
        response = self.client.post(f'/api/source/{self.source.id}/contact', '{"source_contact_id": 133545}', content_type="application/json")
        self.assertEquals(response.status_code, 404)

    def test_create_contact_source_contact_bad_source(self):
        source = synthetic.SyntheticSource(display_name='display_name_2', status=models.SourceStatus.Running.value)
        source.save()

        inject_backend(SyntheticBackend(source))

        body = {'display_name': 'bad_source', 'display_image':  None, 'is_me': False}
        response = self.client.post(f'/api/source/{source.id}/sourcecontact', json.dumps(body), content_type="application/json")
        self.assertEquals(response.status_code, 200)

        body = {'source_contact_id': json.loads(response.content)['id']}
        response = self.client.post(f'/api/source/{self.source.id}/contact', json.dumps(body), content_type="application/json")
        self.assertEquals(response.status_code, 400)

    def create_conversation(self, members: list, display_name='display_name', display_image=None, expected_result=200) -> int:
        body = {'display_name': display_name, 'display_image': display_image, 'members': members}
        response = self.client.post(f'/api/source/{self.source.id}/conversation', json.dumps(body), content_type="application/json")
        self.assertEquals(response.status_code, expected_result)

        if expected_result != 200:
            return None

        conversation = json.loads(response.content)
        self.assertEquals(conversation['display_name'], display_name)
        self.assertEquals(conversation['display_image'], display_image)

        member_ids = [e['id'] for e in conversation['members']]
        self.assertEquals(set(members), set(member_ids))

        return conversation['id']

    def create_me_contact(self) -> int:
        return self.create_contact(self.create_source_contact('me', None, is_me=True))

    def test_create_conversation(self):
        me_contact_id = self.create_me_contact()

        contact_1 = self.create_contact(self.create_source_contact('contact_1'))

        conversation_id = self.create_conversation([me_contact_id, contact_1], 'conversation_1', None)

        conversation = models.Conversation.objects.get(id=conversation_id)

        self.assertEquals(conversation.display_name, 'conversation_1')
        self.assertEquals(conversation.display_image, None)
        self.assertEquals(set([e.id for e in conversation.members.all()]), set([me_contact_id, contact_1]))

    def test_create_conversation_twice(self):
        me_contact_id = self.create_me_contact()

        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        conversation_id = self.create_conversation([me_contact_id, contact_1], 'conversation_1', None)
        conversation = models.Conversation.objects.get(id=conversation_id)

        conversation_id_2 = self.create_conversation([me_contact_id, contact_1], 'conversation_1', None)

        self.assertEquals(conversation_id_2, conversation_id)
        self.assertEquals(conversation.display_name, 'conversation_1')
        self.assertEquals(conversation.display_image, None)
        self.assertEquals(set([e.id for e in conversation.members.all()]), set([me_contact_id, contact_1]))

    def test_create_conversation_twice_different_members(self):
        me_contact_id = self.create_me_contact()

        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        contact_2 = self.create_contact(self.create_source_contact('contact_2'))
        conversation_id = self.create_conversation([me_contact_id, contact_1], 'conversation_1', None)
        conversation = models.Conversation.objects.get(id=conversation_id)

        conversation_id_2 = self.create_conversation([me_contact_id, contact_2], 'conversation_1', None)

        self.assertNotEquals(conversation_id_2, conversation_id)
        self.assertEquals(conversation.display_name, 'conversation_1')
        self.assertEquals(conversation.display_image, None)
        self.assertEquals(set([e.id for e in conversation.members.all()]), set([me_contact_id, contact_1]))

        conversation_2 = models.Conversation.objects.get(id=conversation_id_2)
        self.assertEquals(conversation_2.display_name, 'conversation_1')
        self.assertEquals(conversation_2.display_image, None)
        self.assertEquals(set([e.id for e in conversation_2.members.all()]), set([me_contact_id, contact_2]))


    def test_create_conversation_no_members(self):
        self.create_conversation([], 'conversation_1', None, expected_result=400)

    def test_create_conversation_no_me(self):
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        self.create_conversation([contact_1], 'conversation_1', None, expected_result=400)

    def test_create_conversation_duplicated_member(self):
        me_contact_id = self.create_me_contact()
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        self.create_conversation([contact_1, contact_1, me_contact_id], 'conversation_1', None, expected_result=400)

    def test_create_conversation_3_members(self):
        me_contact_id = self.create_me_contact()
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        contact_2 = self.create_contact(self.create_source_contact('contact_2'))
        conversation_id = self.create_conversation([contact_1, contact_2, me_contact_id], 'conversation_1', 'http://image')

        conversation = models.Conversation.objects.get(id=conversation_id)

        self.assertEquals(conversation.display_name, 'conversation_1')
        self.assertEquals(conversation.display_image, 'http://image')
        self.assertEquals(set([e.id for e in conversation.members.all()]), set([me_contact_id, contact_1, contact_2]))


    def send_message(self, conversation_id: int, text: str, reply_to_id: int=None, attachments = [], expected_result=200):
        body = {'text': text}

        if reply_to_id is not None:
            body['reply_to'] = reply_to_id

        if attachments:
            body['attachments'] = attachments

        response = self.client.post(f'/api/conversation/{conversation_id}/message', json.dumps(body), content_type="application/json")
        self.assertEquals(response.status_code, expected_result)

        if expected_result != 200:
            return

        message = json.loads(response.content)

        self.assertEquals(message['text'], text)
        self.assertEquals(message['author']['is_me'], True)
        self.assertEquals(message['conversation']['id'], conversation_id)

        return message['id']

    def test_send_basic_message(self):
        me_contact_id = self.create_me_contact()
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        conversation_id = self.create_conversation([contact_1, me_contact_id], 'conversation_1', 'http://image')

        message_id = self.send_message(conversation_id, 'message-content')

        message = synthetic.SyntheticMessage.objects.get(id=message_id)
        self.assertEquals(message.text, 'message-content')
        self.assertEquals(message.author.id, me_contact_id)
        self.assertEquals(message.conversation.id, conversation_id)
        self.assertEquals(message.remote_id, None)

    def test_reply_to_message(self):
        me_contact_id = self.create_me_contact()
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        conversation_id = self.create_conversation([contact_1, me_contact_id], 'conversation_1', 'http://image')

        message_id_1 = self.send_message(conversation_id, 'message-content')
        message_id_2 = self.send_message(conversation_id, 'message-content-2', reply_to_id=message_id_1)

        message = synthetic.SyntheticMessage.objects.get(id=message_id_2)
        self.assertEquals(message.text, 'message-content-2')
        self.assertEquals(message.author.id, me_contact_id)
        self.assertEquals(message.conversation.id, conversation_id)
        self.assertEquals(message.remote_id, None)
        self.assertEquals(message.reply_to.id, message_id_1)

    def test_reply_to_message_wrong_conversation(self):
        me_contact_id = self.create_me_contact()
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        conversation_id = self.create_conversation([contact_1, me_contact_id], 'conversation_1', 'http://image')
        conversation_id_2 = self.create_conversation([contact_1, me_contact_id], 'conversation_2', 'http://image')

        message_id_1 = self.send_message(conversation_id, 'message-content')
        message_id_2 = self.send_message(conversation_id_2, 'message-content-2', reply_to_id=message_id_1, expected_result=400)

    def test_message_attachments(self):
        me_contact_id = self.create_me_contact()
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        conversation_id = self.create_conversation([contact_1, me_contact_id], 'conversation_1', 'http://image')

        self.send_message(conversation_id, 'message-content', attachments=[12], expected_result=404)

    def create_or_update_message(
            self,
            text: str,
            author_id: int,
            conversation_id: int,
            remote_id: int,
            timestamp: datetime,
            sent_timestamp: datetime,
            reply_to_id=None,
            local_id=None,
            expected_result=200) -> int:

        body = {'text': text, 'conversation': conversation_id, 'author': author_id, 'timestamp': timestamp.timestamp()}

        if remote_id is not None:
            body['remote_id'] = remote_id

        if reply_to_id is not None:
            body['reply_to'] = reply_to_id

        if sent_timestamp is not None:
            body['sent_timestamp'] = sent_timestamp.timestamp()

        endpoint = f'/api/source/{self.source.id}/{conversation_id}/{local_id}' if local_id else f'/api/source/{self.source.id}/{conversation_id}'
        response = self.client.post(endpoint, json.dumps(body), content_type="application/json")
        self.assertEquals(response.status_code, expected_result)

        if expected_result != 200:
            return

        message = json.loads(response.content)

        self.assertEquals(message['remote_id'], remote_id)
        self.assertEquals(message['author']['id'], author_id)
        self.assertEquals(message['conversation']['id'], conversation_id)
        self.assertEquals(message['text'], text)
        self.assertEquals(parse_ts(message['sent_timestamp']) if message['sent_timestamp'] else None, None if not sent_timestamp else sent_timestamp)
        self.assertEquals(parse_ts(message['timestamp']), timestamp)

        return message['id']

    def test_create_remote_message(self):
        me_contact_id = self.create_me_contact()
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        conversation_id = self.create_conversation([contact_1, me_contact_id], 'conversation_1', 'http://image')

        remote_id = 444
        message_id = self.create_or_update_message('message_1', me_contact_id, conversation_id, remote_id, TIMESTAMP, None)
        message = synthetic.SyntheticMessage.objects.get(id=message_id)

        self.assertEquals(message.text, 'message_1')
        self.assertEquals(message.author.id, me_contact_id)
        self.assertEquals(message.conversation.id, conversation_id)
        self.assertEquals(message.sent_timestamp, None)
        self.assertEquals(message.timestamp, TIMESTAMP)
        self.assertEquals(message.remote_id, remote_id)

    def test_create_remote_message_duplicated(self):
        me_contact_id = self.create_me_contact()
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        conversation_id = self.create_conversation([contact_1, me_contact_id], 'conversation_1', 'http://image')

        remote_id = 444
        message_id = self.create_or_update_message('message_1', me_contact_id, conversation_id, remote_id, TIMESTAMP, None)
        message_id = self.create_or_update_message('message_2', me_contact_id, conversation_id, remote_id, TIMESTAMP, None, expected_result=417)

    def test_create_remote_message_duplicated_different_conversations(self):
        me_contact_id = self.create_me_contact()
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        contact_2 = self.create_contact(self.create_source_contact('contact_2'))
        conversation_id = self.create_conversation([contact_1, me_contact_id], 'conversation_1', 'http://image')
        conversation_id_2 = self.create_conversation([contact_2, me_contact_id], 'conversation_1', 'http://image')

        remote_id = 444
        message_id_1 = self.create_or_update_message('message_1', me_contact_id, conversation_id, remote_id, TIMESTAMP, None)
        message_id_2 = self.create_or_update_message('message_2', me_contact_id, conversation_id_2, remote_id, TIMESTAMP, None)


        message_1 = synthetic.SyntheticMessage.objects.get(id=message_id_1)
        self.assertEquals(message_1.text, 'message_1')
        self.assertEquals(message_1.author.id, me_contact_id)
        self.assertEquals(message_1.conversation.id, conversation_id)
        self.assertEquals(message_1.sent_timestamp, None)
        self.assertEquals(message_1.timestamp, TIMESTAMP)
        self.assertEquals(message_1.remote_id, remote_id)


        message_2 = synthetic.SyntheticMessage.objects.get(id=message_id_2)
        self.assertEquals(message_2.text, 'message_2')
        self.assertEquals(message_2.author.id, me_contact_id)
        self.assertEquals(message_2.conversation.id, conversation_id_2)
        self.assertEquals(message_2.sent_timestamp, None)
        self.assertEquals(message_2.timestamp, TIMESTAMP)
        self.assertEquals(message_2.remote_id, remote_id)


    def test_update_remote_message_duplicated(self):
        me_contact_id = self.create_me_contact()
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        conversation_id = self.create_conversation([contact_1, me_contact_id], 'conversation_1', 'http://image')

        remote_id = 444
        message_id_1 = self.create_or_update_message('message_1', me_contact_id, conversation_id, remote_id, TIMESTAMP, None)
        message_id_2 = self.create_or_update_message('message_2', me_contact_id, conversation_id, 445, TIMESTAMP, None)
        message_id_3 = self.create_or_update_message('message_3', me_contact_id, conversation_id, remote_id, TIMESTAMP, None, message_id_1, expected_result=417)


    def test_create_remote_message_with_transmitted_ts(self):
        me_contact_id = self.create_me_contact()
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        conversation_id = self.create_conversation([contact_1, me_contact_id], 'conversation_1', 'http://image')

        remote_id = 444
        message_id = self.create_or_update_message('message_1', me_contact_id, conversation_id, remote_id, TIMESTAMP, TIMESTAMP)
        message = synthetic.SyntheticMessage.objects.get(id=message_id)

        self.assertEquals(message.text, 'message_1')
        self.assertEquals(message.author.id, me_contact_id)
        self.assertEquals(message.conversation.id, conversation_id)
        self.assertEquals(message.sent_timestamp.replace(tzinfo=timezone.utc), TIMESTAMP)
        self.assertEquals(message.remote_id, remote_id)

    def test_create_remote_message_with_reply_to(self):
        me_contact_id = self.create_me_contact()
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        conversation_id = self.create_conversation([contact_1, me_contact_id], 'conversation_1', 'http://image')

        remote_id = 444
        message_id = self.create_or_update_message('message_1', me_contact_id, conversation_id, remote_id, TIMESTAMP, TIMESTAMP)
        message_id_2 = self.create_or_update_message('message_2', me_contact_id, conversation_id, 455, TIMESTAMP, TIMESTAMP, reply_to_id=message_id)
        message = synthetic.SyntheticMessage.objects.get(id=message_id_2)

        self.assertEquals(message.text, 'message_2')
        self.assertEquals(message.author.id, me_contact_id)
        self.assertEquals(message.conversation.id, conversation_id)
        self.assertEquals(message.sent_timestamp.replace(tzinfo=timezone.utc), TIMESTAMP)
        self.assertEquals(message.reply_to.id, message_id)
        self.assertEquals(message.remote_id, 455)
        self.assertNotEquals(message.conversation.last_read_message, message_id)

    def test_update_sent_message(self):
        me_contact_id = self.create_me_contact()
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        conversation_id = self.create_conversation([contact_1, me_contact_id], 'conversation_1', 'http://image')
        sent_message_id = self.send_message(conversation_id, 'message-content')

        remote_id = 444
        message_id = self.create_or_update_message('message-content', me_contact_id, conversation_id, remote_id, TIMESTAMP, TIMESTAMP, local_id=sent_message_id)
        self.assertEqual(message_id, sent_message_id)

        message = synthetic.SyntheticMessage.objects.get(id=message_id)
        self.assertEquals(message.text, 'message-content')
        self.assertEquals(message.author.id, me_contact_id)
        self.assertEquals(message.conversation.id, conversation_id)
        self.assertEquals(message.sent_timestamp, TIMESTAMP)
        self.assertEquals(message.timestamp, TIMESTAMP)
        self.assertEquals(message.remote_id, remote_id)
        self.assertEquals(message.conversation.last_read_message.id, message_id)

    def test_update_sent_message_with_reply_to(self):
        me_contact_id = self.create_me_contact()
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        conversation_id = self.create_conversation([contact_1, me_contact_id], 'conversation_1', 'http://image')
        message_id_1 = self.send_message(conversation_id, 'message-content')
        message_id_2 = self.send_message(conversation_id, 'message-content', reply_to_id=message_id_1)

        remote_id = 444
        message_id = self.create_or_update_message('message-content', me_contact_id, conversation_id, remote_id, TIMESTAMP, TIMESTAMP, reply_to_id=message_id_1, local_id=message_id_2)
        self.assertEqual(message_id, message_id_2)

        message = synthetic.SyntheticMessage.objects.get(id=message_id)
        self.assertEquals(message.text, 'message-content')
        self.assertEquals(message.author.id, me_contact_id)
        self.assertEquals(message.conversation.id, conversation_id)
        self.assertEquals(message.sent_timestamp, TIMESTAMP)
        self.assertEquals(message.timestamp, TIMESTAMP)
        self.assertEquals(message.remote_id, remote_id)

    def test_update_sent_message_all_fields(self):
        me_contact_id = self.create_me_contact()
        contact_1 = self.create_contact(self.create_source_contact('contact_1'))
        conversation_1 = self.create_conversation([contact_1, me_contact_id], 'conversation_1', 'http://image')
        conversation_2 = self.create_conversation([contact_1, me_contact_id], 'conversation_2', 'http://image')
        sent_message_1 = self.send_message(conversation_1, 'message-content')
        sent_message_2 = self.send_message(conversation_2, 'message-content-2')

        remote_id = 444
        new_ts = TIMESTAMP + timedelta(hours=1)
        message_id = self.create_or_update_message('message-content-3', contact_1, conversation_2, remote_id, new_ts, TIMESTAMP, reply_to_id=sent_message_2, local_id=sent_message_1)
        self.assertEqual(message_id, sent_message_1)

        message = synthetic.SyntheticMessage.objects.get(id=message_id)
        self.assertEquals(message.text, 'message-content-3')
        self.assertEquals(message.author.id, contact_1)
        self.assertEquals(message.conversation.id, conversation_2)
        self.assertEquals(message.sent_timestamp, TIMESTAMP)
        self.assertEquals(message.timestamp, new_ts)
        self.assertEquals(message.remote_id, remote_id)
        self.assertEquals(message.reply_to.id, sent_message_2)

        message_id = self.create_or_update_message('message-content-4', contact_1, conversation_2, remote_id, new_ts, new_ts, reply_to_id=sent_message_2, local_id=sent_message_1)
        self.assertEqual(message_id, sent_message_1)

        message.refresh_from_db()
        self.assertEquals(message.text, 'message-content-4')
        self.assertEquals(message.author.id, contact_1)
        self.assertEquals(message.conversation.id, conversation_2)
        self.assertEquals(message.sent_timestamp, new_ts)
        self.assertEquals(message.timestamp, new_ts)
        self.assertEquals(message.remote_id, remote_id)
        self.assertEquals(message.reply_to.id, sent_message_2)

    def create_source(self, source_type: str, display_name: str, expected_result=200):
        body = {'type': source_type, 'display_name': display_name}
        response = self.client.post(f'/api/source', json.dumps(body), content_type="application/json")
        self.assertEquals(response.status_code, expected_result)

        if expected_result != 200:
            return

        source = json.loads(response.content)

        self.assertEquals(source['display_name'], display_name)
        self.assertEquals(source['type'], f'Source/{source_type}')

        return source['id']


    def test_create_synthetic_source(self):
        source_id = self.create_source('SyntheticSource', 'DummySource')

        restore_source = self.source
        try:
            self.source = synthetic.SyntheticSource.objects.get(id=source_id)
            me_contact_id = self.create_me_contact()
            contact_1 = self.create_contact(self.create_source_contact('contact_1'))
            conversation_1 = self.create_conversation([contact_1, me_contact_id], 'conversation_1', 'http://image')
            conversation_2 = self.create_conversation([contact_1, me_contact_id], 'conversation_2', 'http://image')
            sent_message_1 = self.send_message(conversation_1, 'message-content')

            message = synthetic.SyntheticMessage.objects.get(id=sent_message_1)
            self.assertEquals(message.conversation.source.id, source_id)
        except:
            self.source = restore_source

    def test_create_source_bad_type(self):
        self.create_source('bad', 'name', 400)

    def test_create_source_duplicate_name(self):
        self.create_source('SyntheticSource', 'name', 200)
        self.create_source('SyntheticSource', 'name', 417)
