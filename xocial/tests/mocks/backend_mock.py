import fbchat
from xocial.backends import facebook

class FacebookBackendMock(facebook.FacebookBackend):
    uid = '1234'

    def __init__(self, source):
        # Not calling base by design

        self.source = source
        self.active = False

        self.injected_threads = {}
        self.inject_thread(fbchat.User(uid='1234', name="My", first_name = "Name", last_name = "Myself"))

        self.create_me_if_missing()

        class DownloaderMock:

            def add(self, *args):
                pass

        self.downloader = DownloaderMock()


    def fetchThreadInfo(self, id) -> dict:
        if id in self.injected_threads:
            return {id: self.injected_threads[id]}

        return super().fetchThreadInfo(id)

    def inject_thread(self, thread: fbchat.Thread):
        self.injected_threads[str(thread.uid)] = thread

    def setActiveStatus(self, markAlive: bool):
        self.active = markAlive
