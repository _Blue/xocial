import pytz
import fbchat
import json
import random
from datetime import datetime, timezone
from django.test import Client, TestCase
from xocial import models, settings
from xocial.models import facebook
from xocial.tests.mocks.backend_mock import FacebookBackendMock
from xocial.backends import inject_backend
from xocial.backends.facebook import to_fb_timestamp
from unittest.mock import Mock, patch
from .expected_bodies import expected_send_message_body, expected_send_message_reply_to_body
from xocial.models import meta
from .utils import TIMESTAMP

class BackendTest(TestCase):

    def setUp(self):
        self.maxDiff = 10000

        meta.override_now(TIMESTAMP)
        patch("fbchat.Client").start()

        self.source = facebook.FacebookSource(
                display_name='display_name',
                user_email='user@domain.org',
                credential='{"cred": true}',
                status=models.SourceStatus.Running.value)
        self.source.save()

        self.backend = FacebookBackendMock(self.source)

        inject_backend(self.backend)

    def tearDown(self):
        meta.override_now(None)

    def test_message_processing_error_is_saved(self):
        author = fbchat.User(uid=13, name="Me", first_name = "My", last_name = "Self")

        def fetch_message_info(*args, **kwargs):
            raise RuntimeError("Dummy")

        message = fbchat.Message('text')
        message.uid = 25
        message.author = str(author.uid)
        message.timestamp = datetime.timestamp(TIMESTAMP) * 1000
        message.reply_to_id = '12'

        self.backend.fetchThreadInfo = lambda id: {id: author} if str(id) == author.uid else None

        self.backend.fetchThreadMessages = fetch_message_info

        self.backend.process_message(message, author.uid, fbchat.ThreadType.USER)

        conversation = models.Conversation.objects.all().first()

        self.assertEqual("RuntimeError", conversation.errors.all()[0].code)

        self.assertTrue('Dummy' in conversation.errors.all()[0].details)

    def test_new_group_member_is_detected(self):
        self.validate_message_flow(is_user_gone=False)

    def test_gone_group_member_is_detected(self):
        self.validate_message_flow(is_user_gone=True)

    def validate_message_flow(self, is_user_gone: bool):
        author = fbchat.User(uid=13, name="Me", first_name = "My", last_name = "Self")
        group = fbchat.Group(uid=14, name="Group")

        if not is_user_gone:
            group.participants = [author.uid]

        conversation = facebook.FacebookConversation(
                facebook_conversation_id = group.uid,
                facebook_conversation_type = 'group',
                display_name = 'conversation',
                source = self.source)

        conversation.save()

        self.backend.inject_thread(group)
        self.backend.inject_thread(author)

        message = fbchat.Message('text')
        message.uid = 25
        message.author = str(author.uid)
        message.timestamp = datetime.timestamp(TIMESTAMP) * 1000

        self.backend.process_message(message, group.uid, fbchat.ThreadType.GROUP)

        message = models.Message.objects.all().first()
        author = models.Contact.objects.filter(is_me=False).first()

        contacts = message.conversation.gone_members if is_user_gone else message.conversation.members
        self.assertEqual([author.id], [e.id for e in contacts.all() if not e.is_me])

        conversation.refresh_from_db()
        read_marker = conversation.read_markers.all()[0]
        self.assertEqual(read_marker.contact.id, message.author.id)
        self.assertEqual(read_marker.timestamp, message.timestamp)
        self.assertEqual(read_marker.message.id, message.id)

    def test_fetch_all_messages(self):
        author = fbchat.User(uid=self.backend.uid, name="Me", first_name = "My", last_name = "Self")

        def make_msg(id):
            message = fbchat.Message(text=f"message {id}")
            message.uid = id
            message.author = str(author.uid)
            message.timestamp = int((datetime.timestamp(TIMESTAMP) + id)) * 1000

            return message

        messages = list(reversed([make_msg(e) for e in range(1, 112)]))

        conversation = facebook.FacebookConversation(
                facebook_conversation_id = author.uid,
                facebook_conversation_type = 'user',
                display_name = 'conversation',
                source = self.source)

        conversation.save()

        self.backend.fetchThreadInfo = lambda id: {'13': author} if id == '13' else None

        def fetch_messages(thread_id, limit=20, before=None):
            self.assertEqual(author.uid, thread_id)
            output = [e for e in messages if not before or e.timestamp < before][:limit]
            random.shuffle(output) # Validate that messages are processed in order

            return output

        self.backend.fetchThreadMessages = fetch_messages
        self.backend.fetchThreadInfo = lambda id: {id: author} if str(id) == author.uid else None

        self.backend.fetch_all_conversation_messages(conversation, cooldown=0, fetch_limit=20)

        fetched_messages = facebook.FacebookMessage.objects.order_by('id').all()
        self.assertEquals(set([e.uid for e in messages]), set([int(e.facebook_message_id) for e in fetched_messages]))

        def fetch_messages(thread_id, limit=20, before=None):
            self.assertEqual(messages[-1].timestamp, before)

            return []

        self.backend.fetchThreadMessages = fetch_messages
        self.backend.fetch_all_conversation_messages(conversation, cooldown=0, fetch_limit=20)

        self.assertEqual(0, models.Error.objects.count())

    def test_error_during_conversation_refresh_is_saved(self):
        author = fbchat.User(uid=13, name="Me", first_name = "My", last_name = "Self")
        message = fbchat.Message(text=f"message")
        message.uid = 12
        message.author = str(author.uid)
        message.timestamp = datetime.timestamp(TIMESTAMP) * 1000

        self.backend.fetchThreadInfo = lambda id: {'13': author} if id == '13' else None

        def fetch_messages(thread_id, limit=20, before=None):
            raise RuntimeError("Dummy")

        conversation = facebook.FacebookConversation(
                facebook_conversation_id = author.uid,
                facebook_conversation_type = 'user',
                display_name = 'conversation',
                source = self.source)

        conversation.save()

        self.backend.fetchThreadMessages = fetch_messages
        self.backend.fetchThreadInfo = lambda id: {id: author} if str(id) == author.uid else None
        self.backend.fetch_all_messages()

        self.assertEqual("RuntimeError", conversation.errors.all()[0].code)
        self.assertTrue(conversation.errors.all()[0].details)

    def test_on_message_seen_updates_read_marker(self):
        author = fbchat.User(uid=13, name="Me", first_name = "My", last_name = "Self")
        self.backend.inject_thread(author)
        self.backend.fetchUserInfo = lambda id: {'13': author} if id == '13' else None

        conversation = facebook.FacebookConversation(
                facebook_conversation_id = author.uid,
                facebook_conversation_type = 'user',
                display_name = 'conversation',
                source = self.source)
        conversation.save()


        message = fbchat.Message(text=f"text_2")
        message.uid = 2
        message.author = str(author.uid)
        message.timestamp = datetime.timestamp(TIMESTAMP) * 1000

        message_1 = facebook.FacebookMessage(
                        facebook_message_id=1,
                        text='text',
                        author=self.backend.me_contact,
                        conversation=conversation,
                        timestamp=TIMESTAMP.replace(microsecond=0))
        message_1.save()

        message_2 = facebook.FacebookMessage(
                        facebook_message_id=2,
                        text='text_2',
                        author=self.backend.me_contact,
                        conversation=conversation,
                        timestamp=TIMESTAMP.replace(hour=23, microsecond=0))
        message_2.save()

        conversation.save()

        seen_ts = TIMESTAMP.replace(microsecond=0, hour=22)

        self.backend.onMessageSeen(
                seen_by=author.uid,
                thread_id=author.uid,
                thread_type=fbchat.ThreadType.USER,
                seen_ts=datetime.timestamp(seen_ts) * 1000,
                ts=12,
                metadata=None,
                msg=message)

        conversation.refresh_from_db()
        self.assertEquals(conversation.read_markers.all()[0].message.id, message_1.id)
        self.assertEquals(conversation.read_markers.all()[0].timestamp, seen_ts)

        # Validate that the marker can be moved to a future message
        seen_ts = TIMESTAMP.replace(hour=23, second=59, microsecond=0)

        self.backend.onMessageSeen(
                seen_by=author.uid,
                thread_id=author.uid,
                thread_type=fbchat.ThreadType.USER,
                seen_ts=datetime.timestamp(seen_ts) * 1000 ,
                ts=12,
                metadata=None,
                msg=message)

        conversation.refresh_from_db()
        self.assertEquals(conversation.read_markers.all()[0].message.id, message_2.id)
        self.assertEquals(conversation.read_markers.all()[0].timestamp, seen_ts)
        self.assertEquals(1, conversation.read_markers.count())

        # Validate that an older timestamp will not move the marker back in the past
        self.backend.onMessageSeen(
                seen_by=author.uid,
                thread_id=author.uid,
                thread_type=fbchat.ThreadType.USER,
                seen_ts=message.timestamp,
                ts=12,
                metadata=None,
                msg=message)

        conversation.refresh_from_db()
        self.assertEquals(conversation.read_markers.all()[0].message.id, message_2.id)



    def test_on_marked_seen_marks_message_as_read(self):
        author = fbchat.User(uid=13, name="Me", first_name = "My", last_name = "Self")

        self.backend.fetchUserInfo = lambda id: {'13': author} if id == '13' else None

        conversation = facebook.FacebookConversation(
                facebook_conversation_id = author.uid,
                facebook_conversation_type = 'user',
                display_name = 'conversation',
                source = self.source)
        conversation.save()


        message = fbchat.Message(text=f"text_2")
        message.uid = 2
        message.author = str(author.uid)
        message.timestamp = datetime.timestamp(TIMESTAMP) * 1000

        message_1 = facebook.FacebookMessage(
                        facebook_message_id=1,
                        text='text',
                        author=self.backend.me_contact,
                        conversation=conversation,
                        timestamp=TIMESTAMP.replace(microsecond=0))
        message_1.save()

        message_2 = facebook.FacebookMessage(
                        facebook_message_id=2,
                        text='text_2',
                        author=self.backend.me_contact,
                        conversation=conversation,
                        timestamp=TIMESTAMP.replace(hour=23, microsecond=0))
        message_2.save()


        conversation.last_read_message = message_1
        conversation.save()

        self.backend.onMarkedSeen(
                threads=[(author.uid, fbchat.ThreadType.USER)],
                seen_ts=datetime.timestamp(TIMESTAMP),
                ts=12,
                metadata=None,
                msg=message)

        conversation.refresh_from_db()
        self.assertEquals(conversation.last_read_message.id, message_1.id)

        self.backend.onMarkedSeen(
                threads=[(author.uid, fbchat.ThreadType.USER)],
                seen_ts=datetime.timestamp(TIMESTAMP.replace(hour=23)) * 1000 ,
                ts=12,
                metadata=None,
                msg=message)

        conversation.refresh_from_db()
        self.assertEquals(conversation.last_read_message.id, message_2.id)

    def run_last_updated_test(self, read: bool, previous_message: bool):
        author = fbchat.User(uid=13, name="Me", first_name = "My", last_name = "Self")

        self.backend.fetchUserInfo = lambda id: {'13': author} if id == '13' else None

        msg_count = 0
        def fetch_messages(thread_id, limit=20, before=None):
            nonlocal msg_count
            message = fbchat.Message(text=f"message")
            message.uid = 12 + msg_count
            message.author = str(author.uid)
            message.timestamp = datetime.timestamp(TIMESTAMP) * 1000 - msg_count
            message.is_read = read
            msg_count = msg_count + 1

            return [message]

        conversation = facebook.FacebookConversation(
                facebook_conversation_id = author.uid,
                facebook_conversation_type = 'user',
                display_name = 'conversation',
                source = self.source)

        conversation.save()

        self.backend.fetchThreadMessages = fetch_messages
        self.backend.fetchThreadInfo = lambda id: {id: author} if str(id) == author.uid else None

        if previous_message:
            self.backend.fetch_all_messages()
            conversation.refresh_from_db()
            self.assertEqual('12', conversation.last_read_message.as_leaf_class().facebook_message_id)

        self.backend.fetch_all_messages()
        conversation.refresh_from_db()

        if previous_message:
            self.assertEqual('12', conversation.last_read_message.as_leaf_class().facebook_message_id)
            self.assertEqual(2, models.Message.objects.count())
        elif read:
            self.assertEqual('12', conversation.last_read_message.as_leaf_class().facebook_message_id)
        else:
            self.assertIsNone(conversation.last_read_message)


    def test_conversation_last_read_message_is_not_updated_if_not_read(self):
        self.run_last_updated_test(read=False, previous_message=False)

    def test_conversation_last_read_message_is_not_updated_if_not_read_after_last(self):
        self.run_last_updated_test(read=True, previous_message=True)

    def test_conversation_last_read_message_is_updated_if_read(self):
        self.run_last_updated_test(read=True, previous_message=False)

    def test_fb_placeholder_contacts_are_not_merged(self):
        authors = [
                    fbchat.User(uid=1, name="Facebook User", first_name = "Facebook", last_name = "User"),
                    fbchat.User(uid=2, name="Facebook User", first_name = "Facebook", last_name = "User")
                  ]

        group = fbchat.Group(uid='3', name='Group')
        group.last_message_timestamp = 0
        group.participants = [1, 2]

        def fetchUserInfo(id):
            raise RuntimeError('Dummy')

        self.backend.fetchUserInfo = lambda id: fetchUserInfo

        self.backend.fetchThreadInfo = lambda id: {'3': group} if id == '3' else {}
        self.backend.fetchThreadMessages = lambda thread_id, limit: []

        self.backend.process_discovered_thread_impl(group, fetch_limit=1)

        conversation = facebook.FacebookConversation.objects.get(facebook_conversation_id='3')

        self.assertEqual(conversation.display_name, 'Group')
        self.assertEqual(conversation.members.count(), 3) # User + 2 errors
        self.assertEqual(conversation.members.all()[1].display_name, 'Facebook error user 1')
        self.assertEqual(conversation.members.all()[2].display_name, 'Facebook error user 2')

        self.assertEqual(self.backend.source.errors.count(), 2)

        # Running it again should not trigger any new error now
        self.backend.process_discovered_thread_impl(group, fetch_limit=1)
        self.assertEqual(self.backend.source.errors.count(), 2)


    def test_fb_error_contact_is_created(self):
        authors = [
                    fbchat.User(uid=1, name="Facebook User", first_name = "Facebook", last_name = "User"),
                    fbchat.User(uid=2, name="Facebook User", first_name = "Facebook", last_name = "User")
                  ]

        self.backend.fetchUserInfo = lambda id: next((e for e in authors if e.uid == id), None)

        for e in authors:
            self.backend.process_discovered_contact(e)

        contacts = models.Contact.objects.all()

        self.assertEquals([e.display_name for e in contacts if not e.is_me], ["Facebook User (1)", "Facebook User (2)"])



    def test_send_message(self):

        def mock_send_message(message, id, type):
            self.assertEquals('this is the text', message.text)
            self.assertEquals('12', id)
            self.assertEquals(fbchat.ThreadType.USER , type)

            return 18

        active = False
        def mock_set_status(status):
            nonlocal active

            self.assertNotEqual(active, status)
            active = status


        self.backend.send = mock_send_message
        self.backend.setActiveStatus = mock_set_status

        author = fbchat.User(uid=13, name="Me", first_name = "My", last_name = "Self")

        message = fbchat.Message(text='this is the text')
        message.uid = 18
        message.author = self.backend.uid
        message.timestamp = datetime.timestamp(TIMESTAMP) * 1000

        self.backend.fetchThreadMessages = lambda conv_id, limit: [message]
        self.backend.fetchUserInfo = lambda id: {'13': author} if id == '13' else None
        self.backend.fetchThreadInfo = lambda id: {'12': author} if id == '12' else None

        conversation = facebook.FacebookConversation(
                facebook_conversation_id = 12,
                facebook_conversation_type = 'user',
                display_name = 'conversation',
                source = self.source)

        conversation.save()

        body = {'text': 'this is the text'}

        response = self.client.post(f'/api/conversation/{conversation.id}/messages', json.dumps(body), content_type="application/json")

        self.assertEquals(response.status_code, 200)
        self.assertEquals(json.loads(response.content), json.loads(expected_send_message_body))

    def test_send_message_reply_to(self):
        conversation = facebook.FacebookConversation(
                facebook_conversation_id = 12,
                facebook_conversation_type = 'user',
                display_name = 'conversation',
                source = self.source)

        conversation.save()

        author = fbchat.User(uid=13, name="Me", first_name = "My", last_name = "Self")

        message = fbchat.Message(text='this is the text')
        message.uid = 18
        message.author = self.backend.uid
        message.timestamp = datetime.timestamp(TIMESTAMP) * 1000

        message.reply_to = fbchat.Message(text='text')
        message.reply_to_id = 'fb-mid'
        message.reply_to.uid = message.reply_to_id

        reply_message = facebook.FacebookMessage(
                facebook_message_id = message.reply_to.uid,
                conversation = conversation,
                author = self.backend.me_contact,
                text = message.reply_to.text,
                timestamp = TIMESTAMP)

        reply_message.save()

        def mock_send_message(message, id, type):
            self.assertEquals('this is the text', message.text)
            self.assertEquals('12', id)
            self.assertEquals(fbchat.ThreadType.USER , type)
            self.assertEquals(message.reply_to_id, reply_message.facebook_message_id)

            return 18

        self.backend.fetchThreadMessages = lambda conv_id, limit: [message]
        self.backend.inject_thread(author)
        self.backend.send = mock_send_message

        body = {'text': 'this is the text', 'reply_to': reply_message.id}

        response = self.client.post(f'/api/conversation/{conversation.id}/messages', json.dumps(body), content_type="application/json")

        self.assertEquals(response.status_code, 200)
        self.assertEquals(json.loads(response.content), json.loads(expected_send_message_reply_to_body))

    def test_attachment_upload(self):
        settings.MEDIA_ROOT = '/tmp/xocial_tests'

        file_content = 'content\r\ncontent2'

        upload_response = self.client.post('/api/upload_attachment/file_name.txt', body=file_content.encode())
        self.assertEqual(200, upload_response.status_code)

        attachment = json.loads(upload_response.content)

        # Can't check the timestamp easily here
        expected_response = {"file_name": "file_name.txt", "id": 4, "size": 20, "type": "UploadedAttachment", "last_updated": "2019-11-24T15:08:48.171850Z"}
        for e in expected_response:
            self.assertEqual(expected_response[e], attachment[e])


        conversation = facebook.FacebookConversation(
                facebook_conversation_id = 13,
                facebook_conversation_type = 'user',
                display_name = 'conversation',
                source = self.source)

        conversation.save()

        author = fbchat.User(uid=13, name="Me", first_name = "My", last_name = "Self")

        fb_attachment = fbchat.FileAttachment()
        fb_attachment.uid = 12345
        fb_attachment.url = "http://url.localhost"
        fb_attachment.name = 'file_name.txt'
        fb_attachment.size = 20

        message = fbchat.Message(text='this is the text')
        message.uid = 18
        message.author = author.uid
        message.timestamp = datetime.timestamp(TIMESTAMP) * 1000
        message.attachments = [fb_attachment]


        def sendLocalFilesMock(files: list, text: str, id: str, type):
            self.assertEquals([f'/tmp/xocial_tests/{attachment["id"]}/file_name.txt'], files)
            self.assertEquals(message.text, text)
            self.assertEquals(author.uid, id)
            self.assertEquals(type, fbchat.ThreadType.USER)

            return message.uid

        self.backend.fetchThreadMessages = lambda conv_id, limit: [message]
        self.backend.fetchUserInfo = lambda id: {'13': author} if id == '13' else None
        self.backend.fetchThreadInfo = lambda id: {'13': author} if id == '13' else None
        self.backend.sendLocalFiles = sendLocalFilesMock

        body = {'text': message.text, 'attachments': [attachment['id']]}

        response = self.client.post(f'/api/conversation/{conversation.id}/messages', json.dumps(body), content_type="application/json")
        self.assertEqual(response.status_code, 200)
        message = json.loads(response.content)

        expected =  [{
                        'size': 20, 
                        'facebook_attachment_id': '12345', 
                        'name': 'file_name.txt', 
                        'url': 'http://url.localhost', 
                        'id': 10, 
                        'type': 'Attachment/FileAttachment/FacebookFileAttachment', 
                        'last_updated': '2019-11-24T15:08:48.171850Z',
                        'hosted_url': None}]

        self.assertEqual(expected, message['attachments'])

    def test_thread_is_pulled_if_outdated(self):
        conversation = facebook.FacebookConversation(
            facebook_conversation_id = 17,
            facebook_conversation_type = 'user',
            facebook_last_message_timestamp=TIMESTAMP,
            display_name = 'conversation',
            source = self.source)
        conversation.save()

        thread = fbchat.User(uid = conversation.facebook_conversation_id,
                             name = "contact_1",
                             first_name = "contact",
                             last_name = "1",
                             last_message_timestamp=datetime.timestamp(TIMESTAMP) * 1000,
                             photo="https://picture.com")

        called = False
        def fetchThreadMessages(*args, **kwargs):
            nonlocal called
            called = True

            return {}

        self.backend.fetchThreadMessages = fetchThreadMessages

        self.backend.process_discovered_thread_impl(thread, fetch_limit=1)
        self.assertFalse(called)

        thread.last_message_timestamp = thread.last_message_timestamp + 1000

        self.backend.process_discovered_thread_impl(thread, fetch_limit=1)
        self.assertTrue(called)


    def test_listen(self):
        ts = datetime.timestamp(TIMESTAMP) * 1000 + 1

        author = fbchat.User(uid=12, name="contact_1", first_name = "contact", last_name = "1", last_message_timestamp=ts, photo="https://picture.com")

        self.backend.inject_thread(author)

        message = fbchat.Message(text="text")
        message.uid = 14
        message.author = author.uid
        message.timestamp = ts

        self.backend.fetchThreadMessages = lambda id, limit: [message] if message.author == id else None
        self.backend.onMessage(message.author, message.text, message, author.uid, fbchat.ThreadType.USER)

        db_message = facebook.FacebookMessage.objects.first()

        self.assertEqual(message.text, db_message.text)
        self.assertEqual(db_message.author.sources.all()[0].as_leaf_class().facebook_user_id, author.uid)

        message.reactions = {str(author.uid): fbchat.MessageReaction.LOVE}

        self.backend.onReactionAdded(message.uid, fbchat.MessageReaction.LOVE, author.uid, author.uid, fbchat.ThreadType.USER, ts)

        db_message.refresh_from_db()
        self.assertEquals(fbchat.MessageReaction.LOVE.value, db_message.reactions.all()[0].as_leaf_class().emoji)
        self.assertEquals(author.uid, db_message.reactions.all()[0].author.sources.all()[0].as_leaf_class().facebook_user_id)


        message.reactions = {str(author.uid): fbchat.MessageReaction.ANGRY}
        self.backend.onReactionAdded(message.uid, fbchat.MessageReaction.ANGRY, author.uid, author.uid, fbchat.ThreadType.USER, ts)

        db_message.refresh_from_db()
        self.assertEquals(fbchat.MessageReaction.ANGRY.value, db_message.reactions.all()[0].as_leaf_class().emoji)
        self.assertEquals(author.uid, db_message.reactions.all()[0].author.sources.all()[0].as_leaf_class().facebook_user_id)

        message.reactions = {}
        self.backend.onReactionRemoved(message.uid, author.uid, author.uid, fbchat.ThreadType.USER, ts)

        db_message.refresh_from_db()
        self.assertEquals(0, db_message.reactions.count())


    def test_discovery(self):
        ts = datetime.timestamp(TIMESTAMP) * 1000 + 1
        contact_1 = fbchat.User(uid=12, name="contact_1", first_name = "contact", last_name = "1", last_message_timestamp=ts, photo="https://picture.com")
        contact_2 = fbchat.User(uid=13, name="contact_2", first_name = "contact", last_name = "2", last_message_timestamp=ts)
        contact_3 = fbchat.User(uid=14, name="contact_3", first_name = "contact", last_name = "3", last_message_timestamp=ts)
        page = fbchat.Page(uid=15, url="https://page", name="page_name", last_message_timestamp=ts)

        users = [contact_1, contact_2, contact_3, page]


        self.backend.uid = 13
        self.backend.fetchAllUsers = lambda: users
        threads = {
                    fbchat.ThreadLocation.INBOX: users,
                    fbchat.ThreadLocation.ARCHIVED: [],
                    fbchat.ThreadLocation.OTHER: [],
                    fbchat.ThreadLocation.PENDING: [],
                  }

        self.backend.fetchThreadList = lambda thread_location, limit: threads[thread_location]
        self.backend.fetchThreadInfo = lambda id: {id: next(e for e in users if e.uid == id)}

        def make_msg(text, uid, author):
            message = fbchat.Message(text=text)
            message.uid = uid
            message.author = author
            message.timestamp = datetime.timestamp(TIMESTAMP) * 1000

            return message

        messages = [
                    make_msg(text='message 1', uid=1, author=contact_1.uid),
                    make_msg(text='message 2', uid=2, author=contact_1.uid),
                    make_msg(text='message 3', uid=3, author=contact_1.uid),
                    make_msg(text='message 4', uid=4, author=contact_1.uid),
                    make_msg(text='message 5', uid=5, author=contact_2.uid),
                   ]

        messages[0].reactions = {str(contact_1.uid): fbchat.MessageReaction.LOVE}

        def make_attachment(type, **kwargs):
            output = type()

            for e in kwargs:
                setattr(output, e, kwargs[e])

            return output

        downloaded = []
        class MockDownloader:

            def add(self, attachment):
                nonlocal downloaded

                downloaded.append(str(attachment.facebook_attachment_id))

        self.backend.downloader = MockDownloader()

        attachments = [
                        make_attachment(fbchat.ShareAttachment, url='http://foo.com', original_url='http://foo.bar.com', uid=101, title='title', description='description'),
                        make_attachment(fbchat.Sticker, url='http://foo.com', label='label', uid=102, pack=103),
                        make_attachment(fbchat.LocationAttachment, latitude=48.8534, longitude=2.3488, uid=104, image_url='https://image.com', url='https://maps.com'),
                        make_attachment(fbchat.FileAttachment, uid=105, url='https://maps.com', name='file_name', size=12),
                        make_attachment(fbchat.AudioAttachment, uid=None, url='https://audio.com', filename='file_name', duration=1337), # Audio attachment don't have UID's set by FB
                        make_attachment(fbchat.ImageAttachment, uid=107, large_preview_url='https://preview.com'),
                        make_attachment(fbchat.VideoAttachment, uid=108, large_image_url='https://preview.com', duration=1212),
                       ]

        messages[0].attachments = attachments

        def fetch_image_url(id):
            self.assertEqual(id, attachments[5].uid)

            return "https://image.com"

        self.backend.fetchThreadMessages = lambda id, limit: [e for e in messages if e.author == id]
        self.backend.fetchImageUrl = fetch_image_url
        self.backend.refresh()

        self.assertEquals(sorted(downloaded), ['1-a-4', '102', '105', '107', '108'])

        contacts = facebook.FacebookSourceContact.objects.order_by('facebook_user_id').filter(is_me=False).all()

        def compare_contacts(db, contact):
            self.assertEqual(contact.uid, db.facebook_user_id)
            self.assertEqual(contact.name, db.display_name)
            self.assertEqual(contact.uid == 12, db.is_me)

        self.assertEqual(len(contacts), len(users))
        for (db, contact) in zip(contacts, users):
            compare_contacts(db, contact)

        def compare_conversations(db, conversation):
            self.assertEqual(db.facebook_conversation_id, conversation.uid)
            self.assertEqual(2, len(db.members.all()))
            self.assertEqual(1, len(db.members.all()[0].sources.all()))
            self.assertEqual(1, len(db.members.all()[1].sources.all()))
            self.assertEqual(conversation.photo, db.display_image)
            compare_contacts(db.members.filter(is_me=False).all()[0].sources.all()[0].as_leaf_class(), conversation)
            self.assertEqual(db.display_name, conversation.name)
            self.assertEqual(db.facebook_conversation_type, "page" if type(conversation) is fbchat.Page else "user")

        for (db, conv) in zip(facebook.FacebookConversation.objects.order_by('facebook_conversation_id').all(), users):
            compare_conversations(db, conv)

        def compare_attachments(db, attachment):
            self.assertEqual(db.facebook_attachment_id, str(attachment.uid))

            if attachment.uid == 101:
                self.assertEqual(db.url, attachment.url)
                self.assertEqual(db.original_url, attachment.original_url)
                self.assertEqual(db.title, attachment.title)
                self.assertEqual(db.description, attachment.description)
            elif attachment.uid == 102:
                self.assertEqual(db.url, attachment.url)
                self.assertEqual(db.facebook_label, attachment.label)
                self.assertEqual(db.facebook_sticker_pack_id, str(attachment.pack))
            elif attachment.uid == 104:
                self.assertEqual(db.latitude, attachment.latitude)
                self.assertEqual(db.longitude, attachment.longitude)
                self.assertEqual(db.image_url, attachment.image_url)
                self.assertEqual(db.map_url, attachment.url)
            elif attachment.uid == 105:
                self.assertEqual(db.url, attachment.url)
                self.assertEqual(db.name, attachment.name)
                self.assertEqual(db.size, attachment.size)
            elif attachment.uid == '1-a-4':
                self.assertEqual(db.length_ms, attachment.duration)
                self.assertEqual(db.url, attachment.url)
                self.assertEqual(db.name, attachment.filename)
            elif attachment.uid == 107: # Audio attachment
                self.assertEqual(db.url, "https://image.com")
                self.assertEqual(db.preview_url, attachment.large_preview_url)
            elif attachment.uid == 108:
                self.assertEqual(db.url, attachment.large_image_url)
                self.assertEqual(db.length_ms, attachment.duration)
            else:
                raise RuntimeError(f"Unexpected attachment id: {attachment.uid}")


        def compare_reacts(db, author, react):
            self.assertEqual(db.author.sources.all()[0].as_leaf_class().facebook_user_id, author)
            self.assertEqual(db.emoji, react.value)

        def compare_messages(db, msg, author, conversation):
            self.assertEqual(db.text, msg.text)
            self.assertEqual(db.author.sources.all()[0].as_leaf_class().facebook_user_id, msg.author)
            self.assertEqual(db.conversation.as_leaf_class().facebook_conversation_id, conversation.uid)
            self.assertEqual(db.timestamp, TIMESTAMP.replace(microsecond=171000)) # FB time precision is 1 millisecond

            for (db_attachment, attachment) in zip(db.attachments.all(), msg.attachments):
                compare_attachments(db_attachment.as_leaf_class(), attachment)

            for (db, react) in zip(db.reactions.all(), msg.reactions):
                compare_reacts(db.as_leaf_class(), react, msg.reactions[react])

        for (db, msg) in zip(facebook.FacebookMessage.objects.order_by('facebook_message_id').all(), messages):
            compare_messages(
                    db,
                    msg,
                    contact_1 if msg.uid < 5 else contact_2,
                    contact_1 if msg.uid < 5 else contact_2,)

        self.assertEqual(0, len(models.Error.objects.all()))


    def test_fetch_old_contacts(self):
        # Deleting the 'me' contact to simplify this test case

        self.backend.me_contact.sources.first().delete()

        def make_contact(id):
            contact = fbchat.User(uid=str(id), name="new_name" + str(id), first_name = "new_name" + str(id), last_name = "", photo="https://picture.com")
            db_contact = facebook.FacebookSourceContact(source = self.source,
                                                        facebook_user_id = str(id),
                                                        display_name = 'old_name_' + str(id),
                                                        last_updated = TIMESTAMP.replace(hour=id))
            db_contact.save()
            return contact

        contacts = [make_contact(e) for e in range(0, 24)]

        db_contacts = [facebook.FacebookSourceContact.objects.get(facebook_user_id=str(e)) for e in range(0, 24)]
        db_contacts[0].last_updated = TIMESTAMP.replace(year=2099) # Shouldn't be fetched
        db_contacts[0].save()

        db_contacts[1].last_updated = TIMESTAMP.replace(year=1999) # Should be fetched
        db_contacts[1].save()

        db_contacts[2].last_updated = TIMESTAMP.replace(year=1999) # Should be fetched, will trigger an error
        db_contacts[2].save()


        fetched = set()
        def fetchUserInfo(id):
            nonlocal fetched

            fetched.add(id)
            if id == '2':
                raise RuntimeError('dummy error')

            return {id: next(e for e in contacts if e.uid == id)}

        self.backend.fetchThreadInfo = fetchUserInfo
        self.backend.fetchAllUsers = lambda: []

        self.backend.fetch_contacts()

        self.assertTrue('2' in fetched)
        self.assertFalse('0' in fetched)
        self.assertEquals(self.source.errors.count(), 1) # 1 error for contact 12

        self.assertTrue(all(str(e) in fetched for e in range(1, 12) if e != 10))

        for e in db_contacts[:12]:
            e.refresh_from_db()

            if e.facebook_user_id == '0': # Not fetched
                continue

            if e.facebook_user_id == '2':
                self.assertTrue(e.has_fetch_error)
                continue
            else:
                self.assertFalse(e.has_fetch_error)

            self.assertEquals(e.display_name, 'new_name' + e.facebook_user_id)


        fetched.clear()

        self.backend.fetch_contacts()

        self.assertFalse('0' in fetched)
        self.assertFalse('2' in fetched)

        self.assertEquals(self.source.errors.count(), 1) # No new error is triggered


    def test_synth_friend_request_message(self):
        ts = datetime.timestamp(TIMESTAMP) * 1000 + 1

        author = fbchat.User(uid=12, name="contact_1", first_name = "contact", last_name = "1", last_message_timestamp=ts, photo="https://picture.com")

        self.backend.inject_thread(author)

        self.backend.onFriendRequest(from_id='12', msg=None)

        conversation = facebook.FacebookConversation.objects.first()

        self.assertEquals(conversation.facebook_conversation_id, 'xocial-12-friend-request')
        self.assertEquals(conversation.facebook_conversation_type, fbchat.ThreadType.USER.name.lower())
        self.assertEquals(conversation.synthetic, True)
        self.assertEquals(conversation.display_name, 'contact_1')
        self.assertEquals(conversation.display_image, author.photo)

        message = facebook.FacebookMessage.objects.first()
        self.assertEquals(message.author.display_name, 'contact_1')
        self.assertEquals(message.text, 'Received friend request. Message: None')
        self.assertEquals(message.facebook_message_id, 'xocial-12-friend-request-message-0')

        # Validate that the conversations merge correctly
        fb_message = fbchat.Message(text="text")
        fb_message.uid = 14
        fb_message.author = author.uid
        fb_message.timestamp = ts
        author.name = 'real_name'

        self.backend.fetchThreadMessages = lambda id, limit: [message] if message.author == id else None
        self.backend.onMessage(fb_message.author, fb_message.text, fb_message, author.uid, fbchat.ThreadType.USER)


        conversation.refresh_from_db()
        self.assertEquals(facebook.FacebookConversation.objects.count(), 1)
        self.assertEquals(conversation.facebook_conversation_id, '12')
        self.assertEquals(conversation.display_name, 'real_name')
        self.assertEquals(conversation.synthetic, False)

        real_message = facebook.FacebookMessage.objects.get(facebook_message_id='14')
        self.assertEquals(real_message.conversation.id, conversation.id)



    def test_synth_friend_request_message_reversed(self):
        ts = datetime.timestamp(TIMESTAMP) * 1000 + 1

        author = fbchat.User(uid=12, name="real_name", first_name = "contact", last_name = "1", last_message_timestamp=ts, photo="https://picture.com")

        self.backend.inject_thread(author)

        # First create a conversation
        fb_message = fbchat.Message(text="text")
        fb_message.uid = 14
        fb_message.author = author.uid
        fb_message.timestamp = ts

        self.backend.fetchThreadMessages = lambda id, limit: [message] if message.author == id else None
        self.backend.onMessage(fb_message.author, fb_message.text, fb_message, author.uid, fbchat.ThreadType.USER)

        conversation = facebook.FacebookConversation.objects.first()

        self.assertEquals(conversation.facebook_conversation_id, '12')
        self.assertEquals(conversation.facebook_conversation_type, 'user')
        self.assertEquals(conversation.synthetic, False)
        self.assertEquals(conversation.display_name, 'real_name')
        self.assertEquals(conversation.display_image, author.photo)

        message = facebook.FacebookMessage.objects.first()
        self.assertEquals(message.author.display_name, 'real_name')
        self.assertEquals(message.text, 'text')

        self.backend.onFriendRequest(from_id='12', msg=None)

        conversation.refresh_from_db()
        self.assertEquals(facebook.FacebookConversation.objects.count(), 1)
        self.assertEquals(conversation.facebook_conversation_id, '12')
        self.assertEquals(conversation.display_name, 'real_name')

        synth_message = facebook.FacebookMessage.objects.order_by('-id').first()

        self.assertEquals(synth_message.conversation.id, conversation.id)
        self.assertEquals(synth_message.author.display_name, 'real_name')
        self.assertEquals(synth_message.text, 'Received friend request. Message: None')
        self.assertEquals(synth_message.facebook_message_id, 'xocial-12-friend-request-message-1')


    def test_message_request(self):
        ts = datetime.timestamp(TIMESTAMP) * 1000 + 1

        author = fbchat.User(uid=12, name="contact_1", first_name = "contact", last_name = "1", last_message_timestamp=ts, photo="https://picture.com")

        self.backend.inject_thread(author)

        self.backend.onPendingMessage(thread_id='12', thread_type=fbchat.ThreadType.USER, metadata='metadata', msg={'foo': 'bar'})

        conversation = facebook.FacebookConversation.objects.first()

        self.assertEquals(conversation.facebook_conversation_id, '12')
        self.assertEquals(conversation.facebook_conversation_type, 'user')
        self.assertEquals(conversation.synthetic, False)
        self.assertEquals(conversation.display_name, 'contact_1')
        self.assertEquals(conversation.display_image, author.photo)

        message = facebook.FacebookMessage.objects.first()
        self.assertEquals(message.author.display_name, 'My')
        self.assertEquals(message.text, 'Received conversation request. Metadata: metadata, Message: {\'foo\': \'bar\'}')
        self.assertEquals(message.facebook_message_id, 'xocial-12-friend-request-message-request-0')
