import pytz
import json

from datetime import datetime, timezone
from django.test import Client, TestCase
from xocial import serializers
from xocial import models
from xocial.models import synthetic
from xocial.models import facebook
from . import expected_bodies
from urllib.parse import urlencode
from .utils import TIMESTAMP

class ObjectViewTest(TestCase):

    def setUp(self):
        self.source = facebook.FacebookSource(
                display_name='display_name',
                user_email='user@domain.org',
                credential='{"cred": true}',
                last_updated=TIMESTAMP,
                status=models.SourceStatus.Running.value)
        self.source.save()

        self.fb_contact_1 = facebook.FacebookSourceContact(
                source=self.source,
                display_name="fb_contact_1",
                last_updated=TIMESTAMP.replace(hour=16),
                facebook_user_id=1)
        self.fb_contact_1.save()
        self.fb_contact_1 = facebook.FacebookSourceContact.objects.all()[0]

        self.fb_contact_2 = facebook.FacebookSourceContact(
                source=self.source,
                display_name="fb_contact_2",
                last_updated=TIMESTAMP.replace(hour=17),
                facebook_user_id=2)
        self.fb_contact_2.save()


        self.contact_1 = models.Contact(
                display_name=self.fb_contact_1.display_name,
                last_updated=TIMESTAMP.replace(hour=18))
        self.contact_1.save()

    def test_serialize_different_types(self):
        response = self.client.get(f'/api/object?order=asc')
        self.assertEquals(response.status_code, 200)

        output = json.loads(response.content)
        self.assertEquals(4, len(output))

        self.assertEquals('1', output[1]['facebook_user_id'])

        expected_types = [
                'Source/FacebookSource',
                'SourceContact/FacebookSourceContact',
                'SourceContact/FacebookSourceContact',
                'Contact']

        self.assertEquals(expected_types, [e['type'] for e in output])


    def validate_ordering(self, expected: list, from_id: int=None, count: int=None, sort: str=None, ts=None):
        params = {}

        if from_id:
            params['from_id'] = from_id

        if count:
            params['count'] = count

        if sort:
            params['order'] = sort

        if ts:
            params['updated_after'] = datetime.timestamp(ts)

        response = self.client.get(f'/api/object?' + urlencode(params))

        self.assertEquals(response.status_code, 200)
        body = json.loads(response.content)

        ids = [e['id'] for e in body]
        self.assertEquals(expected, ids)

    def test_default(self):
        self.validate_ordering(expected=[4, 3, 2, 1])

    def test_from(self):
        self.validate_ordering(from_id=4, expected=[3, 2, 1])

    def test_from_desc(self):
        self.validate_ordering(from_id=2, sort='desc', expected=[1])

    def test_from_base_asc(self):
        self.validate_ordering(from_id=0, sort='asc', expected=[1, 2, 3, 4])

    def test_count(self):
        self.validate_ordering(from_id=2, count=1, expected=[1])

    def test_over(self):
        self.validate_ordering(from_id=4, count=5, expected=[3, 2, 1])

    def test_count_desc(self):
        self.validate_ordering(from_id=1, sort='desc', count=1, expected=[])

    def test_count_desc_base(self):
        self.validate_ordering(from_id=4, sort='desc', count=1, expected=[3])

    def test_last_updated_asc(self):
        self.validate_ordering(ts=TIMESTAMP, expected=[2, 3, 4], sort='asc')

    def test_last_updated_desc(self):
        self.validate_ordering(ts=TIMESTAMP.replace(hour=18), sort='desc', expected=[3, 2, 1])

    def test_last_updated_asc_cut(self):
        self.validate_ordering(ts=TIMESTAMP.replace(hour=17), expected=[4], sort='asc')

    def test_last_updated_asc_none(self):
        self.validate_ordering(ts=TIMESTAMP.replace(hour=18), expected=[], sort='asc')

    def test_last_updated_asc_just_before(self):
        self.validate_ordering(ts=TIMESTAMP.replace(hour=18).replace(microsecond=1), expected=[4], sort='asc')

    def test_custom_query(self):
        source = synthetic.SyntheticSource(
                display_name='display_nameè2',
                status=models.SourceStatus.Running.value)
        source.save()

        contact = models.Contact(display_name='contact')
        contact.save()

        conversation = models.Conversation(display_name='conversation', source=source)
        conversation.save()

        message_with_remote_id = synthetic.SyntheticMessage(text='with', author=contact, conversation=conversation, timestamp=TIMESTAMP, remote_id=12)
        message_with_remote_id.save()

        message_without_remote_id = synthetic.SyntheticMessage(text='without', author=contact, conversation=conversation, timestamp=TIMESTAMP)
        message_without_remote_id.save()


        def validate_query(custom_query: dict, expected_ids: list, order=None):
            body = {'custom_query': custom_query, 'source': source.id}

            if order is not None:
                body['order'] = order

            response = self.client.post(f'/api/synthetic_message', json.dumps(body), content_type="application/json")

            self.assertEquals(response.status_code, 200)

            objects = json.loads(response.content)

            self.assertEquals([e['id'] for e in objects], expected_ids)


        validate_query({'remote_id__isnull': True, 'conversation_id': conversation.id}, [message_without_remote_id.id])
        validate_query({'remote_id__isnull': False, 'conversation_id': conversation.id}, [message_with_remote_id.id])
        validate_query({'conversation_id': conversation.id}, [message_with_remote_id.id, message_without_remote_id.id], order='asc')
        validate_query({'conversation_id': conversation.id}, [message_without_remote_id.id, message_with_remote_id.id], order='desc')
