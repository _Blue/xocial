import fbchat
import requests
from http.server import BaseHTTPRequestHandler, HTTPServer
from threading import Thread
from xocial.backends.attachments import AttachmentDownloader
from datetime import datetime, timezone
from django.test import Client, TransactionTestCase
from xocial import models, settings
from xocial.models import facebook
from xocial.tests.mocks.backend_mock import FacebookBackendMock
from xocial.backends import inject_backend
from xocial.serializers import serialize


TEST_PORT = 3333

class DownloaderTest(TransactionTestCase):

    def setUp(self):

        self.source = facebook.FacebookSource(
                display_name='display_name',
                user_email='user@domain.org',
                credential='{"cred": true}',
                status=models.SourceStatus.Running.value)
        self.source.save()

        self.downloader = AttachmentDownloader(self.source)

        self.backend = FacebookBackendMock(self.source)

        self.author = fbchat.User(uid=13, name='me', first_name='m', last_name='e')
        self.backend.inject_thread(self.author)

        inject_backend(self.backend)

        self.downloader.start()

    def tearDown(self):
        self.downloader.stop()

    def serve(self, name: str, content: str, content_type='application/text'):

        test = self
        class handler(BaseHTTPRequestHandler):
            def do_GET(self):
                test.assertEqual(self.path[1:], name)

                self.send_response_only(200)
                self.send_header('Content-length', len(content))

                if content_type:
                    self.send_header('Content-Type', content_type)
                self.end_headers()
                self.wfile.write(content.encode())

        server = HTTPServer(('127.0.0.1', TEST_PORT), handler)
        thread = Thread(target=lambda: server.handle_request())
        thread.start()

        return thread, self.make_attachment(f'http://127.0.0.1:{TEST_PORT}/{name}')

    def make_attachment(self, url: str) -> models.Attachment:
        attachment = models.FileAttachment(url=url)
        attachment.save()

        return attachment

    def get_download(self, attachment: models.Attachment) -> models.AttachmentDownload:
        return models.AttachmentDownload.objects.filter(attachment=attachment).first()

    def test_simple_successful_download(self):
        file, attachment = self.serve('simple-file', 'content')

        self.downloader.add(attachment)

        self.downloader.sync()

        download = self.get_download(attachment)

        self.assertEquals(download.state, models.AttachmentState.Available.value)
        self.assertEquals(download.last_download_url, f'http://127.0.0.1:{TEST_PORT}/simple-file')
        self.assertEquals(download.content, 'content'.encode())
        self.assertEquals(download.web_content_type, 'application/text')
        self.assertEquals(serialize(download.attachment)['hosted_url'], f'{settings.EXTERNAL_URL}/attachment/{download.id}')
        self.assertEquals(download.errors.count(), 0)

        self.validate_attachment_download(serialize(download.attachment)['hosted_url'], 200, 'content'.encode(), 'application/text')


    def test_simple_successful_download_without_content_type(self):
        file, attachment = self.serve('simple-file', 'content', None)

        self.downloader.add(attachment)

        self.downloader.sync()

        download = self.get_download(attachment)

        self.assertEquals(download.state, models.AttachmentState.Available.value)
        self.assertEquals(download.last_download_url, f'http://127.0.0.1:{TEST_PORT}/simple-file')
        self.assertEquals(download.content, 'content'.encode())
        self.assertEquals(download.web_content_type, None)
        self.assertEquals(serialize(download.attachment)['hosted_url'], f'{settings.EXTERNAL_URL}/attachment/{download.id}')
        self.assertEquals(download.errors.count(), 0)

        self.validate_attachment_download(serialize(download.attachment)['hosted_url'], 200, 'content'.encode(), None)


    def test_backed_off_download(self):
        attachment = self.make_attachment(f'http://127.0.0.1:{TEST_PORT}')

        self.downloader.add(attachment)

        self.downloader.sync()

        download = self.get_download(attachment)

        self.assertEquals(download.state, models.AttachmentState.BackedOff.value)
        self.assertEquals(download.last_download_url, f'http://127.0.0.1:{TEST_PORT}')
        self.assertEquals(download.content, None)
        self.assertEquals(download.web_content_type, None)
        self.assertEquals(download.errors.count(), 1)
        self.assertEquals(download.backoff, 2)

    def validate_attachment_download(self, url: str, expected_code: int, expected_content: str, expected_content_type: str):
        response = self.client.get(url[len(settings.EXTERNAL_URL):])

        self.assertEquals(expected_code, response.status_code)

        if expected_content:
            self.assertEquals(response.content, expected_content)

        if expected_content_type:
            self.assertEquals(response.get('Content-Type'), expected_content_type or 'application/data')

    def test_big_attachment_download(self):
        content = 'big' * 1024 * 1024 * 10 # 30 MB

        download = models.AttachmentDownload(content=content.encode(),
                                             attachment=self.make_attachment('http://foo'),
                                             state=models.AttachmentState.Available.value)
        download.save()

        self.validate_attachment_download(f'{settings.EXTERNAL_URL}/attachment/{download.id}', 200, content.encode(), None)

    def test_attachment_download_bad_state(self):
        download = models.AttachmentDownload(content='dummy'.encode(),
                                             attachment=self.make_attachment('http://foo'),
                                             state=models.AttachmentState.Failed.value)
        download.save()

        self.validate_attachment_download(f'{settings.EXTERNAL_URL}/attachment/{download.id}', 417, None, None)

    def test_attachment_download_not_found(self):
        self.validate_attachment_download(f'{settings.EXTERNAL_URL}/attachment/12', 404, None, None)

    def test_download_all_attachments(self):
        self.downloader.stop()

        self.backend.downloader = self.downloader

        message = fbchat.Message(text='message')

        message.uid = '14'
        message.author = self.author.uid
        message.timestamp = 0

        message_attachment = fbchat.FileAttachment()
        message_attachment.uid = '15'
        message_attachment.url =  f'http://127.0.0.1:{TEST_PORT}/a'
        message_attachment.name = 'name'
        message_attachment.size = 12

        location_attachment = fbchat.LocationAttachment()
        location_attachment.latitude = 0
        location_attachment.longtitude = 0
        location_attachment.uid = 16
        location_attachment.image_url = 'foo'
        location_attachment.url = 'bar'

        message.attachments = [message_attachment, location_attachment]

        def fetch_message(id, conv):
            self.assertEquals(id, message.uid)
            self.assertEquals(conv, self.author.uid)

            return message

        self.backend.fetchMessageInfo = fetch_message

        msg = self.backend.process_message(message, self.author.uid, fbchat.ThreadType.USER)

        serve, _ = self.serve('a', 'acontent', 'ct')

        self.backend.download_all_attachments(cooldown=0)

        self.backend.downloader.start()

        self.backend.downloader.sync()

        self.assertEquals(models.AttachmentDownload.objects.count(), 1)


        download = models.AttachmentDownload.objects.filter(attachment=msg.attachments.first()).first()

        self.assertEquals(download.state, models.AttachmentState.Available.value)
        self.assertEquals(download.last_download_url, f'http://127.0.0.1:{TEST_PORT}/a')
        self.assertEquals(download.content, 'acontent'.encode())
        self.assertEquals(download.web_content_type, 'ct')
        self.assertEquals(download.errors.count(), 0)
        self.assertEquals(download.backoff, 1)
        self.assertEquals(serialize(download.attachment)['hosted_url'], f'{settings.EXTERNAL_URL}/attachment/{download.id}')

        # Check that attachment isn't downloaded again
        def fetch_message(id, conv):
            assert False

        self.backend.downloader.stop()

        self.backend.download_all_attachments(cooldown=0)
        self.backend.downloader.start()

        download.content = None
        download.state = models.AttachmentState.Failed.value
        download.save()

        # Check that failed attachments aren't retried
        self.backend.download_all_attachments(cooldown=0)

        self.assertEquals(download.state, models.AttachmentState.Failed.value)
        self.assertEquals(download.errors.count(), 0)

