expected_messages_body='''
[
  {
    "attachments": [],
    "author": {
      "display_image": null,
      "display_name": "fb_contact_1",
      "id": 4,
      "is_me": false,
      "last_updated": "2019-11-24T15:08:48.171850Z",
      "sources": [
        {
          "display_image": null,
          "display_name": "fb_contact_1",
          "facebook_user_id": "1",
          "id": 2,
          "is_me": false,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "source": {
            "display_name": "display_name",
            "errors": [],
            "facebook_user_id": null,
            "id": 1,
            "last_updated": "2019-11-24T15:08:48.171850Z",
            "refresh_time": 180.0,
            "status": "Running",
            "type": "Source/FacebookSource",
            "user_email": "user@domain.org"
          },
          "type": "SourceContact/FacebookSourceContact"
        }
      ],
      "type": "Contact"
    },
    "conversation": {
      "unread_count": 3,
      "display_image": null,
      "display_name": "convesation_1",
      "errors": [],
      "facebook_conversation_id": "1",
      "facebook_conversation_type": "user",
      "facebook_last_message_timestamp": null,
      "gone_members": [],
      "id": 6,
      "last_message": {
        "id": 14,
        "type": "Message/FacebookMessage"
      },
      "last_read_message": null,
      "last_updated": "2019-11-24T15:08:48.171850Z",
      "members": [
        {
          "display_image": null,
          "display_name": "fb_contact_1",
          "id": 4,
          "is_me": false,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "sources": [
            {
              "display_image": null,
              "display_name": "fb_contact_1",
              "facebook_user_id": "1",
              "id": 2,
              "is_me": false,
              "last_updated": "2019-11-24T15:08:48.171850Z",
              "source": {
                "display_name": "display_name",
                "errors": [],
                "facebook_user_id": null,
                "id": 1,
                "last_updated": "2019-11-24T15:08:48.171850Z",
                "refresh_time": 180.0,
                "status": "Running",
                "type": "Source/FacebookSource",
                "user_email": "user@domain.org"
              },
              "type": "SourceContact/FacebookSourceContact"
            }
          ],
          "type": "Contact"
        },
        {
          "display_image": null,
          "display_name": "fb_contact_2",
          "id": 5,
          "is_me": false,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "sources": [
            {
              "display_image": null,
              "display_name": "fb_contact_2",
              "facebook_user_id": "2",
              "id": 3,
              "is_me": false,
              "last_updated": "2019-11-24T15:08:48.171850Z",
              "source": {
                "display_name": "display_name",
                "errors": [],
                "facebook_user_id": null,
                "id": 1,
                "last_updated": "2019-11-24T15:08:48.171850Z",
                "refresh_time": 180.0,
                "status": "Running",
                "type": "Source/FacebookSource",
                "user_email": "user@domain.org"
              },
              "type": "SourceContact/FacebookSourceContact"
            }
          ],
          "type": "Contact"
        }
      ],
      "read_markers": [],
      "source": {
        "display_name": "display_name",
        "errors": [],
        "facebook_user_id": null,
        "id": 1,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "refresh_time": 180.0,
        "status": "Running",
        "type": "Source/FacebookSource",
        "user_email": "user@domain.org"
      },
      "type": "Conversation/FacebookConversation"
    },
    "edited_timestamp": null,
    "facebook_message_id": "14",
    "id": 14,
    "last_updated": "2019-11-24T15:08:48.171850Z",
    "reactions": [],
    "reply_to": {
      "attachments": [],
      "author": {
        "display_image": null,
        "display_name": "fb_contact_2",
        "id": 5,
        "is_me": false,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "sources": [
          {
            "display_image": null,
            "display_name": "fb_contact_2",
            "facebook_user_id": "2",
            "id": 3,
            "is_me": false,
            "last_updated": "2019-11-24T15:08:48.171850Z",
            "source": {
              "display_name": "display_name",
              "errors": [],
              "facebook_user_id": null,
              "id": 1,
              "last_updated": "2019-11-24T15:08:48.171850Z",
              "refresh_time": 180.0,
              "status": "Running",
              "type": "Source/FacebookSource",
              "user_email": "user@domain.org"
            },
            "type": "SourceContact/FacebookSourceContact"
          }
        ],
        "type": "Contact"
      },
      "conversation": {
        "unread_count": 3,
        "display_image": null,
        "display_name": "convesation_1",
        "errors": [],
        "facebook_conversation_id": "1",
        "facebook_conversation_type": "user",
        "facebook_last_message_timestamp": null,
        "gone_members": [],
        "id": 6,
        "last_message": {
          "id": 14,
          "type": "Message/FacebookMessage"
        },
        "last_read_message": null,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "members": [
          {
            "display_image": null,
            "display_name": "fb_contact_1",
            "id": 4,
            "is_me": false,
            "last_updated": "2019-11-24T15:08:48.171850Z",
            "sources": [
              {
                "display_image": null,
                "display_name": "fb_contact_1",
                "facebook_user_id": "1",
                "id": 2,
                "is_me": false,
                "last_updated": "2019-11-24T15:08:48.171850Z",
                "source": {
                  "display_name": "display_name",
                  "errors": [],
                  "facebook_user_id": null,
                  "id": 1,
                  "last_updated": "2019-11-24T15:08:48.171850Z",
                  "refresh_time": 180.0,
                  "status": "Running",
                  "type": "Source/FacebookSource",
                  "user_email": "user@domain.org"
                },
                "type": "SourceContact/FacebookSourceContact"
              }
            ],
            "type": "Contact"
          },
          {
            "display_image": null,
            "display_name": "fb_contact_2",
            "id": 5,
            "is_me": false,
            "last_updated": "2019-11-24T15:08:48.171850Z",
            "sources": [
              {
                "display_image": null,
                "display_name": "fb_contact_2",
                "facebook_user_id": "2",
                "id": 3,
                "is_me": false,
                "last_updated": "2019-11-24T15:08:48.171850Z",
                "source": {
                  "display_name": "display_name",
                  "errors": [],
                  "facebook_user_id": null,
                  "id": 1,
                  "last_updated": "2019-11-24T15:08:48.171850Z",
                  "refresh_time": 180.0,
                  "status": "Running",
                  "type": "Source/FacebookSource",
                  "user_email": "user@domain.org"
                },
                "type": "SourceContact/FacebookSourceContact"
              }
            ],
            "type": "Contact"
          }
        ],
        "read_markers": [],
        "source": {
          "display_name": "display_name",
          "errors": [],
          "facebook_user_id": null,
          "id": 1,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "refresh_time": 180.0,
          "status": "Running",
          "type": "Source/FacebookSource",
          "user_email": "user@domain.org"
        },
        "type": "Conversation/FacebookConversation"
      },
      "edited_timestamp": null,
      "facebook_message_id": "13",
      "id": 13,
      "last_updated": "2019-11-24T15:08:48.171850Z",
      "reactions": [],
      "reply_to": null,
      "text": "message13",
      "timestamp": "2019-11-24T15:09:01.171850Z",
      "type": "Message/FacebookMessage"
    },
    "text": "message14",
    "timestamp": "2019-11-24T15:09:02.171850Z",
    "type": "Message/FacebookMessage"
  },
  {
    "attachments": [],
    "author": {
      "display_image": null,
      "display_name": "fb_contact_2",
      "id": 5,
      "is_me": false,
      "last_updated": "2019-11-24T15:08:48.171850Z",
      "sources": [
        {
          "display_image": null,
          "display_name": "fb_contact_2",
          "facebook_user_id": "2",
          "id": 3,
          "is_me": false,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "source": {
            "display_name": "display_name",
            "errors": [],
            "facebook_user_id": null,
            "id": 1,
            "last_updated": "2019-11-24T15:08:48.171850Z",
            "refresh_time": 180.0,
            "status": "Running",
            "type": "Source/FacebookSource",
            "user_email": "user@domain.org"
          },
          "type": "SourceContact/FacebookSourceContact"
        }
      ],
      "type": "Contact"
    },
    "conversation": {
      "unread_count": 3,
      "display_image": null,
      "display_name": "convesation_1",
      "errors": [],
      "facebook_conversation_id": "1",
      "facebook_conversation_type": "user",
      "facebook_last_message_timestamp": null,
      "gone_members": [],
      "id": 6,
      "last_message": {
        "attachments": [],
        "author": {
          "display_image": null,
          "display_name": "fb_contact_1",
          "id": 4,
          "is_me": false,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "sources": [
            {
              "display_image": null,
              "display_name": "fb_contact_1",
              "facebook_user_id": "1",
              "id": 2,
              "is_me": false,
              "last_updated": "2019-11-24T15:08:48.171850Z",
              "source": {
                "display_name": "display_name",
                "errors": [],
                "facebook_user_id": null,
                "id": 1,
                "last_updated": "2019-11-24T15:08:48.171850Z",
                "refresh_time": 180.0,
                "status": "Running",
                "type": "Source/FacebookSource",
                "user_email": "user@domain.org"
              },
              "type": "SourceContact/FacebookSourceContact"
            }
          ],
          "type": "Contact"
        },
        "conversation": {
          "id": 6,
          "type": "Conversation/FacebookConversation"
        },
        "edited_timestamp": null,
        "facebook_message_id": "14",
        "id": 14,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "reactions": [],
        "reply_to": {
          "id": 13,
          "type": "Message/FacebookMessage"
        },
        "text": "message14",
        "timestamp": "2019-11-24T15:09:02.171850Z",
        "type": "Message/FacebookMessage"
      },
      "last_read_message": null,
      "last_updated": "2019-11-24T15:08:48.171850Z",
      "members": [
        {
          "display_image": null,
          "display_name": "fb_contact_1",
          "id": 4,
          "is_me": false,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "sources": [
            {
              "display_image": null,
              "display_name": "fb_contact_1",
              "facebook_user_id": "1",
              "id": 2,
              "is_me": false,
              "last_updated": "2019-11-24T15:08:48.171850Z",
              "source": {
                "display_name": "display_name",
                "errors": [],
                "facebook_user_id": null,
                "id": 1,
                "last_updated": "2019-11-24T15:08:48.171850Z",
                "refresh_time": 180.0,
                "status": "Running",
                "type": "Source/FacebookSource",
                "user_email": "user@domain.org"
              },
              "type": "SourceContact/FacebookSourceContact"
            }
          ],
          "type": "Contact"
        },
        {
          "display_image": null,
          "display_name": "fb_contact_2",
          "id": 5,
          "is_me": false,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "sources": [
            {
              "display_image": null,
              "display_name": "fb_contact_2",
              "facebook_user_id": "2",
              "id": 3,
              "is_me": false,
              "last_updated": "2019-11-24T15:08:48.171850Z",
              "source": {
                "display_name": "display_name",
                "errors": [],
                "facebook_user_id": null,
                "id": 1,
                "last_updated": "2019-11-24T15:08:48.171850Z",
                "refresh_time": 180.0,
                "status": "Running",
                "type": "Source/FacebookSource",
                "user_email": "user@domain.org"
              },
              "type": "SourceContact/FacebookSourceContact"
            }
          ],
          "type": "Contact"
        }
      ],
      "read_markers": [],
      "source": {
        "display_name": "display_name",
        "errors": [],
        "facebook_user_id": null,
        "id": 1,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "refresh_time": 180.0,
        "status": "Running",
        "type": "Source/FacebookSource",
        "user_email": "user@domain.org"
      },
      "type": "Conversation/FacebookConversation"
    },
    "edited_timestamp": null,
    "facebook_message_id": "13",
    "id": 13,
    "last_updated": "2019-11-24T15:08:48.171850Z",
    "reactions": [],
    "reply_to": null,
    "text": "message13",
    "timestamp": "2019-11-24T15:09:01.171850Z",
    "type": "Message/FacebookMessage"
  },
  {
    "attachments": [],
    "author": {
      "display_image": null,
      "display_name": "fb_contact_1",
      "id": 4,
      "is_me": false,
      "last_updated": "2019-11-24T15:08:48.171850Z",
      "sources": [
        {
          "display_image": null,
          "display_name": "fb_contact_1",
          "facebook_user_id": "1",
          "id": 2,
          "is_me": false,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "source": {
            "display_name": "display_name",
            "errors": [],
            "facebook_user_id": null,
            "id": 1,
            "last_updated": "2019-11-24T15:08:48.171850Z",
            "refresh_time": 180.0,
            "status": "Running",
            "type": "Source/FacebookSource",
            "user_email": "user@domain.org"
          },
          "type": "SourceContact/FacebookSourceContact"
        }
      ],
      "type": "Contact"
    },
    "conversation": {
      "unread_count": 3,
      "display_image": null,
      "display_name": "convesation_1",
      "errors": [],
      "facebook_conversation_id": "1",
      "facebook_conversation_type": "user",
      "facebook_last_message_timestamp": null,
      "gone_members": [],
      "id": 6,
      "last_message": {
        "attachments": [],
        "author": {
          "display_image": null,
          "display_name": "fb_contact_1",
          "id": 4,
          "is_me": false,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "sources": [
            {
              "display_image": null,
              "display_name": "fb_contact_1",
              "facebook_user_id": "1",
              "id": 2,
              "is_me": false,
              "last_updated": "2019-11-24T15:08:48.171850Z",
              "source": {
                "display_name": "display_name",
                "errors": [],
                "facebook_user_id": null,
                "id": 1,
                "last_updated": "2019-11-24T15:08:48.171850Z",
                "refresh_time": 180.0,
                "status": "Running",
                "type": "Source/FacebookSource",
                "user_email": "user@domain.org"
              },
              "type": "SourceContact/FacebookSourceContact"
            }
          ],
          "type": "Contact"
        },
        "conversation": {
          "id": 6,
          "type": "Conversation/FacebookConversation"
        },
        "edited_timestamp": null,
        "facebook_message_id": "14",
        "id": 14,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "reactions": [],
        "reply_to": {
          "attachments": [],
          "author": {
            "display_image": null,
            "display_name": "fb_contact_2",
            "id": 5,
            "is_me": false,
            "last_updated": "2019-11-24T15:08:48.171850Z",
            "sources": [
              {
                "display_image": null,
                "display_name": "fb_contact_2",
                "facebook_user_id": "2",
                "id": 3,
                "is_me": false,
                "last_updated": "2019-11-24T15:08:48.171850Z",
                "source": {
                  "display_name": "display_name",
                  "errors": [],
                  "facebook_user_id": null,
                  "id": 1,
                  "last_updated": "2019-11-24T15:08:48.171850Z",
                  "refresh_time": 180.0,
                  "status": "Running",
                  "type": "Source/FacebookSource",
                  "user_email": "user@domain.org"
                },
                "type": "SourceContact/FacebookSourceContact"
              }
            ],
            "type": "Contact"
          },
          "conversation": {
            "id": 6,
            "type": "Conversation/FacebookConversation"
          },
          "edited_timestamp": null,
          "facebook_message_id": "13",
          "id": 13,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "reactions": [],
          "reply_to": null,
          "text": "message13",
          "timestamp": "2019-11-24T15:09:01.171850Z",
          "type": "Message/FacebookMessage"
        },
        "text": "message14",
        "timestamp": "2019-11-24T15:09:02.171850Z",
        "type": "Message/FacebookMessage"
      },
      "last_read_message": null,
      "last_updated": "2019-11-24T15:08:48.171850Z",
      "members": [
        {
          "display_image": null,
          "display_name": "fb_contact_1",
          "id": 4,
          "is_me": false,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "sources": [
            {
              "display_image": null,
              "display_name": "fb_contact_1",
              "facebook_user_id": "1",
              "id": 2,
              "is_me": false,
              "last_updated": "2019-11-24T15:08:48.171850Z",
              "source": {
                "display_name": "display_name",
                "errors": [],
                "facebook_user_id": null,
                "id": 1,
                "last_updated": "2019-11-24T15:08:48.171850Z",
                "refresh_time": 180.0,
                "status": "Running",
                "type": "Source/FacebookSource",
                "user_email": "user@domain.org"
              },
              "type": "SourceContact/FacebookSourceContact"
            }
          ],
          "type": "Contact"
        },
        {
          "display_image": null,
          "display_name": "fb_contact_2",
          "id": 5,
          "is_me": false,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "sources": [
            {
              "display_image": null,
              "display_name": "fb_contact_2",
              "facebook_user_id": "2",
              "id": 3,
              "is_me": false,
              "last_updated": "2019-11-24T15:08:48.171850Z",
              "source": {
                "display_name": "display_name",
                "errors": [],
                "facebook_user_id": null,
                "id": 1,
                "last_updated": "2019-11-24T15:08:48.171850Z",
                "refresh_time": 180.0,
                "status": "Running",
                "type": "Source/FacebookSource",
                "user_email": "user@domain.org"
              },
              "type": "SourceContact/FacebookSourceContact"
            }
          ],
          "type": "Contact"
        }
      ],
      "read_markers": [],
      "source": {
        "display_name": "display_name",
        "errors": [],
        "facebook_user_id": null,
        "id": 1,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "refresh_time": 180.0,
        "status": "Running",
        "type": "Source/FacebookSource",
        "user_email": "user@domain.org"
      },
      "type": "Conversation/FacebookConversation"
    },
    "edited_timestamp": null,
    "facebook_message_id": "12",
    "id": 12,
    "last_updated": "2019-11-24T15:08:48.171850Z",
    "reactions": [],
    "reply_to": null,
    "text": "message12",
    "timestamp": "2019-11-24T15:09:00.171850Z",
    "type": "Message/FacebookMessage"
  }
]
'''

expected_messages_body_compressed = '''
[
    {
        "attachments": [],
        "author": {
            "id": 4,
            "type": "Contact"
        },
        "conversation": {
            "id": 6,
            "type": "Conversation/FacebookConversation"
        },
        "edited_timestamp": null,
        "facebook_message_id": "14",
        "id": 14,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "reactions": [],
        "reply_to": {
            "attachments": [],
            "author": {
                "id": 5,
                "type": "Contact"
            },
            "conversation": {
                "display_image": null,
                "display_name": "convesation_1",
                "errors": [],
                "facebook_conversation_id": "1",
                "facebook_conversation_type": "user",
                "facebook_last_message_timestamp": null,
                "gone_members": [],
                "id": 6,
                "last_read_message": null,
                "last_updated": "2019-11-24T15:08:48.171850Z",
                "members": [
                    {
                        "display_image": null,
                        "display_name": "fb_contact_1",
                        "id": 4,
                        "is_me": false,
                        "last_updated": "2019-11-24T15:08:48.171850Z",
                        "sources": [
                            {
                                "display_image": null,
                                "display_name": "fb_contact_1",
                                "facebook_user_id": "1",
                                "id": 2,
                                "is_me": false,
                                "last_updated": "2019-11-24T15:08:48.171850Z",
                                "source": {
                                    "id": 1,
                                    "type": "Source/FacebookSource"
                                },
                                "type": "SourceContact/FacebookSourceContact"
                            }
                        ],
                        "type": "Contact"
                    },
                    {
                        "display_image": null,
                        "display_name": "fb_contact_2",
                        "id": 5,
                        "is_me": false,
                        "last_updated": "2019-11-24T15:08:48.171850Z",
                        "sources": [
                            {
                                "display_image": null,
                                "display_name": "fb_contact_2",
                                "facebook_user_id": "2",
                                "id": 3,
                                "is_me": false,
                                "last_updated": "2019-11-24T15:08:48.171850Z",
                                "source": {
                                    "id": 1,
                                    "type": "Source/FacebookSource"
                                },
                                "type": "SourceContact/FacebookSourceContact"
                            }
                        ],
                        "type": "Contact"
                    }
                ],
                "read_markers": [],
                "source": {
                    "display_name": "display_name",
                    "errors": [],
                    "facebook_user_id": null,
                    "id": 1,
                    "last_updated": "2019-11-24T15:08:48.171850Z",
                    "refresh_time": 180.0,
                    "status": "Running",
                    "type": "Source/FacebookSource",
                    "user_email": "user@domain.org"
                },
                "type": "Conversation/FacebookConversation"
            },
            "edited_timestamp": null,
            "facebook_message_id": "13",
            "id": 13,
            "last_updated": "2019-11-24T15:08:48.171850Z",
            "reactions": [],
            "reply_to": null,
            "text": "message13",
            "timestamp": "2019-11-24T15:09:01.171850Z",
            "type": "Message/FacebookMessage"
        },
        "text": "message14",
        "timestamp": "2019-11-24T15:09:02.171850Z",
        "type": "Message/FacebookMessage"
    },
    {
        "id": 13,
        "type": "Message/FacebookMessage"
    },
    {
        "attachments": [],
        "author": {
            "id": 4,
            "type": "Contact"
        },
        "conversation": {
            "id": 6,
            "type": "Conversation/FacebookConversation"
        },
        "edited_timestamp": null,
        "facebook_message_id": "12",
        "id": 12,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "reactions": [],
        "reply_to": null,
        "text": "message12",
        "timestamp": "2019-11-24T15:09:00.171850Z",
        "type": "Message/FacebookMessage"
    }
]
'''


expected_conversation_body = '''
[
  {
    "unread_count": 3,
    "display_image": null,
    "display_name": "convesation_1",
    "errors": [],
    "facebook_conversation_id": "1",
    "facebook_conversation_type": "user",
    "facebook_last_message_timestamp": null,
    "gone_members": [],
    "id": 6,
    "last_message": {
      "attachments": [],
      "author": {
        "display_image": null,
        "display_name": "fb_contact_1",
        "id": 4,
        "is_me": false,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "sources": [
          {
            "display_image": null,
            "display_name": "fb_contact_1",
            "facebook_user_id": "1",
            "id": 2,
            "is_me": false,
            "last_updated": "2019-11-24T15:08:48.171850Z",
            "source": {
              "display_name": "display_name",
              "errors": [],
              "facebook_user_id": null,
              "id": 1,
              "last_updated": "2019-11-24T15:08:48.171850Z",
              "refresh_time": 180.0,
              "status": "Running",
              "type": "Source/FacebookSource",
              "user_email": "user@domain.org"
            },
            "type": "SourceContact/FacebookSourceContact"
          }
        ],
        "type": "Contact"
      },
      "conversation": {
        "id": 6,
        "type": "Conversation/FacebookConversation"
      },
      "edited_timestamp": null,
      "facebook_message_id": "14",
      "id": 14,
      "last_updated": "2019-11-24T15:08:48.171850Z",
      "reactions": [],
      "reply_to": {
        "attachments": [],
        "author": {
          "display_image": null,
          "display_name": "fb_contact_2",
          "id": 5,
          "is_me": false,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "sources": [
            {
              "display_image": null,
              "display_name": "fb_contact_2",
              "facebook_user_id": "2",
              "id": 3,
              "is_me": false,
              "last_updated": "2019-11-24T15:08:48.171850Z",
              "source": {
                "display_name": "display_name",
                "errors": [],
                "facebook_user_id": null,
                "id": 1,
                "last_updated": "2019-11-24T15:08:48.171850Z",
                "refresh_time": 180.0,
                "status": "Running",
                "type": "Source/FacebookSource",
                "user_email": "user@domain.org"
              },
              "type": "SourceContact/FacebookSourceContact"
            }
          ],
          "type": "Contact"
        },
        "conversation": {
          "id": 6,
          "type": "Conversation/FacebookConversation"
        },
        "edited_timestamp": null,
        "facebook_message_id": "13",
        "id": 13,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "reactions": [],
        "reply_to": null,
        "text": "message13",
        "timestamp": "2019-11-24T15:09:01.171850Z",
        "type": "Message/FacebookMessage"
      },
      "text": "message14",
      "timestamp": "2019-11-24T15:09:02.171850Z",
      "type": "Message/FacebookMessage"
    },
    "last_read_message": null,
    "last_updated": "2019-11-24T15:08:48.171850Z",
    "members": [
      {
        "display_image": null,
        "display_name": "fb_contact_1",
        "id": 4,
        "is_me": false,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "sources": [
          {
            "display_image": null,
            "display_name": "fb_contact_1",
            "facebook_user_id": "1",
            "id": 2,
            "is_me": false,
            "last_updated": "2019-11-24T15:08:48.171850Z",
            "source": {
              "display_name": "display_name",
              "errors": [],
              "facebook_user_id": null,
              "id": 1,
              "last_updated": "2019-11-24T15:08:48.171850Z",
              "refresh_time": 180.0,
              "status": "Running",
              "type": "Source/FacebookSource",
              "user_email": "user@domain.org"
            },
            "type": "SourceContact/FacebookSourceContact"
          }
        ],
        "type": "Contact"
      },
      {
        "display_image": null,
        "display_name": "fb_contact_2",
        "id": 5,
        "is_me": false,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "sources": [
          {
            "display_image": null,
            "display_name": "fb_contact_2",
            "facebook_user_id": "2",
            "id": 3,
            "is_me": false,
            "last_updated": "2019-11-24T15:08:48.171850Z",
            "source": {
              "display_name": "display_name",
              "errors": [],
              "facebook_user_id": null,
              "id": 1,
              "last_updated": "2019-11-24T15:08:48.171850Z",
              "refresh_time": 180.0,
              "status": "Running",
              "type": "Source/FacebookSource",
              "user_email": "user@domain.org"
            },
            "type": "SourceContact/FacebookSourceContact"
          }
        ],
        "type": "Contact"
      }
    ],
    "read_markers": [],
    "source": {
      "display_name": "display_name",
      "errors": [],
      "facebook_user_id": null,
      "id": 1,
      "last_updated": "2019-11-24T15:08:48.171850Z",
      "refresh_time": 180.0,
      "status": "Running",
      "type": "Source/FacebookSource",
      "user_email": "user@domain.org"
    },
    "type": "Conversation/FacebookConversation"
  }
]
'''

expected_send_message_body = '''
{
  "attachments": [], 
  "author": {
    "display_image": null, 
    "display_name": "My", 
    "id": 3, 
    "is_me": true, 
    "last_updated": "2019-11-24T15:08:48.171850Z", 
    "sources": [
      {
        "display_image": null, 
        "display_name": "My", 
        "facebook_user_id": "1234", 
        "id": 2, 
        "is_me": true, 
        "last_updated": "2019-11-24T15:08:48.171850Z", 
        "source": {
          "display_name": "display_name", 
          "errors": [], 
          "facebook_user_id": null, 
          "id": 1, 
          "last_updated": "2019-11-24T15:08:48.171850Z", 
          "refresh_time": 180.0, 
          "status": "Running", 
          "type": "Source/FacebookSource", 
          "user_email": "user@domain.org"
        }, 
        "type": "SourceContact/FacebookSourceContact"
      }
    ], 
    "type": "Contact"
  }, 
  "conversation": {
    "unread_count": 0,
    "display_image": null, 
    "display_name": "conversation", 
    "errors": [], 
    "facebook_conversation_id": "12", 
    "facebook_conversation_type": "user", 
    "facebook_last_message_timestamp": null, 
    "gone_members": [], 
    "id": 4, 
    "last_message": {
      "id": 5, 
      "type": "Message/FacebookMessage"
    }, 
    "last_read_message": {
      "id": 5, 
      "type": "Message/FacebookMessage"
    }, 
    "last_updated": "2019-11-24T15:08:48.171850Z", 
    "members": [], 
    "read_markers": [], 
    "source": {
      "display_name": "display_name", 
      "errors": [], 
      "facebook_user_id": null, 
      "id": 1, 
      "last_updated": "2019-11-24T15:08:48.171850Z", 
      "refresh_time": 180.0, 
      "status": "Running", 
      "type": "Source/FacebookSource", 
      "user_email": "user@domain.org"
    }, 
    "type": "Conversation/FacebookConversation"
  }, 
  "edited_timestamp": null, 
  "facebook_message_id": "18", 
  "id": 5, 
  "last_updated": "2019-11-24T15:08:48.171850Z", 
  "reactions": [], 
  "reply_to": null, 
  "text": "this is the text", 
  "timestamp": "2019-11-24T15:08:48.171000Z", 
  "type": "Message/FacebookMessage"
}
'''

expected_send_message_reply_to_body='''
{
  "attachments": [],
  "author": {
    "display_image": null,
    "display_name": "My",
    "id": 3,
    "is_me": true,
    "last_updated": "2019-11-24T15:08:48.171850Z",
    "sources": [
      {
        "display_image": null,
        "display_name": "My",
        "facebook_user_id": "1234",
        "id": 2,
        "is_me": true,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "source": {
          "display_name": "display_name",
          "errors": [],
          "facebook_user_id": null,
          "id": 1,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "refresh_time": 180.0,
          "status": "Running",
          "type": "Source/FacebookSource",
          "user_email": "user@domain.org"
        },
        "type": "SourceContact/FacebookSourceContact"
      }
    ],
    "type": "Contact"
  },
  "conversation": {
    "unread_count": 1,
    "display_image": null,
    "display_name": "conversation",
    "errors": [],
    "facebook_conversation_id": "12",
    "facebook_conversation_type": "user",
    "facebook_last_message_timestamp": null,
    "gone_members": [],
    "id": 4,
    "last_message": {
      "attachments": [],
      "author": {
        "display_image": null,
        "display_name": "My",
        "id": 3,
        "is_me": true,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "sources": [
          {
            "display_image": null,
            "display_name": "My",
            "facebook_user_id": "1234",
            "id": 2,
            "is_me": true,
            "last_updated": "2019-11-24T15:08:48.171850Z",
            "source": {
              "display_name": "display_name",
              "errors": [],
              "facebook_user_id": null,
              "id": 1,
              "last_updated": "2019-11-24T15:08:48.171850Z",
              "refresh_time": 180.0,
              "status": "Running",
              "type": "Source/FacebookSource",
              "user_email": "user@domain.org"
            },
            "type": "SourceContact/FacebookSourceContact"
          }
        ],
        "type": "Contact"
      },
      "conversation": {
        "id": 4,
        "type": "Conversation/FacebookConversation"
      },
      "edited_timestamp": null,
      "facebook_message_id": "fb-mid",
      "id": 5,
      "last_updated": "2019-11-24T15:08:48.171850Z",
      "reactions": [],
      "reply_to": null,
      "text": "text",
      "timestamp": "2019-11-24T15:08:48.171850Z",
      "type": "Message/FacebookMessage"
    },
    "last_read_message": {
      "id": 6,
      "type": "Message/FacebookMessage"
    },
    "last_updated": "2019-11-24T15:08:48.171850Z",
    "members": [],
    "read_markers": [],
    "source": {
      "display_name": "display_name",
      "errors": [],
      "facebook_user_id": null,
      "id": 1,
      "last_updated": "2019-11-24T15:08:48.171850Z",
      "refresh_time": 180.0,
      "status": "Running",
      "type": "Source/FacebookSource",
      "user_email": "user@domain.org"
    },
    "type": "Conversation/FacebookConversation"
  },
  "edited_timestamp": null,
  "facebook_message_id": "18",
  "id": 6,
  "last_updated": "2019-11-24T15:08:48.171850Z",
  "reactions": [],
  "reply_to": {
    "attachments": [],
    "author": {
      "display_image": null,
      "display_name": "My",
      "id": 3,
      "is_me": true,
      "last_updated": "2019-11-24T15:08:48.171850Z",
      "sources": [
        {
          "display_image": null,
          "display_name": "My",
          "facebook_user_id": "1234",
          "id": 2,
          "is_me": true,
          "last_updated": "2019-11-24T15:08:48.171850Z",
          "source": {
            "display_name": "display_name",
            "errors": [],
            "facebook_user_id": null,
            "id": 1,
            "last_updated": "2019-11-24T15:08:48.171850Z",
            "refresh_time": 180.0,
            "status": "Running",
            "type": "Source/FacebookSource",
            "user_email": "user@domain.org"
          },
          "type": "SourceContact/FacebookSourceContact"
        }
      ],
      "type": "Contact"
    },
    "conversation": {
      "unread_count": 1,
      "display_image": null,
      "display_name": "conversation",
      "errors": [],
      "facebook_conversation_id": "12",
      "facebook_conversation_type": "user",
      "facebook_last_message_timestamp": null,
      "gone_members": [],
      "id": 4,
      "last_message": {
        "id": 5,
        "type": "Message/FacebookMessage"
      },
      "last_read_message": {
        "id": 6,
        "type": "Message/FacebookMessage"
      },
      "last_updated": "2019-11-24T15:08:48.171850Z",
      "members": [],
      "read_markers": [],
      "source": {
        "display_name": "display_name",
        "errors": [],
        "facebook_user_id": null,
        "id": 1,
        "last_updated": "2019-11-24T15:08:48.171850Z",
        "refresh_time": 180.0,
        "status": "Running",
        "type": "Source/FacebookSource",
        "user_email": "user@domain.org"
      },
      "type": "Conversation/FacebookConversation"
    },
    "edited_timestamp": null,
    "facebook_message_id": "fb-mid",
    "id": 5,
    "last_updated": "2019-11-24T15:08:48.171850Z",
    "reactions": [],
    "reply_to": null,
    "text": "text",
    "timestamp": "2019-11-24T15:08:48.171850Z",
    "type": "Message/FacebookMessage"
  },
  "text": "this is the text",
  "timestamp": "2019-11-24T15:08:48.171000Z",
  "type": "Message/FacebookMessage"
}
'''
