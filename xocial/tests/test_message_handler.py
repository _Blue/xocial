import pytz
import json
import fbchat

from collections import OrderedDict
from datetime import datetime, timezone, timedelta
from django.test import Client, TestCase
from xocial import models
from xocial.models import facebook
from . import expected_bodies
from urllib.parse import urlencode
from xocial.models import meta
from .utils import TIMESTAMP
from .mocks.backend_mock import FacebookBackendMock
from xocial.backends import inject_backend

class MessageHandlerTests(TestCase):

    def setUp(self):
        meta.override_now(TIMESTAMP)

        self.source = facebook.FacebookSource(
                display_name='display_name',
                user_email='user@domain.org',
                credential='{"cred": true}',
                status=models.SourceStatus.Running.value)
        self.source.save()

        self.fb_contact_1 = facebook.FacebookSourceContact(
                source=self.source,
                display_name="fb_contact_1",
                facebook_user_id=1)
        self.fb_contact_1.save()
        self.fb_contact_1 = facebook.FacebookSourceContact.objects.all()[0]

        self.fb_contact_2 = facebook.FacebookSourceContact(
                source=self.source,
                display_name="fb_contact_2",
                facebook_user_id=2)
        self.fb_contact_2.save()


        self.contact_1 = models.Contact(display_name=self.fb_contact_1.display_name)
        self.contact_1.save()

        self.contact_1.sources.add(self.fb_contact_1)
        self.contact_1.save()

        self.contact_2 = models.Contact(display_name=self.fb_contact_2.display_name)
        self.contact_2.save()

        self.contact_2.sources.add(self.fb_contact_2)
        self.contact_2.save()


        self.conversation = facebook.FacebookConversation(
                facebook_conversation_id=1,
                facebook_conversation_type='user',
                display_name='convesation_1',
                source=self.source)
        self.conversation.save()

        self.conversation.members.add(self.contact_1)
        self.conversation.members.add(self.contact_2)
        self.conversation.save()

        def make_message(id):
            return facebook.FacebookMessage(
                    id=id,
                    text='message' + str(id),
                    timestamp=TIMESTAMP + timedelta(seconds=id),
                    author=self.contact_1 if id % 2 == 0 else self.contact_2,
                    conversation=self.conversation,
                    facebook_message_id=id)

        make_message(12).save()

        msg_13 = make_message(13)
        msg_13.save()

        reply_msg = make_message(14)
        reply_msg.reply_to = msg_13

        self.maxDiff = 1000000

        reply_msg.save()

    def tearDown(self):
        meta.override_now(None)

    def test_message_react(self):
        self.backend = FacebookBackendMock(self.source)

        inject_backend(self.backend)


        fb_message_with_reaction = fbchat.Message(text=f"message {id}")
        fb_message_with_reaction.uid = '12'
        fb_message_with_reaction.author = '1'
        fb_message_with_reaction.timestamp = int(datetime.timestamp(TIMESTAMP)) * 1000
        fb_message_with_reaction.reactions = {self.backend.uid: fbchat.MessageReaction.LOVE}

        def react_to_message(message_id, reaction):
            self.assertEqual(message_id, '12')
            self.assertEqual(reaction, fbchat.MessageReaction.LOVE)

        def fetch_messages(thread_id, limit=20, before=None):
            self.assertEqual('1', thread_id)

            return [fb_message_with_reaction]

        self.backend.reactToMessage = react_to_message
        self.backend.fetchThreadMessages = fetch_messages

        body = {'reaction': {'emoji': fbchat.MessageReaction.LOVE.value}}
        response = self.client.post(f'/api/message/12/react', json.dumps(body), content_type="application/json")
        self.assertEquals(response.status_code, 200)

        message = json.loads(response.content)
        self.assertEquals(message['reactions'][0]['author']['id'], self.backend.me_contact.id)

    def test_serialize_messages(self):
        response = self.client.get(f'/api/conversation/{self.conversation.id}/messages')
        self.assertEquals(response.status_code, 200)

        self.assertEquals(json.loads(response.content), json.loads(expected_bodies.expected_messages_body))

    def test_serialize_conversation(self):
        response = self.client.get('/api/conversation/')

        self.assertEquals(response.status_code, 200)

        body = json.loads(response.content)
        self.assertEquals(json.loads(expected_bodies.expected_conversation_body), body)

    def test_serialize_messages_compressed(self):
        response = self.client.get(f'/api/conversation/{self.conversation.id}/messages', HTTP_X_Xocial_Use_Compact_Serialization='1')
        self.assertEquals(response.status_code, 200)

        response = json.loads(response.content, object_pairs_hook=OrderedDict)

        visited = set()

        def visit(obj):
            if isinstance(obj, list):
                for e in obj:
                    visit(e)
            elif isinstance(obj, dict):
                if (set(obj.keys()) == set(['id', 'type'])):
                    self.assertTrue(obj['id'] in visited)
                else:
                    self.assertFalse(obj['id'] in visited)
                    visited.add(obj['id'])

                    for e in obj:
                        visit(obj[e])

        visit(response)

        self.assertNotEqual(0, len(visited))


    def validate_ordering(self, expected: list, from_id: int=None, count: int=None, sort: str=None):
        params = {}

        if from_id:
            params['from_id'] = from_id

        if count:
            params['count'] = count

        if sort:
            params['order'] = sort

        response = self.client.get(f'/api/message?' + urlencode(params))

        self.assertEquals(response.status_code, 200)
        body = json.loads(response.content)

        ids = [e['id'] for e in body]
        self.assertEquals(expected, ids)

    def test_from(self):
        self.validate_ordering(from_id=13, expected=[12])

    def test_from_desc(self):
        self.validate_ordering(from_id=13, sort='desc', expected=[12])

    def test_from_base_asc(self):
        self.validate_ordering(from_id=12, sort='asc', expected=[13, 14])

    def test_count(self):
        self.validate_ordering(from_id=13, count=1, expected=[12])

    def test_over(self):
        self.validate_ordering(from_id=14, count=5, expected=[13, 12])

    def test_count_desc(self):
        self.validate_ordering(from_id=14, sort='desc', count=1, expected=[13])

    def test_count_desc_base(self):
        self.validate_ordering(from_id=12, sort='desc', count=1, expected=[])

