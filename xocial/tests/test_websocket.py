from xocial import settings

import fbchat
import json
import websockets
import asyncio
import backoff
import os

from datetime import datetime, timezone
from django.test import Client, TransactionTestCase
from django import db
from xocial import models, settings, serializers
from xocial.models import facebook
from xocial.tests.mocks.backend_mock import FacebookBackendMock
from xocial.backends import inject_backend
from xocial.backends.facebook import to_fb_timestamp
from xocial.models import meta, transaction
from xocial.websocket import app
from .utils import TIMESTAMP
from threading import Thread, Event

class WebsocketTest(TransactionTestCase):

    def setUp(self):
        self.settings = self.settings()

        meta.override_now(TIMESTAMP)

        self.source = facebook.FacebookSource(
                display_name='display_name',
                user_email='user@domain.org',
                credential='{"cred": true}',
                status=models.SourceStatus.Running.value)
        self.source.save()

        self.backend = FacebookBackendMock(self.source)

        inject_backend(self.backend)

        app.sync_push = True

    def tearDown(self):
        app.sync()
        app.stop()

        meta.override_now(None)
        self.thread.join()

    def connect(self, types: list, compress=False):
        self.messages = []

        app.start()

        propagate_update = app.runner.propagate_update

        @backoff.on_exception(backoff.constant, (Exception), max_tries=3, interval=1)
        async def retry_propagate(*args, **kwargs):
            return await propagate_update(*args, **kwargs)

        app.runner.propagate_update = retry_propagate

        self.thread = Thread(target=self.connect_impl, args=(types, compress))
        self.event = Event()
        self.thread.start()

        self.event.wait()

    def connect_impl(self, types: list, compress: bool):
        self.loop = asyncio.new_event_loop()
        self.loop.run_until_complete(self.read_websocket(types, compress))


    @backoff.on_exception(backoff.constant, (ConnectionRefusedError, OSError), max_tries=3, interval=1)
    async def read_websocket(self, types: list, compress: False):
        self.connection = await websockets.connect(f"ws://127.0.0.1:{settings.WEBSOCKET_LISTEN_PORT}")

        message = {'action': 'subscribe', 'types': types, 'compress': compress}
        await self.connection.send(json.dumps(message))

        response = json.loads(await self.connection.recv())
        self.assertEquals(response['result'], 'OK')

        self.event.set()

        while True:
            try:
                self.messages.append(json.loads(await self.connection.recv()))
            except:
                break

    def done(self):
        app.sync()
        app.stop()
        self.thread.join()


    def test_new_conversation_is_sent_on_websocket(self):
        self.connect(['Conversation'])

        conversation = facebook.FacebookConversation(
                facebook_conversation_id = 'dummy',
                facebook_conversation_type = 'user',
                display_name = 'conversation',
                source = self.source)

        conversation.save()

        self.done()

        self.assertEquals([conversation.id], [e['id'] for e in self.messages])

    def test_new_conversation_is_sent_on_websocket_compressed(self):
        self.connect(['Conversation'], compress=True)

        conversation = facebook.FacebookConversation(
                facebook_conversation_id = 'dummy',
                facebook_conversation_type = 'user',
                display_name = 'conversation',
                source = self.source)

        conversation.save()

        self.done()

        compressed_body = json.dumps(serializers.serialize(conversation, compress=True))

        self.assertEquals(compressed_body, json.dumps(self.messages[0]))



    def test_new_conversation_is_not_sent_on_websocket_if_not_requested(self):
        self.connect(['Message'])

        conversation = facebook.FacebookConversation(
                facebook_conversation_id = 'dummy',
                facebook_conversation_type = 'user',
                display_name = 'conversation',
                source = self.source)

        conversation.save()

        self.done()

        self.assertEquals([], [e['id'] for e in self.messages])

    def test_message_reaction_triggers_update(self):
        conversation = facebook.FacebookConversation(
                facebook_conversation_id = 'dummy',
                facebook_conversation_type = 'user',
                display_name = 'conversation',
                source = self.source)
        conversation.save()

        contact = models.Contact(display_name="contact_1")
        contact.save()

        message = models.Message(
                author=contact,
                conversation=conversation,
                timestamp=TIMESTAMP,
                text='text')
        message.save()

        reaction = models.MessageReaction(author=contact)
        reaction.save()

        self.connect(['Message'])

        message.reactions.add(reaction)

        self.done()

        self.assertEquals([message.id], [e['id'] for e in self.messages])
        self.assertEquals([reaction.id], [e['id'] for e in self.messages[0]['reactions']])


    def test_transaction_factors_updates(self):
        self.connect(['Conversation'])

        with transaction.transaction():
            conversation = facebook.FacebookConversation(
                    facebook_conversation_id = 'dummy',
                    facebook_conversation_type = 'user',
                    display_name = 'conversation',
                    source = self.source)

            conversation.save()
            conversation.display_name = 'foo'
            conversation.save() # Generate a second message


        self.done()

        self.assertEquals([conversation.id], [e['id'] for e in self.messages])
        self.assertEquals('foo', self.messages[0]['display_name'])

    def test_failed_transaction_does_not_send_updates(self):
        self.connect(['Conversation'])

        try:
            with transaction.transaction():
                conversation = facebook.FacebookConversation(
                        facebook_conversation_id = 'dummy',
                        facebook_conversation_type = 'user',
                        display_name = 'conversation',
                        source = self.source)

                conversation.save()

                raise RuntimeError('Dummy')
        except RuntimeError:
            pass


        self.done()

        self.assertEquals([], [e['id'] for e in self.messages])
        self.assertEquals(0, facebook.FacebookConversation.objects.count())
