import pytz
import json

from datetime import datetime, timezone
from django.test import Client, TestCase
from xocial.tests.mocks.backend_mock import FacebookBackendMock
from xocial.backends import inject_backend
from xocial.backends.facebook import to_fb_timestamp
from xocial import models
from xocial.models import facebook
from xocial.models.meta import override_now
from .utils import TIMESTAMP


class ConversationTests(TestCase):

    def setUp(self):
        override_now(TIMESTAMP)
        self.source = facebook.FacebookSource(
                display_name='display_name',
                user_email='user@domain.org',
                credential='{"cred": true}',
                status=models.SourceStatus.Running.value)
        self.source.save()

        self.contact_1 = models.Contact(display_name='contact_1')
        self.contact_1.save()

        self.conversation = facebook.FacebookConversation(
                facebook_conversation_id=1,
                facebook_conversation_type='user',
                display_name='convesation_1',
                source=self.source)
        self.conversation.save()

        self.conversation.members.add(self.contact_1)
        self.conversation.save()

        self.conversation_2 = facebook.FacebookConversation(
                facebook_conversation_id=2,
                facebook_conversation_type='user',
                display_name='convesation_2',
                source=self.source)

        self.conversation_2.save()


        def make_message(id, conv):
            return facebook.FacebookMessage(
                    id=id,
                    text='message' + str(id),
                    timestamp=TIMESTAMP,
                    author=self.contact_1,
                    conversation=conv,
                    facebook_message_id=id)

        self.message_1 = make_message(10, self.conversation)
        self.message_1.save()

        self.message_2 = make_message(11, self.conversation_2)
        self.message_2.save()

    def tearDown(self):
        override_now(None)


    def get_conversation(self):
        response = self.client.get(f'/api/conversation/{self.conversation.id}/')
        self.assertEquals(response.status_code, 200)

        return json.loads(response.content)

    def expect_unread_conversations(self, expected_ids):
        response = self.client.get(f'/api/conversation/unread/')
        self.assertEqual(response.status_code, 200)

        body = json.loads(response.content)

        ids = set([e['id'] for e in body])

        self.assertEquals(set(expected_ids), ids)


    def test_unread_conversations(self):
        self.expect_unread_conversations([self.conversation.id, self.conversation_2.id])

    def test_unread_conversations_1_read(self):
        self.conversation.last_read_message = self.message_1
        self.conversation.save()
        self.expect_unread_conversations([self.conversation_2.id])

    def test_unread_conversations_last_message_not_read(self):
        last_message = facebook.FacebookMessage(
                    text='message4',
                    timestamp=TIMESTAMP.replace(hour=23),
                    author=self.contact_1,
                    conversation=self.conversation,
                    facebook_message_id='fbid3')
        last_message.save()

        self.conversation.last_read_message = self.message_1
        self.conversation.save()
        self.expect_unread_conversations([self.conversation_2.id, self.conversation.id])


    def test_all_conversation_read(self):
        self.conversation.last_read_message = self.message_1
        self.conversation.save()

        self.conversation_2.last_read_message = self.message_2
        self.conversation_2.save()

        self.expect_unread_conversations([])

    def test_no_last_read_message(self):
        conversation = self.get_conversation()

        self.assertIsNone(conversation['last_read_message'])


    def test_set_last_read_message(self):
        body = {'message_id': self.message_1.id}
        response = self.client.post(f'/api/message/{self.message_1.id}/read', json.dumps(body), content_type="application/json")

        self.assertEqual(response.status_code, 200)

        response = self.client.get(f'/api/message/{self.message_1.id}/')
        self.assertEquals(response.status_code, 200)

        message = json.loads(response.content)
        conversation = self.get_conversation()
        self.assertEqual(self.message_1.id, conversation['last_read_message']['id'])

        self.assertEqual(message, json.loads(response.content))

    def test_set_last_read_message_wrong_type(self):
        body = {}
        response = self.client.post(f'/api/message/{self.conversation.id}/read', json.dumps(body), content_type="application/json")

        self.assertEqual(response.status_code, 404)

    def test_set_last_read_message_with_backend(self):
        self.backend = FacebookBackendMock(self.source)
        inject_backend(self.backend)


        called = False
        def mark_as_read(thread_ids: list):
            nonlocal called

            self.assertEqual([str(self.conversation.facebook_conversation_id)], thread_ids)

            called = True

        self.backend.markAsRead= mark_as_read

        body = {'update_backend': True}
        response = self.client.post(f'/api/message/{self.message_1.id}/read', json.dumps(body), content_type="application/json")
        self.assertEqual(response.status_code, 200)

        self.assertEqual(self.message_1.id, json.loads(response.content)['conversation']['last_read_message']['id'])

        self.assertTrue(called)

    def test_last_message(self):
        self.message_1.timestamp = TIMESTAMP.replace(second=58)
        self.message_1.save()

        self.message_2.timestamp = TIMESTAMP.replace(second=59)
        self.message_2.conversation = self.conversation
        self.message_2.save()

        response = self.client.get(f'/api/conversation/')
        self.assertEquals(response.status_code, 200)

        content = json.loads(response.content)
        self.assertEquals(2, len(content))

        self.assertEquals(self.message_2.id, content[0]['last_message']['id'])
        self.assertEquals(None, content[1]['last_message'])

        self.message_2.timestamp = TIMESTAMP.replace(second=57)
        self.message_2.save()

        response = self.client.get(f'/api/conversation/')

        content = json.loads(response.content)
        self.assertEquals(self.message_1.id, content[0]['last_message']['id'])

    def test_unread_count(self):
        self.message_1.timestamp = TIMESTAMP.replace(second=58)
        self.message_1.save()

        self.message_2.timestamp = TIMESTAMP.replace(second=59)
        self.message_2.conversation = self.conversation
        self.message_2.save()

        response = self.client.get(f'/api/conversation/')
        self.assertEquals(response.status_code, 200)
        content = json.loads(response.content)
        self.assertEquals(2, content[0]['unread_count'])

        self.conversation.last_read_message = self.message_1
        self.conversation.save()

        response = self.client.get(f'/api/conversation/')
        self.assertEquals(response.status_code, 200)
        content = json.loads(response.content)
        self.assertEquals(1, content[0]['unread_count'])

        self.conversation.last_read_message = self.message_2
        self.conversation.save()

        response = self.client.get(f'/api/conversation/')
        self.assertEquals(response.status_code, 200)
        content = json.loads(response.content)
        self.assertEquals(0, content[0]['unread_count'])
