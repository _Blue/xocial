import pytz
import json
from datetime import datetime, timezone
from django.test import Client, TestCase
from xocial import models


TIMESTAMP = datetime.strptime('2019-11-24T15:08:49.000', '%Y-%m-%dT%H:%M:%S.%f').replace(tzinfo=timezone.utc)

class ErrorTest(TestCase):

    def setUp(self):
        self.source = models.Source(
                display_name='display_name',
                status=models.SourceStatus.Running.value)
        self.source.save()

    def test_error_is_acknowledged(self):
        error = models.Error()
        error.code = "code"
        error.details = "details"
        error.save()

        self.source.errors.add(error)
        self.source.save()

        response = self.client.get(f'/api/source/{self.source.id}')
        self.assertEqual(200, response.status_code)

        body = json.loads(response.content)

        self.assertEqual(error.code, body['errors'][0]['code'])
        self.assertEqual(error.details, body['errors'][0]['details'])

        # Acknowledge the error
        response = self.client.post(f'/api/error/{error.id}/acknowledge')
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, json.loads(response.content)['affected'])


        # Check that is has been removed from object
        response = self.client.get(f'/api/source/{self.source.id}')
        self.assertEqual(200, response.status_code)
        self.assertEqual([], json.loads(response.content)['errors'])
