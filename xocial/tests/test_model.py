import pytz
from datetime import datetime, timezone
from django.test import Client, TestCase
from xocial import models, settings
from xocial.models import transaction
from .utils import TIMESTAMP


class ModelTest(TestCase):

    def setUp(self):
        self.source = models.Source(display_name='source')
        self.source.save()

        self.conversation = models.Conversation(source=self.source)

        self.conversation.save()
        self.conversation.refresh_from_db()

        self.updated = self.conversation.last_updated

    def test_new_object_has_updated_time_set(self):
        self.assertIsNotNone(self.conversation.last_updated)

    def test_updating_string_field_updates_time(self):
        self.conversation.display_name = 'foo'
        self.conversation.save()

        self.assertNotEqual(self.updated, self.conversation.last_updated)

    def test_updating_visible_by_id_field_updates_time_(self):
        contact = models.Contact(display_name = "name")
        contact.save()

        message = models.Message(author = contact, text = "text", timestamp=TIMESTAMP, conversation = self.conversation)
        message.save()

        self.conversation.last_read_message = message

        self.conversation.save()

        self.assertNotEqual(self.updated, self.conversation.last_updated)


    def test_updating_foreign_key_field_updates_time(self):
        source = models.Source(display_name='source2')
        source.save()

        self.conversation.source = source
        self.conversation.save()

        self.assertNotEqual(self.updated, self.conversation.last_updated)


    def test_not_updating_field_does_no_update_time(self):
        self.conversation.display_name = self.conversation.display_name

        self.conversation.save()

        self.assertEqual(self.updated, self.conversation.last_updated)

    def test_updating_many_to_many_field_updates_time(self):
        contact_1 = models.Contact(display_name="contact_1")
        contact_1.save()

        contact_2 = models.Contact(display_name="contact_2")
        contact_2.save()

        self.conversation.members.add(contact_1)
        self.conversation.members.add(contact_2)

        self.assertNotEqual(self.updated, self.conversation.last_updated)
        ts_1 = self.conversation.last_updated

        self.conversation.members.remove(contact_1)

        ts_2 = self.conversation.last_updated
        self.assertNotEqual(ts_1, ts_2)

        self.conversation.members.clear()
        ts_3 = self.conversation.last_updated
        self.assertNotEqual(ts_2, ts_3)

        self.conversation.save()
        self.assertEqual(ts_3, self.conversation.last_updated)


    def test_adding_message_updates_conversation(self):
        ts_1 = self.conversation.last_updated

        contact = models.Contact(display_name="contact_1")
        contact.save()

        message = models.Message(
                author=contact,
                conversation=self.conversation,
                timestamp=TIMESTAMP,
                text='text')

        message.save()

        self.conversation.refresh_from_db()
        ts_2 = self.conversation.last_updated

        self.assertNotEqual(ts_2, ts_1)

        message.text = 'text_2'
        message.save()

        self.conversation.refresh_from_db()
        self.assertNotEqual(ts_2, self.conversation.last_updated)


    def test_saving_message_does_not_update(self):
        contact = models.Contact(display_name="contact_1")
        contact.save()

        message = models.Message(
                author=contact,
                conversation=self.conversation,
                timestamp=TIMESTAMP,
                text='text')

        message.save()

        ts_1 = message.last_updated

        message.save()

        self.assertEqual(ts_1, message.last_updated)

    def test_new_message_reaction_updates_message(self):
        contact = models.Contact(display_name="contact_1")
        contact.save()

        message = models.Message(
                author=contact,
                conversation=self.conversation,
                timestamp=TIMESTAMP,
                text='text')

        message.save()

        ts_1 = message.last_updated

        reaction = models.MessageReaction(author=contact)
        reaction.save()
        message.reactions.add(reaction)
        message.save()

        self.assertNotEqual(ts_1, message.last_updated)



    def test_updating_read_marker_updates_conversation(self):
        ts_1 = self.conversation.last_updated

        contact = models.Contact(display_name="contact_1")
        contact.save()

        message = models.Message(
                author=contact,
                conversation=self.conversation,
                timestamp=TIMESTAMP,
                text='text')
        message.save()

        message2 = models.Message(
                author=contact,
                conversation=self.conversation,
                timestamp=TIMESTAMP,
                text='text')


        message2.save()

        read_marker = models.ReadMarker(message=message, timestamp=TIMESTAMP, contact=contact)
        read_marker.save()

        self.conversation.refresh_from_db()
        ts_2 = self.conversation.last_updated

        self.assertNotEqual(ts_2, ts_1)

        read_marker.timestamp = TIMESTAMP.replace(hour=23)
        read_marker.save()

        self.conversation.refresh_from_db()
        self.assertNotEqual(ts_2, self.conversation.last_updated)

        read_marker.save()
        ts_3 = self.conversation.last_updated # No change, shouldn't update the conversation

        self.conversation.refresh_from_db()
        self.assertEqual(ts_3, self.conversation.last_updated)


        ts_4 = self.conversation.last_updated # No change, shouldn't update the conversation
        read_marker.message = message2
        read_marker.save()

        self.conversation.refresh_from_db()
        self.assertNotEqual(ts_4, self.conversation.last_updated)


    def test_subsequent_update_changes_last_updated(self):
        contact = models.Contact(display_name="contact_1")
        contact.save()


        for i in range(0, 25):
            ts = contact.last_updated
            contact.display_name = str(i)
            contact.save()
            ts_2 = contact.last_updated
            contact.refresh_from_db()
            self.assertNotEqual(ts, ts_2)


    def test_transaction_run_tasks(self):
        flag = False
        with transaction.transaction():
            def task():
                nonlocal flag
                flag = True

            transaction.push_transaction_task(task)


        self.assertTrue(flag)

    def test_failed_transaction_does_not_run_tasks(self):
        flag = False

        try:
            with transaction.transaction():
                def task():
                    nonlocal flag
                    flag = True

                transaction.push_transaction_task(task)
                raise RuntimeError('Dummy')
        except:
            pass


        self.assertFalse(flag)

    def test_deep_transaction_run_tasks(self):
        flag = 0
        with transaction.transaction():
            def task():
                nonlocal flag
                flag = flag + 1

            transaction.push_transaction_task(task)

            with transaction.transaction():
                transaction.push_transaction_task(task)

        self.assertEqual(flag, 2)


    def test_failed_deep_transaction_does_not_run_tasks(self):
        flag = 0

        try:
            with transaction.transaction():
                def task():
                    nonlocal flag
                    flag = flag + 1

                transaction.push_transaction_task(task)

                with transaction.transaction():
                    transaction.push_transaction_task(task)
                    raise RuntimeError('Dummy')
        except:
            pass


        self.assertEqual(flag, 0)


    def test_failed_deep_transaction_does_not_run_tasks_2(self):
        flag = 0

        try:
            with transaction.transaction():
                def task():
                    nonlocal flag
                    flag = flag + 1

                transaction.push_transaction_task(task)

                with transaction.transaction():
                    transaction.push_transaction_task(task)
                raise RuntimeError('Dummy')
        except:
            pass


        self.assertEqual(flag, 0)

