import pytz
import json
import urllib.parse

from datetime import datetime, timezone, timedelta
from django.test import Client, TestCase
from xocial import models
from xocial.models import facebook
from . import expected_bodies
from urllib.parse import urlencode
from xocial.models import meta

TIMESTAMP = datetime.strptime('2019-11-24T15:08:48.171850', '%Y-%m-%dT%H:%M:%S.%f').replace(tzinfo=timezone.utc)


class SearchTests(TestCase):

    def setUp(self):
        meta.override_now(TIMESTAMP)

        self.source = facebook.FacebookSource(
                display_name='display_name',
                user_email='user@domain.org',
                credential='{"cred": true}',
                status=models.SourceStatus.Running.value)
        self.source.save()

        self.contact_1 = models.Contact(display_name="fb_contact_1")
        self.contact_1.save()

        self.contact_2 = models.Contact(display_name="fb_contact_2")
        self.contact_2.save()

        self.conversation = facebook.FacebookConversation(
                facebook_conversation_id=1,
                facebook_conversation_type='user',
                display_name='convesation_1',
                source=self.source)
        self.conversation.save()

        self.conversation.members.add(self.contact_1)
        self.conversation.members.add(self.contact_2)

        self.conversation_2 = facebook.FacebookConversation(
                facebook_conversation_id=2,
                facebook_conversation_type='user',
                display_name='convesation_2',
                source=self.source)
        self.conversation_2.save()

        def make_message(id):
            return facebook.FacebookMessage(
                    id=id,
                    text='message' + str(id),
                    timestamp=TIMESTAMP - timedelta(seconds=id),
                    author=self.contact_1 if id % 2 == 0 else self.contact_2,
                    conversation=self.conversation,
                    facebook_message_id=id)

        for i in range(10, 20):
            make_message(i).save()

        self.other_msg = make_message(20)
        self.other_msg.text = "What a mess"
        self.other_msg.conversation = self.conversation_2
        self.other_msg.save()

    def tearDown(self):
        meta.override_now(None)

    def search(self, expected: list, text: str, conversation_id = None, author_id = None, from_index = None , count = None):
        uri = '/api/search' if not conversation_id else f'/api/conversation/{conversation_id}/message/search'

        args = {'text': text}
        if from_index:
            args['from'] = from_index

        if count:
            args['count'] = count

        if author_id:
            args['author'] = author_id

        response = self.client.get(f'{uri}?{urllib.parse.urlencode(args)}')

        self.assertEqual(200, response.status_code)

        output = [e['id'] for e in json.loads(response.content)]

        self.assertEqual(expected, output)

    def test_simple_search(self):
        self.search([10], 'message10')

    def test_search_all_match(self):
        self.search(list(range(10, 21)), 'mess')

    def test_search_by_author(self):
        self.search([e for e in range(10, 20) if e % 2 == 0], 'message', author_id=self.contact_1.id)

    def test_search_with_start(self):
        self.search(list(range(11, 20)), 'message', from_index=1)

    def test_search_with_count(self):
        self.search(list(range(10, 15)), 'message', count=5)

    def test_search_with_count_and_range(self):
        self.search(list(range(11, 14)), 'message', from_index=1, count=3)

    def test_search_no_match(self):
        self.search([], 'not-found')

    def test_search_conversation_filter(self):
        self.search([20], 'mess', conversation_id=self.conversation_2.id)

