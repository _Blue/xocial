import traceback
import logging
from xocial import settings

TIME_FORMAT = '%Y-%m-%d %H:%M:%S'

def setup_logging():

    handlers = [logging.StreamHandler()]

    logging.basicConfig(format=settings.LOG_FORMAT,
                        datefmt=TIME_FORMAT,
                        handlers = handlers,
                        level=logging.DEBUG)

def log_error(ex):
    try:
        logging.error("Caught exception: " + traceback.format_exc())
    except Exception as e:
        logging.error("Caught exception (failed to log due to" + str(e) + ": " + traceback.format_exc() + ")")


