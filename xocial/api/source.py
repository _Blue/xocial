import json
import logging
from .object_view import ObjectView
from .endpoint import Endpoint
from .utils import *
from xocial import backends, models, errors
from xocial.models import synthetic
from django.http import HttpResponse
from django.db.utils import IntegrityError
from .request_helper import RequestHelper
from datetime import datetime

class SourceHandler(ObjectView):
    model_type = models.Source

    def post_impl(self, request):
        helper = RequestHelper(request)
        source_type = helper.get_str('type')
        display_name = helper.get_str('display_name')

        if source_type != 'SyntheticSource':
            raise errors.InvalidArgument('type', f'Unsupported source type: {source_type}')

        try:
            source = synthetic.SyntheticSource(display_name=display_name, status=models.SourceStatus.NotStarted)
            source.save()
        except IntegrityError as e:
            raise errors.AlreadyExists(f'Backend named "{display_name}" already exists') from e

        logging.info(f'Created new source: {source}')

        return self.response(source)

class SourceSourceContactHandler(Endpoint):
    def post_impl(self, request, id: int):
        helper = RequestHelper(request)
        display_name = helper.get_str('display_name')
        display_image = helper.get_str('display_image', allow_none=True)
        is_me = helper.get_bool('is_me', False)

        source = get_source(id)
        backend = self.get_backend(source)

        return self.response(backend.create_source_contact(display_name, display_image, is_me))

class SourceContactHandler(Endpoint):
    def post_impl(self, request, id: int):
        helper = RequestHelper(request)
        source_contact_id = helper.get_int('source_contact_id')
        source = get_source(id)
        backend = self.get_backend(source)

        return self.response(backend.create_contact(get_source_contact(source_contact_id)))

class SourceConversationHandler(Endpoint):
    def post_impl(self, request, id: int):
        helper = RequestHelper(request)
        member_ids = helper.get_list('members', int)
        display_name = helper.get_str('display_name')
        display_image = helper.get_str('display_image', allow_none=True)
        source = get_source(id)

        members = [get_contact(e) for e in member_ids]
        backend = self.get_backend(source)

        return self.response(backend.create_conversation(display_name, display_image, members))


def create_or_update_message(endpoint, request, source_id: int, conversation_id: int, message_id: int) -> models.Message:
        source_id = int(source_id)
        helper = RequestHelper(request)
        text = helper.get_str('text', default=None, allow_none=True)
        remote_id = helper.get_int('remote_id', default=None)
        attachments = helper.get_list('attachments', int, [])
        reply_to_id = helper.get_int('reply_to', -1)
        author_id = helper.get_int('author', default=None, allow_none=True)
        timestamp = helper.get_timestamp('timestamp', allow_none=True)
        sent_timestamp = helper.get_timestamp('sent_timestamp', allow_none=True)

        conversation = get_conversation(conversation_id).as_leaf_class()
        author = get_contact(author_id) if author_id is not None else None

        if conversation.source.id != source_id:
            raise errors.InvalidArgument('source_id', f'Conversation {conversation} does not match source {source_id}')

        reply_to = None
        if reply_to_id != -1:
            reply_to = get_message(reply_to_id)

            if reply_to.conversation.id != conversation.id:
                raise errors.InvalidRequest(f"Reply-to message {reply_to_id} is in conversation {reply_to.conversation.id}, expected {conversation.id}")

        backend = endpoint.get_backend(conversation.source)

        if message_id is None:
            return backend.create_message(text=text,
                                          conversation=conversation,
                                          attachments=[get_attachment(e) for e in attachments],
                                          reply_to=reply_to,
                                          author=author,
                                          timestamp=timestamp,
                                          sent_timestamp=sent_timestamp,
                                          remote_id=remote_id)
        else:
            return backend.update_message(text=text,
                                          conversation=conversation,
                                          attachments=[get_attachment(e) for e in attachments],
                                          reply_to=reply_to,
                                          author=author,
                                          timestamp=timestamp,
                                          sent_timestamp=sent_timestamp,
                                          remote_id=remote_id,
                                          local_id=message_id)



class SourceMessageConversationHandler(Endpoint):
    def post_impl(self, request, source_id: int, conversation_id: int):
        return self.response(create_or_update_message(self, request, source_id, conversation_id, None))

class SourceMessageConversationHandlerUpdate(Endpoint):
    def post_impl(self, request, source_id: int, conversation_id: int, message_id: int):
        return self.response(create_or_update_message(self, request, source_id, conversation_id, message_id))
