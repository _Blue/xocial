from xocial import models, errors
from django.http import Http404
from datetime import datetime

def get_source(id) -> models.Source:
    try:
        return models.Source.objects.get(id=id)
    except models.Source.DoesNotExist as e:
        raise errors.SourceNotFound(id) from e

def get_source_contact(id) -> models.SourceContact:
    try:
        return models.SourceContact.objects.get(id=id)
    except models.SourceContact.DoesNotExist as e:
        raise errors.SourceContactNotFound(id) from e

def get_contact(id) -> models.Contact:
    try:
        return models.Contact.objects.get(id=id)
    except models.Contact.DoesNotExist as e:
        raise errors.ContactNotFound(id) from e


def get_message(id) -> models.Message:
    try:
        return models.Message.objects.get(id=id)
    except models.Message.DoesNotExist as e:
        raise errors.MessageNotFound(id) from e

def get_conversation(id) -> models.Conversation:
    try:
        return models.Conversation.objects.get(id=id)
    except models.Conversation.DoesNotExist as e:
        raise errors.ConversationNotFound(id) from e

def get_attachment(id) -> models.UploadedAttachment:
    try:
        return models.UploadedAttachment.objects.get(id=id)
    except models.UploadedAttachment.DoesNotExist as e:
        raise errors.UploadedAttachmentNotFound(id) from e


def get_contact(id) -> models.Contact:
    try:
        return models.Contact.objects.get(id=id)
    except models.Contact.DoesNotExist as e:
        raise errors.ContactNotFound(id) from e

def get_attachment_download(id) -> models.AttachmentDownload:
    try:
        return models.AttachmentDownload.objects.get(id=id)
    except models.AttachmentDownload.DoesNotExist as e:
        raise errors.ObjectNotFound(id)
