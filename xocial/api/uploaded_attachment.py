import json
import logging
import os
from datetime import datetime
from .endpoint import Endpoint
from xocial import backends, models, errors, settings
from django.http import HttpResponse

class UploadedAttachmentHandler(Endpoint):

    def post_impl(self, request, filename: str):
        if any(c in filename for c in ['\\', '/', '"', "'"]):
            raise errors.InvalidRequest(f'Invalid filename: "{filename}')

        uploaded_attachment = models.UploadedAttachment(file_name=filename)
        uploaded_attachment.save()

        file_path = os.path.join(settings.MEDIA_ROOT, str(uploaded_attachment.id), filename)
        logging.info(f'Saving uploaded attachment {uploaded_attachment.id} in "{file_path}"')

        os.makedirs(os.path.dirname(file_path), exist_ok=True)


        with open(file_path, 'wb') as fd:
            fd.write(request.body)

        uploaded_attachment.size = os.path.getsize(file_path)
        uploaded_attachment.save()

        return self.response(uploaded_attachment)
