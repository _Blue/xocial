import json
from .endpoint import Endpoint
from xocial import backends, models, errors
from django.http import HttpResponse

class ErrorAcknowledgeHandler(Endpoint):
    def post_impl(self, request, id: str):

        try:
            error = models.Error.objects.get(id=id)
        except models.Error.DoesNotExist as e:
            raise errors.ErrorNotFound(id) from e

        affected = 0
        for e in list(models.Source.objects.filter(errors=error)) + list(models.Conversation.objects.filter(errors=error)):
            e.errors.remove(error)
            e.save()

            affected = affected + 1

        return HttpResponse(content=json.dumps({'affected': affected}))
