import json
from .endpoint import Endpoint
from xocial import backends, models, errors
from xocial.backends.facebook import FacebookBackend
from xocial.errors import *
from django.views import View
from django.http import HttpResponse
from .utils import *

class AttachmentDownloadHandler(Endpoint):
    def get_impl(self, request, id):
        download = get_attachment_download(id)

        if not download.state == models.AttachmentState.Available.value:
            raise AttachmentUnavailable(id)

        response = HttpResponse(download.content, content_type=download.web_content_type or 'application/data')

        response['Content-Length'] = len(download.content)

        return response

