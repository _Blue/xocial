import json
from xocial import errors
from datetime import datetime, timezone
import django.utils.timezone

class RequestHelper:
    def __init__(self, request):
        self.request = request
        self._body = None

    @property
    def body(self) -> dict:
        try:
            self._body = self._body or json.loads(self.request.body)
        except Exception as e:
            raise errors.InvalidRequest("Failed to decode the request body") from e

        return self._body

    def get(self, name: str, default=None, allow_none=False):
        if not name in self.body:
            if default is not None:
                return default
            else:
                if allow_none:
                    return None
                else:
                    raise errors.MissingArgument(name)

        return self.body[name]

    def get_str(self, name: str, default=None, allow_none=False) -> str:
        value = self.get(name, default, allow_none)

        if value is None and allow_none:
            return None

        if not isinstance(value, str):
            raise errors.InvalidArgumentType(name, "string", type(value))

        return value

    def get_float(self, name: str, default=None, allow_none=False) -> str:
        value = self.get(name, default, allow_none)

        if value is None and allow_none:
            return None

        if not isinstance(value, float):
            raise errors.InvalidArgumentType(name, "float", type(value))

        return value


    def get_int(self, name: str, default=None, allow_none=False) -> int:
        value = self.get(name, default, allow_none)

        if value is None and allow_none:
            return None

        if not isinstance(value, int):
            raise errors.InvalidArgumentType(name, "int", type(value))

        return value

    def get_bool(self, name: str, default=None) -> bool:
        value = self.get(name, default)

        if not isinstance(value, bool):
            raise errors.InvalidArgumentType(name, "bool", type(value))

        return value

    def get_list(self, name: str, inner_type: type, default=None) -> list:
        value = self.get(name, default)

        if value == default:
            return value

        if not isinstance(value, list):
           raise errors.InvalidArgumentType(name, "list", type(value))

        if not all(isinstance(e, inner_type) for e in value):
           raise errors.InvalidArgumentType(name, f"list<{inner_type}>", type(value))

        return value

    def get_dict(self, name: str, default=None) -> dict:
        value = self.get(name, default)

        if value == default:
            return value

        if not isinstance(value, dict):
           raise errors.InvalidArgumentType(name, "dict", type(value))

        return value

    def get_timestamp(self, name: str, allow_none=False) -> datetime:
        float_value = self.get_float(name, None, allow_none)
        if not float_value:
            return None

        return datetime.fromtimestamp(float_value).replace(tzinfo=django.utils.timezone.get_current_timezone())
