import json

from urllib.parse import urlparse, parse_qs
from .endpoint import Endpoint
from xocial import models, errors
from django.http import HttpResponse
from .message import MessageHandler
from .request_helper import RequestHelper
from .utils import *

class SearchHandler(MessageHandler):
    def search_impl(self, text: str, conversation_id: int, contact_id: int, start_index: int, count: int):
        query = {'text__icontains': text}

        if conversation_id:
            query['conversation'] = get_conversation(conversation_id)

        if contact_id:
            query['author'] = get_contact(contact_id)

        query = models.Message.objects.filter(**query).order_by('-timestamp')[start_index:start_index + count]

        return self.response([e for e in query])

    def search(self, request, conversation_id: int) -> HttpResponse:
        arguments = parse_qs(urlparse(request.build_absolute_uri()).query)

        if not 'text' in arguments:
            raise errors.MissingArgument('text')

        query = arguments['text'][0]
        author = int(arguments.get('author')[0]) if 'author' in arguments else None
        start_index = int(arguments['from'][0]) if 'from' in arguments else 0
        count = int(arguments.get('count')[0]) if 'count' in arguments else 20

        return self.search_impl(
                query,
                conversation_id = conversation_id,
                contact_id = author,
                start_index = start_index,
                count = count)

    def get_impl(self, request, author=None):
        return self.search(request, None)

class ConversationSearchHandler(SearchHandler):

    def get_impl(self, request, id):
        return self.search(request, conversation_id=id)
