import json

from .endpoint import Endpoint
from xocial import models, errors
from django.http import HttpResponse
from .message import MessageHandler
from .request_helper import RequestHelper
from .utils import *

class ConversationMessageHandler(MessageHandler):
    def get_impl(self, request, id):
        from_id = request.GET.get('from_id', None)
        count = int(request.GET.get('count', 100))
        order = request.GET.get('order', 'desc')

        return self.get_messages(from_id, count, order, get_conversation(id))

    def post_impl(self, request, id):
        helper = RequestHelper(request)
        text = helper.get_str('text')
        attachments = helper.get_list('attachments', int, [])
        reply_to_id = helper.get_int('reply_to', -1)

        conversation = get_conversation(id).as_leaf_class()

        reply_to = None
        if (reply_to_id != -1):
            reply_to = get_message(reply_to_id)

            if reply_to.conversation.id != conversation.id:
                raise errors.InvalidRequest(f"Reply-to message {reply_to_id} is in conversation {reply_to.conversation.id}, expected {conversation.id}")

        backend = self.get_backend(conversation.source)
        message = backend.send_message(text=text, conversation=conversation, attachments=[get_attachment(e) for e in attachments], reply_to=reply_to)

        return self.response(message)
