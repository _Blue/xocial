import json
from .endpoint import Endpoint
from xocial import backends, models, errors
from xocial.backends.facebook import FacebookBackend
from xocial.errors import *
from django.views import View
from django.http import HttpResponse
from .request_helper import RequestHelper
from .utils import get_conversation

class ConversationRefreshHandler(Endpoint):
    def post_impl(self, request, id):
        conversation = get_conversation(id)

        backend = self.get_backend(conversation.source)
        backend.fetch_all_conversation_messages(conversation)

        return HttpResponse()

class UnreadConversationHandler(Endpoint):
    def get_impl(self, request):
        conversations = models.Conversation.objects.all()

        def read(conversation: models.Conversation) -> bool:
            last_message = models.Message.objects.filter(conversation=conversation).order_by('-timestamp').first()

            return not last_message or (conversation.last_read_message and conversation.last_read_message.id == last_message.id)

        unread_conversations = [e for e in conversations if not read(e)]
        return self.response(unread_conversations)

def CreateConversationHandler(Endpoint):

    def post_impl(self, request):
        helper = RequestHelper(request)

        members = helper.get_list('members', str)
        message = helper.get_str('message')

        if not members:
            raise errors.InvalidRequest('A new conversation needs at least one member')

        source_contacts = []

        for e in members:
            try:
                source_contacts.add(models.SourceContact.get(id=int(e)))
            except models.SourceContact.DoesNotExist as ex:
                raise errors.ContactNotFound(e) from ex

        sources = [e.source for e in source_contacts]
        if set(len(sources)) != 1:
            raise errors.InvalidRequest(f"Cannot create conversation with contacts from different sources {[e.id for e in sources]}")

        backend = self.get_backend(self, sources[0])

