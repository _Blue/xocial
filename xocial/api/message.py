import json
from .endpoint import Endpoint
from .utils import *
from xocial import backends, models, errors
from django.http import HttpResponse
from .request_helper import RequestHelper

class MessageHandler(Endpoint):
    def get_messages(self, from_id: int, count: int, order: str, conversation=None) -> list:
        if order not in ['asc', 'desc']:
            raise errors.InvalidArgument('order', 'expected "asc" or "desc"')

        query_args = {}

        if from_id:
            try:
                message = models.Message.objects.get(id=from_id)
            except models.Message.DoesNotExist as e:
                raise errors.MessageNotFound(from_id) from e

            query_args['timestamp__gt' if order == 'asc' else 'timestamp__lt'] = message.timestamp

        if conversation:
            query_args['conversation'] = conversation

        messages = models.Message.objects.order_by('{}timestamp'.format('-' if order == 'desc' else '')).filter(**query_args)[:count]

        return self.response(list(messages))

    def get_impl(self, request):
        from_id = request.GET.get('from_id', None)
        count = int(request.GET.get('count', 100))
        order = request.GET.get('order', 'desc')

        return self.get_messages(from_id, count, order)

class MessageReactHandler(Endpoint):
    def post_impl(self, request, id):
        helper = RequestHelper(request)
        reaction = helper.get_dict('reaction')

        message = get_message(id).as_leaf_class()

        backend = self.get_backend(message.conversation.source)

        return self.response(backend.react_to_message(message, reaction))


class MessageReadHandler(Endpoint):
    def post_impl(self, request, id):
        helper = RequestHelper(request)
        update_backend = helper.get_bool('update_backend', False)

        message = get_message(id).as_leaf_class()
        if not message.conversation.last_read_message or message.timestamp > message.conversation.last_read_message.timestamp:
            message.conversation.last_read_message = message
            message.conversation.save()

        if update_backend:
            backend = self.get_backend(message.conversation.source)

            try:
                backend.mark_message_as_read(message)
            except Exception as e:
                raise errors.BackendError(backend) from e

        return self.response(message)

