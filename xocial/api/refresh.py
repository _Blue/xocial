from xocial import models
from xocial.backends.facebook import FacebookBackend
from xocial.api.endpoint import Endpoint
from django.http import HttpResponse

class RefreshHandler(Endpoint):
    def post_impl(self, request):

        source = models.FacebookSource.objects.get(id=2)
        backend = FacebookBackend(source)

        backend.refresh()
        backend.listen()

        return HttpResponse()
