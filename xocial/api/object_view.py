import json
from .endpoint import Endpoint
from xocial import backends, models, errors
from xocial.backends.facebook import FacebookBackend
from django.views import View
from django.http import HttpResponse
from .request_helper import RequestHelper
from datetime import datetime, timezone

class ObjectView(Endpoint):
    model_type = models.DataModel

    def get_objects(self, from_id: int, count: int, order: str, updated_after: float, custom_query: dict) -> list:
        if order not in ['asc', 'desc']:
            raise errors.InvalidArgument('order', 'expected "asc" or "desc"')

        query_args = {}

        if from_id:
            try:
                object = models.DataModel.objects.get(id=from_id)
            except models.DataModel.DoesNotExist as e:
                raise errors.ObjectNotFound(from_id) from e

            query_args['last_updated__gt' if order == 'asc' else 'last_updated__lt'] = object.last_updated

        if updated_after:
            query_args['last_updated__gt' if order == 'asc' else 'last_updated__lt'] = datetime.fromtimestamp(updated_after).replace(tzinfo=timezone.utc)

        for e in custom_query:
            query_args[e] = custom_query[e]

        objects = self.model_type.objects.order_by('{}last_updated'.format('-' if order == 'desc' else '')).filter(**query_args)[:count]

        return self.response(list(objects))

    def get_single_object(self, id: int):
        try:
            object = self.model_type.objects.get(id=id)
        except self.model_type.DoesNotExist as e:
            raise errors.ObjectNotFound(id) from e

        return self.response(object)

    def get_impl(self, request, id=None):
        if id:
            return self.get_single_object(id)

        order = request.GET.get('order', 'desc')
        from_id = request.GET.get('from_id', None)
        updated_after = request.GET.get('updated_after', None)
        count = int(request.GET.get('count', 100))

        custom_query = {}
        for e in request.GET:
            if e not in ['from_id', 'updated_after', 'count', 'order']:
                custom_query[e] = request.GET(e)


        return self.get_objects(from_id, count, order, float(updated_after) if updated_after else None, {})

    def post_impl(self, request):
        helper = RequestHelper(request)
        from_id = helper.get_int('from_id', default=None, allow_none=True)
        updated_after = helper.get_float('updated_after', default=None, allow_none=True)
        count = helper.get_int('count', default=100)
        order = helper.get_str('order', 'desc')
        custom_query = helper.get_dict('custom_query', {})


        return self.get_objects(from_id, count, order, float(updated_after) if updated_after else None, custom_query)


def make_view(model_type: type) -> ObjectView:
    return type(f'{model_type}_view', (ObjectView,), {'model_type': model_type}).as_view()
