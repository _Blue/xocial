import json
import traceback
from xocial import log
from xocial.errors import Error, InvalidRequest
from xocial import backends
from xocial import models
from xocial.backends.facebook import FacebookBackend
from xocial.serializers import serialize
from django.views import View
from django.http import HttpResponse

class Endpoint(View):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.compress = False
        self.cleanups = []

    def get_backend(self, source: models.Source):
        backend = backends.acquire_backend(source)

        self.cleanups.append(lambda: backends.release_backend(backend))

        return backend

    def read_compress_flag(self, request):
        if not 'X-Xocial-Use-Compact-Serialization' in request.headers:
            return

        header_value = request.headers['X-Xocial-Use-Compact-Serialization']
        if header_value not in ['0', '1']:
            raise InvalidRequest('X-Xocial-Use-Compact-Serialization must have a value of 0 or 1')

        self.compress = header_value == '1'

    def post(self, request, *args, **kwargs):
        try:
            self.read_compress_flag(request)

            return self.post_impl(request, *args, **kwargs)
        except Exception as e:
            return self.handle_exception(e)
        finally:
            self.cleanup()

    def get(self, request, *args, **kwargs):
        try:
            self.read_compress_flag(request)

            return self.get_impl(request, *args, **kwargs)
        except Exception as e:
            return self.handle_exception(e)
        finally:
            self.cleanup()

    def get_impl(self, *args, **kwargs):
        raise InvalidRequest("GET not supported on this route")

    def post_impl(self, *args, **kwargs):
        raise InvalidRequest("POST not supported on this route")

    def standard_error_response(self, error: Error) -> HttpResponse:
        body = {'error': str(error), 'details': traceback.format_exc()}

        return HttpResponse(json.dumps(body, indent=2), status=error.HttpCode)

    def handle_exception(self, exception):
        log.log_error(exception)

        if isinstance(exception, Error):
            return self.standard_error_response(exception)

        body = {'error': 'UncaughtException', 'details': traceback.format_exc()}

        return HttpResponse(json.dumps(body, indent=2), status=500)

    def cleanup(self):
        for e in self.cleanups:
            e()

    def response(self, content: dict):
        return HttpResponse(json.dumps(serialize(content, compress=self.compress)))
