import inspect
import django.db
from pydoc import locate
from threading import local
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from . import models
from .models import facebook, synthetic
from xocial.models.meta import get_all_visible_fields, get_all_extra_queries
from django.db.models import Model, ManyToManyField
from django.utils.functional import cached_property
from collections import OrderedDict


context = local()
available_serializers = {}

def build_type_string(instance):
    current_type = type(instance)

    types = []
    while not current_type is models.DataModel:
        types.append(current_type.__name__)

        current_type = current_type.__bases__[0]

    return "/".join(reversed(types))


def serialize_as_reference(instance):
    return {
            'id': instance.id,
            'type': build_type_string(instance.as_leaf_class())
           }

class ExtraField:
    def __init__(self, name, query):
        self.query = query
        self.field_name = name

    def get_attribute(self, instance):
        return self.query(instance)

    def to_representation(self, value):
        if isinstance(value, models.DataModel):
            return serialize_impl(value.as_leaf_class())
        else:
            return value

class TypeSerializer(serializers.ModelSerializer):

    @property
    def _readable_fields(self): # Hack to call serializers in sorted order
        classic_fields = super()._readable_fields
        extra_fields = [ExtraField(e, self.extra_queries[e]) for e in self.extra_queries]

        return sorted(list(classic_fields) + extra_fields, key = lambda e: e.field_name)

    def to_representation(self, instance):
        leaf_instance = instance.as_leaf_class()

        if instance.id in context.parents: # We are already in the process of serializing this object, don't create an infinite recursion
            return serialize_as_reference(instance)

        if type(leaf_instance) is self.Meta.model:
            if context.compress:
                if instance.id in context.processed:
                    return serialize_as_reference(instance)
                else:
                    context.processed.add(instance.id)

            context.parents.append(instance.id)
            output = super().to_representation(leaf_instance)
            context.parents.pop()

            output["type"] = build_type_string(leaf_instance)

        else: # In case the leaf type requires a different serializer
            output = available_serializers[type(leaf_instance)]().to_representation(leaf_instance)

        return output

class IDSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        return serialize_as_reference(instance)

def make_serializer(model_type) -> TypeSerializer:
    if model_type in available_serializers:
        return available_serializers[model_type]

    (visible_fields, visible_by_id_fields) = get_all_visible_fields(model_type)

    class Meta:
        model = model_type
        fields = list(visible_fields) + list(visible_by_id_fields)

    inners = OrderedDict()
    inners['Meta'] = Meta
    inners['extra_queries'] = get_all_extra_queries(model_type)
    t = type('serializer', (TypeSerializer,), inners)

    for e in sorted(visible_fields):
        field = model_type._meta.get_field(e)

        if hasattr(field, '_type'):

            # Special case for foreign key of the same type as the model, or forward-declared with an str
            if type(field._type) is str:
                inners[e] = t()
            else:
                field_type = locate(f'xocial.models.{field._type}') if type(field._type) is str else field._type
                inners[e] = make_inner(field_type, many=isinstance(field, ManyToManyField))

    for e in visible_by_id_fields:
        inners[e] = IDSerializer()

    for e in inners:
        setattr(t, e, inners[e])

    t._declared_fields = t._get_declared_fields((TypeSerializer,), inners)

    available_serializers[model_type] = t
    return t

def make_inner(model_type, many=False):
    return make_serializer(model_type)(many=many)

def make_viewset(model_type):
    class ViewSet(viewsets.ModelViewSet):
        queryset = model_type.objects.all()
        serializer_class = make_serializer(model_type)

    return ViewSet


def initialize_serializers():
    for _, e in inspect.getmembers(models) + inspect.getmembers(models.facebook) + inspect.getmembers(models.synthetic):
        if inspect.isclass(e) and hasattr(e, 'visible_fields'):
            make_serializer(e)

def serialize(object, compress=False) -> dict:
    context.processed = set()
    context.parents = []
    context.compress = compress

    if isinstance(object, list):
        if len(object) == 0:
            return []
        else:
            return [serialize_impl(e.as_leaf_class()) for e in object]
    else:
        return serialize_impl(object)

def serialize_impl(object) -> dict:
    if object is None:
        return None

    if type(object) not in available_serializers:
        raise RuntimeError(f"No serializer loaded for type: {type(object)}")

    return available_serializers[type(object)](object).data

initialize_serializers()
