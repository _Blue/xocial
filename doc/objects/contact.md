# Contact object

The contact object represents a user contact.

Not to be confused with [SourceContact](source_contact), which is backend-specific. A **Contact** can contain multiple [SourceContact](source_contact) objects

```
{
  display_name: string,
  display_image: url?,
  sources: list<SourceContact>
  is_me: bool
}
```

Fields:

* `display_name`: Displayable string representing the contact's name or nickname
* `display_image`: Url pointing to a displayable image for this contact (can be null)
* `sources`: A list of associated [SourceContact](source_contact) for this contact
* `is_me` True if this contact represents the current user

