# Source

A source object represents a communication backend.

Supported specializations:

* [FacebookSource](facebooksource)

```
{
  display_name: string,
  errors: list<Error>,
  last_updated: Timestamp;
  status: string,
}
```

Fields:

* `display_name`: A displayable name of the source
* `errors` A list of [Error] objects that are linked to this source
* `last_updated`: Timestamp representing when the object was last modified
* `status`: A string describing the source status

Source status:

* `NotStarted`: When xocial is starting, and the source is not running yet
* `Initializing`: When xocial starts, all sources are set to `Initializing` state
* `Running`: The source is running normally
* `SoftFailed`: An error has occurred and the source is backed off
* `HardFailed`: The source has backed off multiple time without success and is in an error state
