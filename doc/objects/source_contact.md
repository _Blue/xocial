# Source contact object

A source contact represents a backend-specific contact (like a Facebook contact for example).

This object is specialized per source and contains backend-specific information such as internal user ids, types, and so on.


```
{
  source: Source,
  display_name: string,
  display_image: Url?,
  is_me: bool,
}
```

Fields:

* `Source`: The [Source](source) object that this contact comes from
* `display_name`: A displayable string representing the contact's name or nickname
* `display_image`: A URL pointing to a displayable image for this contact (can be null)
* `is_me`: True if this contact represents the current user
