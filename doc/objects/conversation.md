# Conversation object

A conversation represents a chat room with one or more contacts

```
{
  display_name: string,
  members: list<Contact>,
  gone_members: list<Contact>
  source: Source
  last_read_message: Message
  errors: list<Error>
}
```

Fields:

* `display_name`: A displayable name representing the conversation
* `members`: A list of [contacts](Contact) that are part of this conversation
* `gone_members`: A list of [contacts](Contact) that used to be part of the conversation, but that are now absent
* `source`: Associated [Source](Source)
* `last_read_message`: An object containing the id and of the last message that has been read by the user in this conversation (can be null)
* `errors`: List of [Errors](Error) that have occurred in this conversation

