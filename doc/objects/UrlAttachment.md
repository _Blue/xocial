# UrlAttachment

A Url attachment represent a URL attachment to a message.

Content:

```
{
  url: url,
  title: string?,
  description: string?
  original_url: url?
}
```
