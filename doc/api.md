# Xocial API documentation

Xocial API exposes a set of objects and methods which can be used to interact with xocial sources (A.K.A. backends) or database.

## Objects

Xocial objects JSON representation follow the same format:

```json
{
  "id": {object_id},
  "type": {object_type_string}
  [Arbitrairy fields]
}
```

Every object is guaranteed to have the following fields:

* `id`: A unique id that identifies the object (guaranteed to be unique between tables and increasing)
* `type`: A string that describes the object type from the most basic to the most derived, example type string: `Message/FacebookMessage/FacebookReplyToMessage`

The type strings allows objects to be easily specialized without breaking API compatibility.

### Fetching objects

To fetch an object, regardless of its type, call:

```
/api/object/{id}
```

To fetch all objects, call:
```
/api/object?count={count}&from_id{from_id}&order={order}
```

This call accepts the following arguments:

* `count` : number of objects to return. Default: 100
* `from_id`: object id to start enumerating from (excluded), default: 0
* `order`: order to enumerate object. Either `asc` for ascending or `desc` for descending. Default: `asc`

To fetch objects of a specific type only, use the following routes:

```
/api/{object_name}
```

For example, to fetch Conversation objects, use `/api/conversation`
