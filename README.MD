# Xocial, an intuitive API frontend for various messaging services

## Deployement

```bash
$ git clone https://bitbucket.org/_Blue/xocial # clone this repo
$ cd xocial
$ pip install -r requirements.txt # install dependencies
$ python manage.py makemigrations # compute DB migrations
$ python manage.py migrate # migrate the DB
$ python manage.py runserver # run the server (not suitable for production)
```

## Adding sources

Currently, only Facebook is supported.
To add a new facebook source, get a facebook cookie and create a new source as shown below:

```bash
$ python ./manage.py shell
>> import json
>> from fbchat import Client
>> client = Client('facebook_email', 'facebook_password')
>> fb_cookie = json.dumps(client.getSession())
>> from xocial.models import FacebookSource
>> source = FacebookSource()
>> source.email = 'facebook_email'
>> source.credentials =  facebook_cookie
>> source.display_name = 'Facebook'
>> source.save()
```

When the server is started, all sources start polling messages

## Backfilling the database

When xocial is running, all new messages are pulled and added to the database, but old messages are not fetched.
To fetch old messages, run:

```bash
$ python ./manage.py shell
>> from xocial import shell
>> backend = get_backend(source_id) # You can ommit source_id if you only have one source
>> backend.fetch_all_messages() # Will fetch all messages from facebook. This operation can take a long time depending on the amount of data to fetch
```


## Running tests

Run unit tests by:

```
$ python managed.py test
```


